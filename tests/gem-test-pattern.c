/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

/**
 * SECTION: gem-test-pattern
 * @Title: Test Patterns
 * @Short_Description: predefined test patterns
 */


/* Stateless patterns.  */

static gem_PatternAction
pat_stateless_any (unused (gem_Pattern *pat),
                   unused (gem_Event *evt),
                   gem_EventID *id,
                   gem_EventType *type,
                   gem_EventTime *time,
                   unused (gpointer data))
{
  *id = 0; *type = 0; *time = 0;
  return GEM_PATTERN_COLLECT_AND_COMPLETE;
}

static gem_PatternAction
pat_stateless_any_expand (unused (gem_Pattern *pat),
                          unused (gem_Event *evt),
                          gem_EventID *id,
                          gem_EventType *type,
                          gem_EventTime *time,
                          unused (gpointer data))
{
  *id = 0; *type = 0; *time = 0;
  return GEM_PATTERN_COLLECT_AND_EXPAND;
}

static gem_PatternAction
pat_stateless_wacky (unused (gem_Pattern *pat),
                     unused (gem_Event *evt),
                     gem_EventID *id,
                     gem_EventType *type,
                     gem_EventTime *time,
                     unused (gpointer data))
{
  gem_PatternAction act;
  *id = 0; *type = 0; *time = 0;
  act = g_random_int_range (GEM_PATTERN_SKIP,
                            GEM_PATTERN_CLEAR_AND_COLLECT + 1);
  return act;
}


/* Mod-based patterns.  */

typedef struct
{
  gint n_calls;
  gint num;
  gint ret;
} pat_mod_State;

static gem_PatternAction
pat_mod (unused (gem_Pattern *pat),
          unused (gem_Event *evt),
          gem_EventID *id,
          gem_EventType *type,
          gem_EventTime *time,
          pat_mod_State *st)
{
  if (st->n_calls++ % st->num == 0)
    {
      *id = 0; *type = 0; *time = 0;
      return st->ret;
    }
  return GEM_PATTERN_SKIP;
}

#define PAT_MOD_INIT_DEFN(Name, _num, _ret)                     \
  static void                                                   \
  pat_mod_init_##Name (unused (gem_Pattern *pat),               \
                       pat_mod_State *st)                       \
  {                                                             \
    st->n_calls = 0;                                            \
    st->num = (_num);                                           \
    st->ret = (_ret);                                           \
  }

PAT_MOD_INIT_DEFN (2, 2, GEM_PATTERN_COLLECT_AND_COMPLETE);
PAT_MOD_INIT_DEFN (16, 16, GEM_PATTERN_COLLECT_AND_COMPLETE);
PAT_MOD_INIT_DEFN (128, 128, GEM_PATTERN_COLLECT_AND_COMPLETE);
PAT_MOD_INIT_DEFN (1024, 1024, GEM_PATTERN_COLLECT_AND_COMPLETE);
PAT_MOD_INIT_DEFN (10240, 10240, GEM_PATTERN_COLLECT_AND_COMPLETE);

#define PAT_MOD_INIT_CONFIG(cfg, data_init_func)                        \
  G_STMT_START                                                          \
  {                                                                     \
    (cfg)->func = (gem_PatternFunc) pat_mod;                            \
    (cfg)->data_size = sizeof (pat_mod_State);                          \
    (cfg)->data_init = (gem_PatternDataInitFunc) data_init_func;        \
    (cfg)->data_fini = NULL;                                            \
  }                                                                     \
  G_STMT_END


/* Automata-based patterns.  */

typedef struct _pat_State
{
  gint state;
} pat_State;

static void
pat_state_init (unused (gem_Pattern *pat),
                gpointer data)
{
  pat_State *st = (pat_State *) data;
  st->state = 0;
}

#define PAT_INIT_CONFIG(cfg, pat_func)                                  \
  G_STMT_START                                                          \
  {                                                                     \
    (cfg)->func = (gem_PatternFunc) pat_func;                           \
    (cfg)->data_size = sizeof (pat_State);                              \
    (cfg)->data_init = pat_state_init;                                  \
    (cfg)->data_fini = NULL;                                            \
  }                                                                     \
  G_STMT_END

#define PAT_OVERHEAD()                          \
  G_STMT_START                                  \
  {                                             \
    guint i;                                    \
    for (i=0; i < 1000; i++);                   \
  }                                             \
  G_STMT_END

#define PAT_FUNC(Name)                          \
  /* FUNC-START */                              \
  static gem_PatternAction                      \
  pat_##Name (unused (gem_Pattern *pat),        \
              gem_Event *evt,                   \
              unused (gem_EventID *id),         \
              unused (gem_EventType *type),     \
              unused (gem_EventTime *time),     \
              pat_State *st)                    \
  { PAT_OVERHEAD (); switch (st->state) {/*}}*/

#define PAT_END()                               \
  default: g_assert_not_reached (); /*{{*/}}    \
  /* FUNC-END */

#define PAT_STATE(s)                            \
  /* STATE-BEGIN */                             \
  case (s): { switch (evt->type) {/*}}*/

#define PAT_CASE_TMPL(tp, act, code)\
  case ((tp)): { code; return GEM_PATTERN_##act; }

#define PAT_CASE(tp, act, to)\
  PAT_CASE_TMPL ((tp), act, st->state = (to))

/* #define PAT_CASE_MARK(tp, act, to, newtp)               \ */
/*   PAT_CASE_TMPL                                         \ */
/*   ((tp), act, (st->state = (to), *type = (newtp))) */

#define PAT_DEFAULT(act, to)                                    \
  default: { st->state = (to); return GEM_PATTERN_##act; }      \
  /*{{*/ } break; }                                             \
  /* STATE-END */

/* Pattern: 0 */
PAT_FUNC (0)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_END ();
}

/* Pattern: 1 */
PAT_FUNC (1)
{
  PAT_STATE (0)
    {
      PAT_CASE (1, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_END ();
}

/* Pattern: 01 */
PAT_FUNC (0_1)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (1, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (SKIP, 1);
    }
  PAT_END ();
}

/* Pattern: 012 */
PAT_FUNC (0_1_2)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (1, COLLECT, 2);
      PAT_DEFAULT (SKIP, 1);
    }
  PAT_STATE (2)
    {
      PAT_CASE (2, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (SKIP, 2);
    }
  PAT_END ();
}

/* Pattern: 0123 */
PAT_FUNC (0_1_2_3)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (1, COLLECT, 2);
      PAT_DEFAULT (SKIP, 1);
    }
  PAT_STATE (2)
    {
      PAT_CASE (2, COLLECT, 3);
      PAT_DEFAULT (SKIP, 2);
    }
  PAT_STATE (3)
    {
      PAT_CASE (3, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (SKIP, 3);
    }
  PAT_END ();
}

/* Pattern: 01234 */
PAT_FUNC (0_1_2_3_4)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (1, COLLECT, 2);
      PAT_DEFAULT (SKIP, 1);
    }
  PAT_STATE (2)
    {
      PAT_CASE (2, COLLECT, 3);
      PAT_DEFAULT (SKIP, 2);
    }
  PAT_STATE (3)
    {
      PAT_CASE (3, COLLECT, 4);
      PAT_DEFAULT (SKIP, 3);
    }
  PAT_STATE (4)
    {
      PAT_CASE (4, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (SKIP, 4);
    }
  PAT_END ();
}

/* Pattern: 0+ */
PAT_FUNC (0_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_END ();
}

/* Pattern: 1+ */
PAT_FUNC (1_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (1, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_END ();
}

/* Pattern: (01)+ */
PAT_FUNC (0_1_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (1, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (SKIP, 1);
    }
  PAT_END ();
}

/* Pattern: (012)+ */
PAT_FUNC (0_1_2_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (1, COLLECT, 2);
      PAT_DEFAULT (SKIP, 1);
    }
  PAT_STATE (2)
    {
      PAT_CASE (2, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (SKIP, 2);
    }
  PAT_END ();
}

/* Pattern: exact:01 */
PAT_FUNC (exact_0_1)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (0, CLEAR_AND_COLLECT, 1);
      PAT_CASE (1, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_END ();
}

/* Pattern: exact:012 */
PAT_FUNC (exact_0_1_2)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (0, CLEAR_AND_COLLECT, 1);
      PAT_CASE (1, COLLECT, 2);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_STATE (2)
    {
      PAT_CASE (2, COLLECT_AND_COMPLETE, 0);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_END ();
}

/* Pattern: exact:0+ */
PAT_FUNC (exact_0_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_END ();
}

/* Pattern: exact:1+ */
PAT_FUNC (exact_1_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (1, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_END ();
}

/* Pattern: exact:(01)+ */
PAT_FUNC (exact_0_1_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (0, CLEAR_AND_COLLECT, 1);
      PAT_CASE (1, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_END ();
}

/* Pattern: exact:(012)+ */
PAT_FUNC (exact_0_1_2_plus)
{
  PAT_STATE (0)
    {
      PAT_CASE (0, COLLECT, 1);
      PAT_DEFAULT (SKIP, 0);
    }
  PAT_STATE (1)
    {
      PAT_CASE (0, CLEAR_AND_COLLECT, 1);
      PAT_CASE (1, COLLECT, 2);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_STATE (2)
    {
      PAT_CASE (2, COLLECT_AND_EXPAND, 0);
      PAT_DEFAULT (CLEAR, 0);
    }
  PAT_END ();
}


/* User-friendly names for values of #gem_test_Pattern.  */
static const gchar *gem_test_pattern_names[] =
{
  "0",                          /* GEM_TEST_PATTERN_0 */
  "1",                          /* GEM_TEST_PATTERN_1 */
  "01",                         /* GEM_TEST_PATTERN_0_1 */
  "012",                        /* GEM_TEST_PATTERN_0_1_2 */
  "0123",                       /* GEM_TEST_PATTERN_0_1_2_3 */
  "01234",                      /* GEM_TEST_PATTERN_0_1_2_3_4 */
  "0+",                         /* GEM_TEST_PATTERN_0_PLUS */
  "1+",                         /* GEM_TEST_PATTERN_1_PLUS */
  "(01)+",                      /* GEM_TEST_PATTERN_0_1_PLUS */
  "(012)+",                     /* GEM_TEST_PATTERN_0_1_2_PLUS */
  "exact:0",                    /* GEM_TEST_PATTERN_EXACT_0 */
  "exact:1",                    /* GEM_TEST_PATTERN_EXACT_1 */
  "exact:01",                   /* GEM_TEST_PATTERN_EXACT_0_1 */
  "exact:012",                  /* GEM_TEST_PATTERN_EXACT_0_1_2 */
  "exact:0+",                   /* GEM_TEST_PATTERN_EXACT_0_PLUS */
  "exact:1+",                   /* GEM_TEST_PATTERN_EXACT_1_PLUS */
  "exact:(01)+",                /* GEM_TEST_PATTERN_EXACT_0_1_PLUS */
  "exact:(012)+",               /* GEM_TEST_PATTERN_EXACT_0_1_2_PLUS */
  "none",                       /* GEM_TEST_PATTERN_NONE */
  "any",                        /* GEM_TEST_PATTERN_ANY */
  "any-expand",                 /* GEM_TEST_PATTERN_ANY_EXPAND */
  "any-mod2",                   /* GEM_TEST_PATTERN_ANY_MOD2 */
  "any-mod16",                  /* GEM_TEST_PATTERN_ANY_MOD16 */
  "any-mod128",                 /* GEM_TEST_PATTERN_ANY_MOD128 */
  "any-mod1024",                /* GEM_TEST_PATTERN_ANY_MOD1024 */
  "any-mod10240",               /* GEM_TEST_PATTERN_ANY_MOD10240 */
  "wacky",                      /* GEM_TEST_PATTERN_WACKY */
  "imfeelinglucky",             /* GEM_TEST_PATTERN_IMFEELINGLUCKY */
};

/**
 * gem_test_pattern_get_name:
 * @test_pat: a #gem_test_Pattern
 *
 * Returns: the name of @test_pat.
 */
const gchar *
gem_test_pattern_get_name (gem_test_Pattern test_pat)
{
  g_return_val_if_fail (test_pat >= GEM_TEST_PATTERN_0, NULL);
  g_return_val_if_fail (test_pat <= GEM_TEST_PATTERN_IMFEELINGLUCKY, NULL);

  g_assert_cmpint (nelementsof (gem_test_pattern_names),
                   ==, GEM_TEST_PATTERN_IMFEELINGLUCKY + 1);

  return gem_test_pattern_names[test_pat];
}

/**
 * gem_test_pattern_by_name:
 * @name: the name of a test pattern
 * @test_pat: (out): the corresponding #gem_test_Pattern if any
 *
 * Returns: %TRUE if successful or %FALSE otherwise (no such test pattern).
 */
gboolean
gem_test_pattern_by_name (const gchar *name,
                          gem_test_Pattern *test_pat)
{
  guint i;

  for (i = 0; i < nelementsof (gem_test_pattern_names); i++)
    {
      if (g_str_equal (name, gem_test_pattern_names[i]))
        {
          tryset (test_pat, i);
          return TRUE;
        }
    }

  return FALSE;
}

/**
 * gem_test_pattern:
 * @test_pat: a #gem_test_Pattern
 *
 * Returns: The #gem_PatternConfig corresponding to @test_pat.
 */
gem_PatternConfig
gem_test_pattern (gem_test_Pattern test_pat)
{
  gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
  switch (test_pat)
    {
    case GEM_TEST_PATTERN_0:
      PAT_INIT_CONFIG (&cfg, pat_0);
      break;
    case GEM_TEST_PATTERN_1:
      PAT_INIT_CONFIG (&cfg, pat_1);
      break;
    case GEM_TEST_PATTERN_0_1:
      PAT_INIT_CONFIG (&cfg, pat_0_1);
      break;
    case GEM_TEST_PATTERN_0_1_2:
      PAT_INIT_CONFIG (&cfg, pat_0_1_2);
      break;
    case GEM_TEST_PATTERN_0_1_2_3:
      PAT_INIT_CONFIG (&cfg, pat_0_1_2_3);
      break;
    case GEM_TEST_PATTERN_0_1_2_3_4:
      PAT_INIT_CONFIG (&cfg, pat_0_1_2_3_4);
      break;
    case GEM_TEST_PATTERN_0_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_0_plus);
      break;
    case GEM_TEST_PATTERN_1_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_1_plus);
      break;
    case GEM_TEST_PATTERN_0_1_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_0_1_plus);
      break;
    case GEM_TEST_PATTERN_0_1_2_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_0_1_2_plus);
      break;
    case GEM_TEST_PATTERN_EXACT_0: /* same as "0" */
      PAT_INIT_CONFIG (&cfg, pat_0);
      break;
    case GEM_TEST_PATTERN_EXACT_1: /* same as "1" */
      PAT_INIT_CONFIG (&cfg, pat_1);
      break;
    case GEM_TEST_PATTERN_EXACT_0_1:
      PAT_INIT_CONFIG (&cfg, pat_exact_0_1);
      break;
    case GEM_TEST_PATTERN_EXACT_0_1_2:
      PAT_INIT_CONFIG (&cfg, pat_exact_0_1_2);
      break;
    case GEM_TEST_PATTERN_EXACT_0_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_exact_0_plus);
      break;
    case GEM_TEST_PATTERN_EXACT_1_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_exact_1_plus);
      break;
    case GEM_TEST_PATTERN_EXACT_0_1_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_exact_0_1_plus);
      break;
    case GEM_TEST_PATTERN_EXACT_0_1_2_PLUS:
      PAT_INIT_CONFIG (&cfg, pat_exact_0_1_2_plus);
      break;
    case GEM_TEST_PATTERN_NONE:
      break;
    case GEM_TEST_PATTERN_ANY:
      cfg.func = pat_stateless_any;
      break;
    case GEM_TEST_PATTERN_ANY_EXPAND:
      cfg.func = pat_stateless_any_expand;
      break;
    case GEM_TEST_PATTERN_ANY_MOD2:
      PAT_MOD_INIT_CONFIG (&cfg, pat_mod_init_2);
      break;
    case GEM_TEST_PATTERN_ANY_MOD16:
      PAT_MOD_INIT_CONFIG (&cfg, pat_mod_init_16);
      break;
    case GEM_TEST_PATTERN_ANY_MOD128:
      PAT_MOD_INIT_CONFIG (&cfg, pat_mod_init_128);
      break;
    case GEM_TEST_PATTERN_ANY_MOD1024:
      PAT_MOD_INIT_CONFIG (&cfg, pat_mod_init_1024);
      break;
    case GEM_TEST_PATTERN_ANY_MOD10240:
      PAT_MOD_INIT_CONFIG (&cfg, pat_mod_init_10240);
      break;
    case GEM_TEST_PATTERN_WACKY:
      cfg.func = pat_stateless_wacky;
      break;
    case GEM_TEST_PATTERN_IMFEELINGLUCKY:
      test_pat = g_random_int_range (GEM_TEST_PATTERN_NONE,
                                     GEM_TEST_PATTERN_IMFEELINGLUCKY);
      return gem_test_pattern (test_pat);
    default:
      g_assert_not_reached ();
    }
  return cfg;
}
