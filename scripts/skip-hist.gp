#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load 'init.gp'
@INIT_HIST

NAME = 'skip'
PREFIX = NAME.'.out/0-9.01234.'
set output NAME.'-hist.pdf'

set style histogram gap 1
set size ratio 0.45
set xtics format '%g' font ',8' offset 0,.5
set format y '%g'
set ytics font ',8'

TITLE = "Recovering from one failure at the middle of processing\n\
{/*.8 {/:Bold Pattern \"abcde\", Types a-j, W.Size 1000}}"

SUBTITLE = sprintf ('{/*.8 %s}',\
  '(50 runs, 1M events per run)')

#XTICLABELS = 'xticlabels(stringcolumn(12)."\n".stringcolumn(18))'
XTICLABELS = 'xticlabels(stringcolumn(12))'

# set multiplot layout 2,2 rowsfirst title TITLE."\n".SUBTITLE\
#   margins .1, .98, .13, .85 spacing .1,.1

set multiplot layout 2,2 rowsfirst\
  margins .09, .99, .13, .99 spacing .065,.065


unset xlabel
set ylabel 'Recover (% runtime)'
set yrange [0:.5]
USING = 'using ($7/$1*100):@XTICLABELS with hist'

set key at 1,.5 samplen 2.5 font ',8' width -15

set label 1 at 0,.07 '-57%' center font ',8' offset 0,char .5
set label 2 at 1,.09 '-55%' center font ',8' offset 0,char .5
set label 3 at 2,.13 '-30%' center font ',8' offset 0,char .5
set label 4 at 3,.19 '-21%' center font ',8' offset 0,char .5
set label 5 at 4,.35 '-11%' center font ',8' offset 0,char .5
set label 6 at 5,.5 'W.Slide 50' right font ',8'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'50.8.avg' @USING title 'skip off',\
    PREFIX.'50.8.skip.avg' @USING title 'skip on',\
  newhistogram lt 2 at 1,\
    PREFIX.'50.16.avg' @USING notitle,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'50.32.avg' @USING notitle,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'50.64.avg' @USING notitle,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'50.128.avg' @USING notitle,\
    PREFIX.'50.128.skip.avg' @USING notitle

set label 1 at 0,0.07 '-43%' center font ',8' offset 0,char .5
set label 2 at 1,0.08 '-50%' center font ',8' offset 0,char .5
set label 3 at 2,0.12 '-42%' center font ',8' offset 0,char .5
set label 4 at 3,0.19 '-21%' center font ',8' offset 0,char .5
set label 5 at 4,0.38 '-10%' center font ',8' offset 0,char .5
unset label 6
set label 6 at 5,.5 'W.Slide 800' right font ',8'
unset ylabel
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'800.8.avg' @USING notitle,\
    PREFIX.'800.8.skip.avg' @USING notitle,\
  newhistogram lt 2 at 1,\
    PREFIX.'800.16.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'800.32.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'800.64.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'800.128.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle,\



set yrange [0:1]
set xlabel 'Save interval'
set ylabel 'Saves (% runtime)'
set label 1 at 0,.8 '+11%' center font ',8' offset 0,char .5
set label 2 at 1,.5 '+13%' center font ',8' offset 0,char .5
set label 3 at 2,.35 '+13%' center font ',8' offset 0,char .5
set label 4 at 3,.28 '+13%' center font ',8' offset 0,char .5
set label 5 at 4,.32 '+18%' center font ',8' offset 0,char .5
set label 6 at 5,1 'W.Slide 50' right font ',8'
USING = 'using ($4/$1*100):@XTICLABELS with hist'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'50.8.avg' @USING notitle,\
    PREFIX.'50.8.skip.avg' @USING notitle,\
  newhistogram lt 2 at 1,\
    PREFIX.'50.16.avg' @USING notitle,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'50.32.avg' @USING notitle,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'50.64.avg' @USING notitle,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'50.128.avg' @USING notitle,\
    PREFIX.'50.128.skip.avg' @USING notitle,\

unset ylabel
set label 1 at 0,.63 '+110%' center font ',8' offset 0,char .5
set label 2 at 1,.74 '+100%' center font ',8' offset 0,char .5
set label 3 at 2,.6 '+114%' center font ',8' offset 0,char .5
set label 4 at 3,.52 '+136%' center font ',8' offset 0,char .5
set label 5 at 4,.49 '+158%' center font ',8' offset 0,char .5
set label 6 at 5,1 'W.Slide 800' right font ',8'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'800.8.avg' @USING notitle,\
    PREFIX.'800.8.skip.avg' @USING notitle,\
  newhistogram lt 2 at 1,\
    PREFIX.'800.16.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'800.32.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'800.64.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'800.128.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle

unset multiplot


unset logscale
set yrange[0:5000]
unset xlabel

set key at 1,5000
# set multiplot layout 2,2 rowsfirst title TITLE."\n".SUBTITLE\
#   margins .1, .98, .13, .85 spacing .1,.1

set multiplot layout 2,2 rowsfirst\
  margins .09, .99, .13, .99 spacing .065,.065

set format y '%0.s%c'
set ylabel 'Events replayed'

set label 1 at 0,1144 '-72%' center font ',8' offset 0,char .5
set label 2 at 1,1339 '-62%' center font ',8' offset 0,char .5
set label 3 at 2,1753 '-47%' center font ',8' offset 0,char .5
set label 4 at 3,2374 '-34%' center font ',8' offset 0,char .5
set label 5 at 4,3993 '-20%' center font ',8' offset 0,char .5
set label 6 at 5,5000 'W.Slide 50' right font ',8'
USING = 'using ($37):@XTICLABELS with hist'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'50.8.avg' @USING title 'skip off',\
    PREFIX.'50.8.skip.avg' @USING title 'skip on',\
  newhistogram lt 2 at 1,\
    PREFIX.'50.16.avg' @USING notitle,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'50.32.avg' @USING notitle,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'50.64.avg' @USING notitle,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'50.128.avg' @USING notitle,\
    PREFIX.'50.128.skip.avg' @USING notitle,\

unset ylabel
set label 1 at 0,800 '-42%' center font ',8' offset 0,char .5
set label 2 at 1,960 '-56%' center font ',8' offset 0,char .5
set label 3 at 2,1328 '-41%' center font ',8' offset 0,char .5
set label 4 at 3,2096 '-27%' center font ',8' offset 0,char .5
set label 5 at 4,4048 '-13%' center font ',8' offset 0,char .5
set label 6 at 5,5000 'W.Slide 800' right font ',8'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'800.8.avg' @USING notitle,\
    PREFIX.'800.8.skip.avg' @USING notitle,\
  newhistogram lt 2 at 1,\
    PREFIX.'800.16.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'800.32.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'800.64.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'800.128.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle


set yrange[0:10000]
set format y '%.0s%c'
set xlabel 'Save interval'
set ylabel 'Max. events kept'

set label 1 at 0,1582 '-27%' center font ',8' offset 0,char .5
set label 2 at 1,1981 '-30%' center font ',8' offset 0,char .5
set label 3 at 2,2773 '-30%' center font ',8' offset 0,char .5
set label 4 at 3,4279 '-19%' center font ',8' offset 0,char .5
set label 5 at 4,7227 '-11%' center font ',8' offset 0,char .5
set label 6 at 5,10000 'W.Slide 50' right font ',8'

USING = 'using ($34):@XTICLABELS with hist'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'50.8.avg' @USING notitle,\
    PREFIX.'50.8.skip.avg' @USING notitle,\
  newhistogram lt 2 at 1,\
    PREFIX.'50.16.avg' @USING notitle,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'50.32.avg' @USING notitle,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'50.64.avg' @USING notitle,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'50.128.avg' @USING notitle,\
    PREFIX.'50.128.skip.avg' @USING notitle


unset ylabel
set label 1 at 0,1566 '-15%' center font ',8' offset 0,char .5
set label 2 at 1,2010 '-12%' center font ',8' offset 0,char .5
set label 3 at 2,2862 '-28%' center font ',8' offset 0,char .5
set label 4 at 3,4520 '-17%' center font ',8' offset 0,char .5
set label 5 at 4,7804 '-9%' center font ',8' offset 0,char .5
set label 6 at 5,10000 'W.Slide 800' right font ',8'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'800.8.avg' @USING notitle,\
    PREFIX.'800.8.skip.avg' @USING notitle,\
  newhistogram lt 2 at 1,\
    PREFIX.'800.16.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'800.32.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'800.64.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'800.128.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle

unset multiplot
