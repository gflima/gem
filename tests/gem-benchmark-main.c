/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"
#include "gem-benchmark.h"

/* The name of this program for pretty-printing.  */
#define PROGRAM_NAME "gem-benchmark"

/* Prints formatted error message.  */
static G_GNUC_PRINTF (1, 2) void
error (const gchar *fmt, ...)
{
  va_list args;

  va_start (args, fmt);
  g_fprintf (stderr, "%s: ", g_get_prgname ());
  g_vfprintf (stderr, fmt, args);
  g_fprintf (stderr, "\n");

  va_end (args);
}

/* Prints usage message and exits.  */
static void
usage (gint status)
{
  if (status != EXIT_SUCCESS)
    g_printerr ("Try '%s --help' for more information.\n",
                g_get_prgname ());
  exit (status);
}

/* Prints custom benchmark results.  */
static void
print_custom_results (const gem_BenchmarkResults *res,
                      gchar **fields)
{
# define RES_MAP(name, type, field)\
  {name, G_VARIANT_TYPE_##type, offsetof (gem_BenchmarkResults, field)}

  static struct
  {
    const gchar *name;
    const GVariantType *type;
    gsize offset;
  } res_map[] =
  {
    /* opts */
    RES_MAP ("opts.sample.seed",        UINT32, opts.sample.seed),
    RES_MAP ("opts.sample.input-size",  UINT32, opts.sample.input_size),
    RES_MAP ("opts.sample.win-size",    UINT64, opts.sample.win_size),
    RES_MAP ("opts.sample.win-slide",   UINT64, opts.sample.win_slide),
    RES_MAP ("opts.sample.win-max-open",
                                        UINT32, opts.sample.win_max_open),
    RES_MAP ("opts.sample.win-overlap-area",
                                        UINT32,
                                        opts.sample.win_overlap_area),
    RES_MAP ("opts.ack-countdown",      UINT32, opts.ack_countdown),

    /* opts aliases */
    RES_MAP ("win-size",                UINT64, opts.sample.win_size),
    RES_MAP ("win-slide",               UINT64, opts.sample.win_slide),
    RES_MAP ("win-max-open",            UINT32, opts.sample.win_max_open),
    RES_MAP ("win-overlap-area",        UINT32,
                                        opts.sample.win_overlap_area),
    RES_MAP ("ack-countdown",           UINT32, opts.ack_countdown),

    /* stats */
    RES_MAP ("stats.evts-pushed",       UINT32, stats.evts_pushed),
    RES_MAP ("stats.evts-skipped",      UINT32, stats.evts_skipped),
    RES_MAP ("stats.evts-popped",       UINT32, stats.evts_popped),
    RES_MAP ("stats.wins-allocated",    UINT32, stats.wins_allocated),
    RES_MAP ("stats.wins-opened",       UINT32, stats.wins_opened),
    RES_MAP ("stats.wins-closed",       UINT32, stats.wins_closed),
    RES_MAP ("stats.wins-disposed",     UINT32, stats.wins_disposed),
    RES_MAP ("stats.wins-max-opened",   UINT32, stats.wins_max_opened),
    RES_MAP ("stats.wins-max-closed",   UINT32, stats.wins_max_opened),
    RES_MAP ("stats.wins-max-disposed", UINT32, stats.wins_max_opened),

    /* stats aliases */
    RES_MAP ("evts-pushed",             UINT32, stats.evts_pushed),
    RES_MAP ("evts-skipped",            UINT32, stats.evts_skipped),
    RES_MAP ("evts-popped",             UINT32, stats.evts_popped),
    RES_MAP ("wins-allocated",          UINT32, stats.wins_allocated),
    RES_MAP ("wins-opened",             UINT32, stats.wins_opened),
    RES_MAP ("wins-closed",             UINT32, stats.wins_closed),
    RES_MAP ("wins-disposed",           UINT32, stats.wins_disposed),
    RES_MAP ("wins-max-opened",         UINT32, stats.wins_max_opened),
    RES_MAP ("wins-max-closed",         UINT32, stats.wins_max_opened),
    RES_MAP ("wins-max-disposed",       UINT32, stats.wins_max_opened),

    /* top-level */
    RES_MAP ("evts-in",                 UINT32, evts_in),
    RES_MAP ("evts-out",                UINT32, evts_out),
    RES_MAP ("duplicates",              UINT32, duplicates),
    RES_MAP ("inq-max-length",          UINT32, inq_max_length),
    RES_MAP ("outq-max-length",         UINT32, outq_max_length),
    RES_MAP ("accum-time",              UINT64, accum_time),
    RES_MAP ("runtime",                 UINT64, runtime),
    RES_MAP ("accum-runtime",           UINT64, accum_runtime),
    RES_MAP ("restores",                UINT32, restores),
    RES_MAP ("replayed",                UINT32, replayed),
    RES_MAP ("replayed-dups",           UINT32, replayed_dups),
    RES_MAP ("restore-runtime",         UINT64, restore_runtime),
    RES_MAP ("acks",                    UINT32, acks),
    RES_MAP ("saves",                   UINT32, saves),
    RES_MAP ("saves-failed",            UINT32, saves_failed),
    RES_MAP ("saves-out-ids",           UINT32, saves_out_ids),
    RES_MAP ("saves-skip-pairs",        UINT32, saves_skip_pairs),
    RES_MAP ("saves-skip-pairs-range",  UINT32, saves_skip_pairs_range),
    RES_MAP ("saves-runtime",           UINT64, saves_runtime),
  };
  guint i, j, n;

  g_return_if_fail (res);
  g_return_if_fail (fields);

  n = 0;
  for (i = 0; fields[i] != NULL; i++)
    {
      gboolean found;
      const GVariantType *type;
      gsize offset;

      found = FALSE;
      for (j = 0; j < nelementsof (res_map); j++)
        {
          if (g_str_equal (fields[i], res_map[j].name))
            {
              found = TRUE;
              type = res_map[j].type;
              offset = res_map[j].offset;
              break;
            }
        }

      if (!found)
        {
          g_warning ("Unknown result field '%s'", fields[i]);
          continue;
        }

      if (n++ > 0)
        g_print ("\t");

      if (type == G_VARIANT_TYPE_UINT32)
        g_print ("%u", *(guint32 *)((gchar *) res + offset));
      else if (type == G_VARIANT_TYPE_UINT64)
        g_print ("%"G_GUINT64_FORMAT, *(guint64 *)((gchar *) res + offset));
      else
        g_assert_not_reached ();
    }
  g_print ("\n");
}

/* Prints benchmark results.  */
static void
print_results (const gem_BenchmarkResults *res)
{
  GList *ell;

  g_return_if_fail (res);

  if (res->opts.custom_results != NULL)
    {
      print_custom_results (res, res->opts.custom_results);
      return;
    }

  g_print ("\n--- Gem Benchmark Results ---\n\n");
  g_print ("Options:\n\
  sample-seed: %u\n\
  sample-input-size: %u\n\
  sample-win-size: %"GEM_TIME_FORMAT"\n\
  sample-win-slide: %"GEM_TIME_FORMAT"\n\
  sample-win-density: %g\n\
  sample-win-max-open: %u\n\
  sample-win-overlap-area: %u\n\
  ack-countdown: %u\n\
  ack: %"GEM_TIME_FORMAT"-%"GEM_TIME_FORMAT"\n\
  time: %"GEM_TIME_FORMAT"-%"GEM_TIME_FORMAT"\n\
  type: %u-%u\n\
  test-pat: %s\n\
  output-file: %s\n\
",
           res->opts.sample.seed,
           res->opts.sample.input_size,
           GEM_TIME_ARGS (res->opts.sample.win_size),
           GEM_TIME_ARGS (res->opts.sample.win_slide),
           res->opts.sample.win_density,
           res->opts.sample.win_max_open,
           res->opts.sample.win_overlap_area,
           res->opts.ack_countdown,
           GEM_TIME_ARGS (res->opts.ack_min),
           GEM_TIME_ARGS (res->opts.ack_max),
           GEM_TIME_ARGS (res->opts.time_min),
           GEM_TIME_ARGS (res->opts.time_max),
           res->opts.type_min,
           res->opts.type_max,
           gem_test_pattern_get_name (res->opts.test_pat),
           res->opts.output_file);

  ell = res->opts.failure_points->head;
  g_print ("  failure-points: ");
  if (ell != NULL)
    {
      g_print ("%d", GPOINTER_TO_INT (ell->data));
      for (ell = ell->next; ell != NULL; ell = ell->next)
        g_print (", %d", GPOINTER_TO_INT (ell->data));
      g_print ("\n");
    }
  else
    {
      g_print ("(none)\n");
    }

#define TIMES(x, t)   ((t) <= 0. ? 0. : ((gdouble)(x) / (gdouble)(t)))
#define PERCENT(x, t) (TIMES ((x), (t)) * 100.)
#define SECONDS(us)   ((gdouble)(us) / G_USEC_PER_SEC)

  g_print ("\
  op-save: %s\n\
  op-skip: %s\n\
  op-stats: %s\n\
  op-trace: %s\n\
",
           strbool (res->opts.op_save),
           strbool (res->opts.op_skip),
           strbool (res->opts.op_stats),
           strbool (res->opts.op_trace));

  if (res->opts.op_stats)
    {
      g_print ("\nOperator:\n\
  evts-pushed: %u\n\
  evts-skipped: %u (%.1f%%)\n\
  evts-popped: %u\n\
  wins-allocated: %u\n\
  wins-opened: %u\n\
  wins-closed: %u\n\
  wins-disposed: %u\n\
  wins-max-opened: %u\n\
  wins-max-closed: %u\n\
  wins-max-disposed: %u\n\
",
               res->stats.evts_pushed,
               res->stats.evts_skipped,
               PERCENT (res->stats.evts_skipped, res->stats.evts_pushed),
               res->stats.evts_popped,
               res->stats.wins_allocated,
               res->stats.wins_opened,
               res->stats.wins_closed,
               res->stats.wins_disposed,
               res->stats.wins_max_opened,
               res->stats.wins_max_closed,
               res->stats.wins_max_disposed);
    }

  g_print ("\nEvents:\n\
  in: %u\n\
  out: %u (~%.2fx)\n\
",
           res->evts_in,
           res->evts_out,
           TIMES (res->evts_out, res->evts_in));

  if (res->opts.duplicates)
    {
      g_print ("\
  dups: %u (%.1f%%)\n\
",
               res->duplicates,
               PERCENT (res->duplicates, res->evts_out));
    }

  g_print ("\
  inq-max-len: %u\n\
  outq-max-len: %u\n\
",
           res->inq_max_length,
           res->outq_max_length);

  if (res->evts_in > 0)
    {
      gdouble ideal_throughput;
      gdouble ideal_latency;
      gdouble accum_throughput;
      gdouble accum_latency;

      ideal_throughput = res->evts_in / SECONDS (res->accum_time);
      ideal_latency = (gdouble) res->accum_time / res->evts_in;

      accum_throughput = res->evts_in / SECONDS (res->accum_runtime);
      accum_latency = (gdouble) res->accum_runtime / res->evts_in;

      g_print ("\
  runtime: %"GEM_TIME_FORMAT"\n\
  throughput: %g evt/s\n\
  latency: %g us/evt\n\
  ideal-time: %"GEM_TIME_FORMAT"\n\
  ideal-throughput: %g evt/s\n\
  ideal-latency: %g us/evt\n\
  accum-time: %"GEM_TIME_FORMAT" (~%.2fx)\n\
  accum-throughput: %g evt/s (%.1f%%)\n\
  accum-latency: %g us/evt (~%.2fx)\n\
",
               GEM_TIME_ARGS (res->runtime),
               res->evts_in / SECONDS (res->runtime),
               (gdouble) res->runtime / res->evts_in,
               GEM_TIME_ARGS (res->accum_time),
               ideal_throughput,
               ideal_latency,
               GEM_TIME_ARGS (res->accum_runtime),
               TIMES (res->accum_runtime, res->accum_time),
               accum_throughput,
               PERCENT (accum_throughput, ideal_throughput),
               accum_latency,
               TIMES (accum_latency, ideal_latency));
    }

  if (res->restores > 0)
    {
      g_print ("\nRestores:\n\
  restores: %u\n\
  replayed: %u (%.1f%%)\n\
  replayed_dups: %u (%.1f%%)\n\
  restore_runtime: %"GEM_TIME_FORMAT" (%.1f%%)\n\
",
               res->restores,
               res->replayed,
               PERCENT (res->replayed, res->evts_in),
               res->replayed_dups,
               PERCENT (res->replayed_dups, res->evts_out),
               GEM_TIME_ARGS (res->restore_runtime),
               PERCENT (res->restore_runtime, res->runtime));
    }

  if (res->opts.ack_countdown > 0)
    {
      g_print ("\nSaves:\n\
  acks: %u\n\
  saves: %u\n\
  saves-failed: %u (%.1f%%)\n\
  saves-out-ids: %u (avg %.1f)\n\
  saves-skip-pairs: %u (avg %.1f)\n\
  saves-skip-pairs-range: %u (avg %.1f)\n\
  saves-runtime: %"GEM_TIME_FORMAT" (%.1f%%)\n\
",
               res->acks,
               res->saves,
               res->saves_failed,
               PERCENT (res->saves_failed, res->saves),
               res->saves_out_ids,
               TIMES (res->saves_out_ids, res->saves),
               res->saves_skip_pairs,
               TIMES (res->saves_skip_pairs, res->saves),
               res->saves_skip_pairs_range,
               TIMES (res->saves_skip_pairs_range,
                      res->saves_skip_pairs),
               GEM_TIME_ARGS (res->saves_runtime),
               PERCENT (res->saves_runtime, res->runtime));
    }

  g_print ("\n");
}


gint
main (gint argc, gchar **argv)
{
  gem_BenchmarkOptions _opts;
  gem_BenchmarkOptions *opts;
  GError *err = NULL;
  guint i;

  g_set_prgname (PROGRAM_NAME);

  if (unlikely (!gem_benchmark_options_parse (argc, argv, &_opts, &err)))
    {
      g_assert_nonnull (err);
      error ("%s", err->message);
      g_error_free (err);
      usage (EXIT_FAILURE);
    }

  opts = &_opts;
  if (opts->op_trace)
    {
      g_assert (g_setenv ("G_MESSAGES_DEBUG", "all", TRUE));
    }

  for (i = 0; i < opts->runs; i++)
    {
      gem_Benchmark *run;

      run = gem_benchmark_new (opts);
      gem_benchmark_run (run);

      if (opts->show_results)
        print_results (gem_benchmark_get_results (run));

      gem_benchmark_unref (run);
    }

  gem_benchmark_options_clear (opts);

  exit (EXIT_SUCCESS);
}
