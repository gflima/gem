/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  gem_Operator *op;
  gem_OperatorFlags flags;

  op = gem_operator_new (NULL, 0, 1, 1);
  flags = gem_operator_get_flags (op);
  g_assert (flags == GEM_OPERATOR_DEFAULT_FLAGS);
  gem_operator_unref (op);

  op = gem_operator_new_with_flags (NULL, 0, 1, 1, 0);
  flags = gem_operator_get_flags (op);
  g_assert (flags == 0);
  gem_operator_unref (op);

  exit (EXIT_SUCCESS);
}
