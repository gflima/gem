/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  GArray *array;
  gem_EventID id;

  gem_Savepoint *save;
  GArray *output_ids;
  GArray *skip_ranges;

  save = gem_savepoint_new (0xdeafbeef, NULL, NULL);
  g_assert (gem_savepoint_get_start_id (save) == 0xdeafbeef);
  g_assert_null (gem_savepoint_get_output_ids (save));
  g_assert_null (gem_savepoint_get_skip_ranges (save));
  gem_savepoint_unref (save);

  array = g_array_new (FALSE, FALSE, sizeof (gem_EventID));

  id = 5;
  g_array_append_val (array, id);

  id = 8;
  g_array_append_val (array, id);

  save = gem_savepoint_new (32, array, NULL);
  g_assert (gem_savepoint_get_start_id (save) == 32);
  g_assert_null (gem_savepoint_get_skip_ranges (save));

  output_ids = gem_savepoint_get_output_ids (save);
  g_assert (output_ids->len == 2);
  g_assert (g_array_index (output_ids, gem_EventID, 0) == 5);
  g_assert (g_array_index (output_ids, gem_EventID, 1) == 8);
  gem_savepoint_unref (save);

  save = gem_savepoint_new (1, NULL, array);
  g_assert (gem_savepoint_get_start_id (save) == 1);
  g_assert_null (gem_savepoint_get_output_ids (save));
  skip_ranges = gem_savepoint_get_skip_ranges (save);
  g_assert (skip_ranges->len == 2);
  g_assert (g_array_index (skip_ranges, gem_EventID, 0) == 5);
  g_assert (g_array_index (skip_ranges, gem_EventID, 1) == 8);
  gem_savepoint_unref (save);

  exit (EXIT_SUCCESS);
}
