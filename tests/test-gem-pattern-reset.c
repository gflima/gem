/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

static gem_PatternAction
collect_and_expand (unused (gem_Pattern *pat),
                    unused (gem_Event *evt),
                    gem_EventID *id,
                    gem_EventType *type,
                    gem_EventTime *time,
                    unused (gpointer data))
{
  *id = 0;
  *type = 0;
  *time = 0;
  return GEM_PATTERN_COLLECT_AND_EXPAND;
}

gint
main (void)
{
  gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
  gem_Pattern *pat;

  gem_test_Rand *rand;
  gem_EventArray *input;
  gem_Event *out;
  guint i;

  cfg.func = collect_and_expand;
  pat = gem_pattern_new (&cfg);

  rand = gem_test_rand_new ();
  input = gem_test_rand_event_array (rand, 10);

  for (i = 0; i < input->len; i++)
    {
      gem_PatternAction act;
      out = gem_pattern_match (pat, gem_event_array_index (input, i), &act);
      g_assert_null (out);
      g_assert (act == GEM_PATTERN_COLLECT_AND_EXPAND);
    }

  gem_pattern_reset (pat);

  out = gem_pattern_match (pat, gem_event_array_index (input, 0), NULL);
  g_assert_null (out);

  out = gem_pattern_match_complete (pat);
  g_assert_nonnull (out);
  g_assert (gem_event_get_id (out) == 0);
  g_assert (gem_event_get_type (out) == 0);
  g_assert (gem_event_get_time (out) == 0);
  g_assert (gem_event_has_match (out));
  g_assert (out->match->len == 1);

  g_assert (gem_event_array_index (out->match, 0)
            == gem_event_array_index (input, 0));

  gem_event_array_unref (input);
  gem_event_unref (out);
  gem_pattern_unref (pat);

  exit (EXIT_SUCCESS);
}
