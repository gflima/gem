/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include <config.h>
#include "gem.h"

/**
 * SECTION: gem-version
 * @Title: Library Version
 * @Short_Description: compile-time and run-time version checks
 */

/**
 * GEM_VERSION:
 *
 * The version of gem available at compile-time, encoded using
 * GEM_VERSION_ENCODE().
 */

/**
 * GEM_VERSION_MAJOR:
 *
 * The major component of the version of gem available at compile-time.
 */

/**
 * GEM_VERSION_MINOR:
 *
 * The minor component of the version of gem available at compile-time.
 */

/**
 * GEM_VERSION_MICRO:
 *
 * The micro component of the version of gem available at compile-time.
 */

/**
 * GEM_VERSION_STRING:
 *
 * A human-readable string literal containing the version of gem available
 * at compile-time, in the form of "X.Y.Z".
 */

/**
 * GEM_VERSION_ENCODE:
 * @major: the major component of the version number
 * @minor: the minor component of the version number
 * @micro: the micro component of the version number
 *
 * This macro encodes the given gem version into an integer.  The numbers
 * returned by %GEM_VERSION and gem_version() are encoded using this macro.
 * Two encoded version numbers can be compared as integers.  The encoding
 * ensures that later versions compare greater than earlier versions.
 *
 * Returns: The encoded version.
 */

/**
 * GEM_VERSION_STRINGIZE:
 * @major: the major component of the version number
 * @minor: the minor component of the version number
 * @micro: the micro component of the version number
 *
 * This macro encodes the given gem version into an string.  The numbers
 * returned by %GEM_VERSION_STRING and gem_version_string() are encoded
 * using this macro.  The parameters to this macro must expand to numerical
 * literals.
 *
 * Returns: A string literal containing the version.
 */

/**
 * gem_version:
 *
 * Returns the version of gem library encoded in a single integer.
 *
 * Returns: The encoded version.
 */
gint
gem_version (void)
{
  return GEM_VERSION;
}

/**
 * gem_version_string:
 *
 * Returns the version of the gem library as a human-readable string
 * of the form "X.Y.Z".
 *
 * Returns: The version string.
 */
const gchar *
gem_version_string (void)
{
  return GEM_VERSION_STRING;
}
