#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load 'skip.gp'
set output NAME.'-hist-3.pdf'

set yrange[0:5000]
set xlabel 'Save interval'

set key at 0.1,5000
set format y '%0.s%c'
set ylabel 'Events replayed'

set label 1 at 0,1144 '-72%' center offset 0,char .5
set label 2 at 1,1339 '-62%' center offset 0,char .5
set label 3 at 2,1753 '-47%' center offset 0,char .5
set label 4 at 3,2374 '-34%' center offset 0,char .5
set label 5 at 4,3993 '-20%' center offset 0,char .5
set label 6 at 5,5000 'W.Size 1k / W.Slide 50' right
USING = 'using ($37):@XTICLABELS with hist'
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'50.8.avg' @USING title 'skip off',\
    PREFIX.'50.8.skip.avg' @USING title 'skip on',\
  newhistogram lt 2 at 1,\
    PREFIX.'50.16.avg' @USING notitle,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'50.32.avg' @USING notitle,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'50.64.avg' @USING notitle,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'50.128.avg' @USING notitle,\
    PREFIX.'50.128.skip.avg' @USING notitle,\

unset ylabel
set label 1 at 0,800 '-42%' center offset 0,char .5
set label 2 at 1,960 '-56%' center offset 0,char .5
set label 3 at 2,1328 '-41%' center offset 0,char .5
set label 4 at 3,2096 '-27%' center offset 0,char .5
set label 5 at 4,4048 '-13%' center offset 0,char .5
set label 6 at 5,5000 'W.Size 1k / W.Slide 800' right
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'800.8.avg' @USING title 'skip off',\
    PREFIX.'800.8.skip.avg' @USING title 'skip on',\
  newhistogram lt 2 at 1,\
    PREFIX.'800.16.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'800.32.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'800.64.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'800.128.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle
