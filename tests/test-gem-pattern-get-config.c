/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

static void
data_init (gem_Pattern *pat,
           gpointer data)
{
  g_assert_nonnull (pat);
  g_assert_nonnull (data);
}

static void
data_fini (gem_Pattern *pat,
           gpointer data)
{
  g_assert_nonnull (pat);
  g_assert_nonnull (data);
}

gint
main (void)
{
  /* Default configuration.  */
  {
    gem_Pattern *pat;
    gem_PatternConfig cfg;

    pat = gem_pattern_new (NULL);
    cfg = gem_pattern_get_config (pat);
    g_assert (cfg.func == NULL);
    g_assert (cfg.data_init == NULL);
    g_assert (cfg.data_fini == NULL);
    g_assert (cfg.data_size == 0);
    gem_pattern_unref (pat);
  }

  /* Custom configuration.  */
  {
    gem_Pattern *pat;
    gem_PatternConfig cfg;

    cfg.func = (gem_PatternFunc) main;
    cfg.data_init = data_init;
    cfg.data_fini = data_fini;
    cfg.data_size = 10;

    pat = gem_pattern_new (&cfg);

    cfg = gem_pattern_get_config (pat);
    g_assert (cfg.func == (gem_PatternFunc) main);
    g_assert (cfg.data_init == data_init);
    g_assert (cfg.data_fini == data_fini);
    g_assert (cfg.data_size == 10);

    gem_pattern_unref (pat);
  }

  exit (EXIT_SUCCESS);
}
