#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load 'skip.gp'
set output NAME.'-hist-1.pdf'

set xlabel 'Save interval'
set ylabel 'Recover (% runtime)'
set yrange [0:.5]
USING = 'using ($7/$1*100):@XTICLABELS with hist'

set key at 0.1,.5 width -15

set label 1 at 0,.07 '-57%' center offset 0,char .5
set label 2 at 1,.09 '-55%' center offset 0,char .5
set label 3 at 2,.13 '-30%' center offset 0,char .5
set label 4 at 3,.19 '-21%' center offset 0,char .5
set label 5 at 4,.357 '-11%' center offset 0,char .5
set label 6 at 5,.5 'W.Size 1k / W.Slide 50' right
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'50.8.avg' @USING title 'skip off',\
    PREFIX.'50.8.skip.avg' @USING title 'skip on',\
  newhistogram lt 2 at 1,\
    PREFIX.'50.16.avg' @USING notitle,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'50.32.avg' @USING notitle,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'50.64.avg' @USING notitle,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'50.128.avg' @USING notitle,\
    PREFIX.'50.128.skip.avg' @USING notitle

set label 1 at 0,0.07 '-43%' center offset 0,char .5
set label 2 at 1,0.08 '-50%' center offset 0,char .5
set label 3 at 2,0.12 '-42%' center offset 0,char .5
set label 4 at 3,0.19 '-21%' center offset 0,char .5
set label 5 at 4,0.38 '-10%' center offset 0,char .5
unset label 6
set label 6 at 5,.5 'W.Size 1k / W.Slide 800' right
plot\
  newhistogram lt 2 at 0,\
    PREFIX.'800.8.avg' @USING title 'skip off',\
    PREFIX.'800.8.skip.avg' @USING title 'skip on',\
  newhistogram lt 2 at 1,\
    PREFIX.'800.16.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 2 at 2,\
    PREFIX.'800.32.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 2 at 3,\
    PREFIX.'800.64.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 2 at 4,\
    PREFIX.'800.128.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle
