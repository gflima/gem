# configure.ac -- Configure template for Gem.
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

AC_PREREQ([2.62])
AU_GIT_VERSION_GEN([gem])
AC_INIT([Gem],
  gem_version_string,
 [gflima@inf.puc-rio.br],
 [gem],
 [http://gitlab.com/gflima/gem])

AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([build-aux])
AC_USE_SYSTEM_EXTENSIONS
AC_CONFIG_SRCDIR([lib/gem.h])
AC_CONFIG_HEADERS([lib/config.h])

AM_INIT_AUTOMAKE([1.14 gnu -Wall -Werror dist-xz no-dist-gzip])
AM_SILENT_RULES([yes])
m4_ifdef([AM_PROG_AR], [AM_PROG_AR]) dnl Workaround for Automake 1.12

LT_PREREQ([2.2])
LT_INIT([win32-dll])
AU_LIBTOOL_MODULE_LDFLAGS

# API documentation.  Note that gtkdocize greps for ^GTK_DOC_CHECK and
# parses it, so you need to have it on it's own line.
m4_ifdef([GTK_DOC_CHECK],[
GTK_DOC_CHECK([1.20], [--flavour no-tmpl])
], [AM_CONDITIONAL([ENABLE_GTK_DOC], [false])])

# Gem (package) version.
AC_SUBST([GEM_VERSION_MAJOR], gem_version_major)
AC_SUBST([GEM_VERSION_MINOR], gem_version_minor)
AC_SUBST([GEM_VERSION_MICRO], gem_version_micro)
AC_SUBST([GEM_VERSION_STRING], gem_version_string)

# Libgem version.
# - library code modified:            REVISION++
# - interfaces changed/added/removed: REVISION=0, CURRENT++
# - interfaces added:                 AGE++
# - interfaces removed:               AGE=0
AU_LIBTOOL_VERSION([GEM], [0], [0], [0])

# Dependencies.
m4_define([glib_required_version], [2.36])
AC_SUBST([GLIB_REQUIRED_VERSION], glib_required_version)

# Tool checks.
AU_PROG_CC_VISIBILITY
AU_PROG_PKG_CONFIG
AU_PROG_UNIX_TOOLS

# System checks.
AU_SYSTEM
AU_SYSTEM_MINGW

# Configure options.
AU_ARG_ENABLE_COVERAGE
AU_ARG_ENABLE_DEBUG
AU_ARG_ENABLE_VALGRIND

nw=
nw="$nw -Wsystem-headers"     # suppress system headers warnings
nw="$nw -Wpadded"             # our structs are not packed
nw="$nw -Wcast-qual"          # triggered by GLib headers
nw="$nw -Wc++-compat"         # ditto
AU_ARG_ENABLE_WARNINGS([$nw])

# Library functions.
AC_CHECK_LIBM

# Check for GLib.
AU_VERSION_BREAK([glib], glib_required_version)
AU_CHECK_PKG([GLIB],
  glib-2.0 >= glib_required_version,
 [AC_LANG_PROGRAM([[
#include <glib.h>
#if !GLIB_CHECK_VERSION  \
    (GLIB_REQUIRED_MAJOR,\
     GLIB_REQUIRED_MINOR,\
     GLIB_REQUIRED_MICRO)
# error "glib is too old"
#endif
 ]])],
 [AC_LANG_PROGRAM([[]], [[glib_check_version (0, 0, 0);]])])

AU_ARG_ENABLE([glib-checks], [enable on Glib checks], [yes])
AS_IF([test "$enable_glib_checks" = yes], [],
 [GLIB_CHECKS="-DG_DISABLE_CHECKS -DG_DISABLE_ASSERT"
  AC_SUBST([GLIB_CHECKS])])

AC_CONFIG_FILES([
Makefile
doc/Makefile
doc/reference/Makefile
doc/reference/version.xml
lib/Makefile
lib/gemconf.h
tests/Makefile
])

AC_OUTPUT
AC_MSG_NOTICE([summary of main build options:

  version:            ${VERSION}
  host type:          ${host}
  install prefix:     ${prefix}
  compiler:           ${CC}
  cppflags:           ${CPPFLAGS}
  cflags:             ${CFLAGS}
  ldflags:            ${LDFLAGS}
  warning flags:      ${WERROR_CFLAGS} ${WARN_CFLAGS}
  library types:      Shared=${enable_shared}, Static=${enable_static}
  valgrind:           ${VALGRIND}
])
