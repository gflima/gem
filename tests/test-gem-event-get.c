/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
    gem_Event *e1;
    gem_Event *e2;
    gem_EventArray *match;

    e1 = gem_event_new (GEM_EVENT_ID_NONE,
                        GEM_EVENT_TYPE_NONE,
                        GEM_EVENT_TIME_NONE,
                        NULL);

    g_assert_false (gem_event_has_id (e1));
    g_assert_false (gem_event_has_type (e1));
    g_assert_false (gem_event_has_time (e1));
    g_assert_false (gem_event_has_match (e1));

    match = gem_event_array_sized_new (1);
    gem_event_array_add (match, e1);
    gem_event_unref (e1);

    e2 = gem_event_new (3, 2, 1, match);
    gem_event_array_unref (match);

    g_assert (gem_event_has_id (e2));
    g_assert (gem_event_get_id (e2) == 3);
    g_assert (gem_event_has_type (e2));
    g_assert (gem_event_get_type (e2) == 2);
    g_assert (gem_event_has_time (e2));
    g_assert (gem_event_get_time (e2) == 1);
    g_assert (gem_event_has_match (e2));
    g_assert (gem_event_get_match (e2) == match);

    gem_event_unref (e2);

    exit (EXIT_SUCCESS);
}
