/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#ifndef GEM_TEST_H
#define GEM_TEST_H

#include <config.h>
#include <math.h>
#include "aux-glib.h"
#include "gem.h"
#include "gem-private.h"

/**
 * GEM_TEST:
 * @msg: the header message
 *
 * Prints a header for a test.
 */
#define GEM_TEST(msg)                                           \
  G_STMT_START                                                  \
  {                                                             \
    g_print ("#########\n");                                    \
    g_print ("### TEST:%s:%d: %s\n", __FILE__, __LINE__, msg);  \
    g_print ("###\n");                                          \
  }                                                             \
  G_STMT_END

/**
 * gem_test_Pattern:
 * @GEM_TEST_PATTERN_0: Match "0".
 * @GEM_TEST_PATTERN_1: Match "1".
 * @GEM_TEST_PATTERN_0_1: Match "01".
 * @GEM_TEST_PATTERN_0_1_2: Match "012".
 * @GEM_TEST_PATTERN_0_1_2_3: Match "0123".
 * @GEM_TEST_PATTERN_0_1_2_3_4: Match "01234".
 * @GEM_TEST_PATTERN_0_PLUS: Match "0+".
 * @GEM_TEST_PATTERN_1_PLUS: Match "1+".
 * @GEM_TEST_PATTERN_0_1_PLUS: Match "(01)+".
 * @GEM_TEST_PATTERN_0_1_2_PLUS: Match "(012)+".
 * @GEM_TEST_PATTERN_EXACT_0: Match "0", exact.
 * @GEM_TEST_PATTERN_EXACT_1: Match "1", exact.
 * @GEM_TEST_PATTERN_EXACT_0_1: Match "01", exact.
 * @GEM_TEST_PATTERN_EXACT_0_1_2: Match "012", exact.
 * @GEM_TEST_PATTERN_EXACT_0_PLUS: Match "0+", exact.
 * @GEM_TEST_PATTERN_EXACT_1_PLUS: Match "1+", exact.
 * @GEM_TEST_PATTERN_EXACT_0_1_PLUS: Match "(01)+", exact.
 * @GEM_TEST_PATTERN_EXACT_0_1_2_PLUS: Match "(012)", exact.
 * @GEM_TEST_PATTERN_NONE: Match nothing.
 * @GEM_TEST_PATTERN_ANY: Match anything.
 * @GEM_TEST_PATTERN_ANY_EXPAND: Match anything and expand.
 * @GEM_TEST_PATTERN_ANY_MOD2: Match anything every 2 calls
 * @GEM_TEST_PATTERN_ANY_MOD16: Match anything every 16 calls
 * @GEM_TEST_PATTERN_ANY_MOD128: Match anything every 128 calls
 * @GEM_TEST_PATTERN_ANY_MOD1024: Match anything every 1024 calls
 * @GEM_TEST_PATTERN_ANY_MOD10240: Match anything every 10240 calls
 * @GEM_TEST_PATTERN_WACKY: Match or skip anything randomly.
 * @GEM_TEST_PATTERN_IMFEELINGLUCKY: Get a random test pattern.
 *
 * Predefined test patterns.
 */
typedef enum
{
  GEM_TEST_PATTERN_0 = 0,            /* 0              */
  GEM_TEST_PATTERN_1,                /* 1              */
  GEM_TEST_PATTERN_0_1,              /* 01             */
  GEM_TEST_PATTERN_0_1_2,            /* 012            */
  GEM_TEST_PATTERN_0_1_2_3,          /* 0123           */
  GEM_TEST_PATTERN_0_1_2_3_4,        /* 01234          */
  GEM_TEST_PATTERN_0_PLUS,           /* 0+             */
  GEM_TEST_PATTERN_1_PLUS,           /* 1+             */
  GEM_TEST_PATTERN_0_1_PLUS,         /* (01)+          */
  GEM_TEST_PATTERN_0_1_2_PLUS,       /* (012)+         */
  GEM_TEST_PATTERN_EXACT_0,          /* exact:0        */
  GEM_TEST_PATTERN_EXACT_1,          /* exact:1        */
  GEM_TEST_PATTERN_EXACT_0_1,        /* exact:01       */
  GEM_TEST_PATTERN_EXACT_0_1_2,      /* exact:012      */
  GEM_TEST_PATTERN_EXACT_0_PLUS,     /* exact:0+       */
  GEM_TEST_PATTERN_EXACT_1_PLUS,     /* exact:1+       */
  GEM_TEST_PATTERN_EXACT_0_1_PLUS,   /* exact:(01)+    */
  GEM_TEST_PATTERN_EXACT_0_1_2_PLUS, /* exact:(012)+   */
  GEM_TEST_PATTERN_NONE,             /* none           */
  GEM_TEST_PATTERN_ANY,              /* any            */
  GEM_TEST_PATTERN_ANY_EXPAND,       /* any-expand     */
  GEM_TEST_PATTERN_ANY_MOD2,         /* any-mod2       */
  GEM_TEST_PATTERN_ANY_MOD16,        /* any-mod16      */
  GEM_TEST_PATTERN_ANY_MOD128,       /* any-mod128     */
  GEM_TEST_PATTERN_ANY_MOD1024,      /* any-mod1024    */
  GEM_TEST_PATTERN_ANY_MOD10240,     /* any-mod10240   */
  GEM_TEST_PATTERN_WACKY,            /* wacky          */
  GEM_TEST_PATTERN_IMFEELINGLUCKY,   /* imfeelinglucky */
} gem_test_Pattern;

const gchar *
gem_test_pattern_get_name (gem_test_Pattern test_pat);

gboolean
gem_test_pattern_by_name (const gchar *name,
                          gem_test_Pattern *test_pat);

gem_PatternConfig
gem_test_pattern (gem_test_Pattern test_pat);

/**
 * gem_test_Rand:
 *
 * Random event generator.
 */
typedef struct _gem_test_Rand gem_test_Rand;

gem_test_Rand *
gem_test_rand_new (void);

gem_test_Rand *
gem_test_rand_new_with_seed (guint seed);

gem_test_Rand *
gem_test_rand_new_full (gem_EventID start_id,
                        gem_EventType type_min,
                        gem_EventType type_max,
                        gem_EventTime time_min,
                        gem_EventTime time_max);

gem_test_Rand *
gem_test_rand_new_full_with_seed (guint seed,
                                  gem_EventID start_id,
                                  gem_EventType type_min,
                                  gem_EventType type_max,
                                  gem_EventTime time_min,
                                  gem_EventTime time_max);

gem_test_Rand *
gem_test_rand_ref (gem_test_Rand *rand);

void
gem_test_rand_unref (gem_test_Rand *rand);

guint
gem_test_rand_get_seed (const gem_test_Rand *rand);

GRand *
gem_test_rand_get_rand (const gem_test_Rand *rand);

gem_EventType
gem_test_rand_event_type (gem_test_Rand *rand);

gem_EventType
gem_test_rand_event_type_full (gem_test_Rand *rand,
                               gem_EventType type_min,
                               gem_EventType type_max);

gem_EventTime
gem_test_rand_event_time (gem_test_Rand *rand);

gem_EventTime
gem_test_rand_event_time_full (gem_test_Rand *rand,
                               gem_EventTime time_min,
                               gem_EventTime time_max);

gem_Event *
gem_test_rand_event (gem_test_Rand *rand);

gem_Event *
gem_test_rand_event_full (gem_test_Rand *rand,
                          gem_EventID id,
                          gem_EventType type_min,
                          gem_EventType type_max,
                          gem_EventTime time_min,
                          gem_EventTime time_max);

gem_EventArray *
gem_test_rand_event_array (gem_test_Rand *rand,
                           guint len);

gem_EventArray *
gem_test_rand_event_array_full (gem_test_Rand *rand,
                                guint len,
                                gem_EventID start_id,
                                gem_EventType type_min,
                                gem_EventType type_max,
                                gem_EventTime time_min,
                                gem_EventTime time_max);
/**
 * gem_test_RandSample:
 * @seed: the seed that generated the sample
 * @input_size: total number of input events
 * @win_size: window size
 * @win_slide: window slide
 * @win_density: window density of the sample
 * @win_max_open: maximum number of windows open in sample
 * @win_overlap_area: number of overlapping event slots
 *
 * Sample data for #gem_Operator tests.
 */
typedef struct _gem_test_RandSample gem_test_RandSample;

struct _gem_test_RandSample
{
  guint seed;
  guint input_size;
  gem_EventTime win_size;
  gem_EventTime win_slide;
  gdouble win_density;
  guint win_max_open;
  guint win_overlap_area;
};

gem_test_RandSample
gem_test_rand_sample (gem_test_Rand *rand,
                      guint max);

#endif /* GEM_TEST_H */
