/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  GEM_TEST("No-op");
  {
    gem_Operator *op;

    op = gem_operator_new (NULL, 0, 1, 1);
    g_assert_null (gem_operator_pop (op));

    gem_operator_unref (op);
  }

  GEM_TEST("Pop some output events");
  {
    gem_test_Rand *rand;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Event *evt;
    gem_Event *out;

    rand = gem_test_rand_new_full (0, 0, 0, 0, 0);

    cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY);
    op = gem_operator_new (&cfg, 0, 1, 1);

    evt = gem_test_rand_event (rand);
    evt->time = 0;
    g_assert (gem_operator_push (op, evt) == 1);
    gem_event_unref (evt);

    out = gem_operator_pop (op);
    g_assert_nonnull (out);
    g_assert (gem_event_has_match (out));
    g_assert (evt == gem_event_array_index (out->match, 0));
    gem_event_unref (out);

    g_assert_null (gem_operator_pop (op));
    g_assert_null (gem_operator_pop (op));

    evt = gem_test_rand_event (rand);
    evt->time = 31;
    g_assert (gem_operator_push (op, evt) == 1);
    gem_event_unref (evt);

    out = gem_operator_pop (op);
    g_assert_nonnull (out);
    g_assert (gem_event_has_match (out));
    g_assert (evt == gem_event_array_index (out->match, 0));
    gem_event_unref (out);

    g_assert_null (gem_operator_pop (op));
    g_assert_null (gem_operator_pop (op));

    gem_operator_unref (op);
    gem_test_rand_unref (rand);
  }

  exit (EXIT_SUCCESS);
}
