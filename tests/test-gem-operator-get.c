/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  gem_PatternConfig cfg = {NULL, NULL, NULL, 10};
  gem_Operator *op;

  op = gem_operator_new (&cfg, 1, 2, 3);
  cfg = gem_operator_get_config (op);
  g_assert (cfg.data_size == 10);
  g_assert (gem_operator_get_start_id (op) == 1);
  g_assert (gem_operator_get_window_size (op) == 2);
  g_assert (gem_operator_get_window_slide (op) == 3);

  exit (EXIT_SUCCESS);
}
