/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#ifndef GEM_PRIVATE_H
#define GEM_PRIVATE_H

GEM_BEGIN_DECLS

/**
 * SECTION: gem-private
 * @Title: Private Stuff
 * @Short_Description: internal types and functions
 */

/**
 * GEM_TIME_FORMAT:
 *
 * A string that can be used in printf-like format strings to display a
 * #gem_EventTime value in h:m:s format.  Use GEM_TIME_ARGS() to construct
 * the matching arguments.
 *
 * Example:
 * |[<!-- language="C" -->
 * printf("%" GEM_TIME_FORMAT "\n", GEM_TIME_ARGS(t));
 * ]|
 */
#define GEM_TIME_FORMAT "u:%02u:%02u.%06u"

/**
 * GEM_TIME_ARGS:
 * @t: a #gem_EventTime
 *
 * Format @t for the #GEM_TIME_FORMAT format string. Note: @t will be
 * evaluated more than once.
 */
#define GEM_TIME_ARGS(t)                                                \
  GEM_EVENT_TIME_IS_VALID (t) ?                                         \
    (guint) (((gem_EventTime)(t))                                       \
             / ((gint64) G_USEC_PER_SEC * 60 * 60)) : 99,               \
    GEM_EVENT_TIME_IS_VALID (t) ?                                       \
    (guint) ((((gem_EventTime)(t))                                      \
              / ((gint64) G_USEC_PER_SEC * 60)) % 60) : 99,             \
    GEM_EVENT_TIME_IS_VALID (t) ?                                       \
    (guint) ((((gem_EventTime)(t))                                      \
              / (gint64) G_USEC_PER_SEC) % 60) : 99,                    \
    GEM_EVENT_TIME_IS_VALID (t) ?                                       \
    (guint) (((gem_EventTime)(t)) % (gint64) G_USEC_PER_SEC) : 999999999

GEM_API void
gem_debug_event (const gem_Event *evt);

GEM_API void
gem_debug_event_array (const gem_EventArray *array);

/**
 * gem_Window:
 *
 * A segment of the input put events pushed to a #gem_Operator.
 */
typedef struct _gem_Window gem_Window;

GEM_API gem_Window *
gem_event_get_window (const gem_Event *evt);

GEM_API void
gem_event_set_window (gem_Event *evt, gem_Window *win);

GEM_API const gem_Event *
gem_window_get_first (const gem_Window *win);

GEM_API const gem_Event *
gem_window_get_last (const gem_Window *win);

GEM_API const GQueue *
gem_window_get_output (const gem_Window *win);

GEM_API void
gem_debug_window (const gem_Window *win);

GEM_API const GQueue *
gem_operator_get_open_windows (const gem_Operator *op);

GEM_API const GQueue *
gem_operator_get_closed_windows (const gem_Operator *op);

GEM_API const GQueue *
gem_operator_get_output (const gem_Operator *op);

GEM_API void
gem_debug_operator (const gem_Operator *op);

GEM_API void
gem_debug_savepoint (const gem_Savepoint *save);

GEM_END_DECLS

#endif /* GEM_PRIVATE_H */
