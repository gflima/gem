/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include <config.h>
#include "gem.h"
#include "gem-private.h"

/**
 * SECTION: gem-event
 * @Title: Events
 * @Short_Description: primitive and complex events
 */

/* Event flags.  */
typedef enum
{
  GEM_EVENT_FLAG_HASH = 1 << 1,
} gem_EventFlags;

/* The real event.  */
typedef struct _gem_RealEvent
{
  /* public */
  gem_EventID id;
  gem_EventType type;
  gem_EventTime time;
  gem_EventArray *match;

  /* private */
  gem_EventFlags flags;         /* flags */
  guint hash;                   /* whole event hash */
  gem_Window *win;              /* window in operator */
  volatile guint ref_count;     /* reference counter */
} gem_RealEvent;

/**
 * GEM_EVENT_ID_NONE: (type gem_EventID)
 *
 * The undefined #gem_EventID.
 */

/**
 * GEM_EVENT_TYPE_NONE: (type gem_EventType)
 *
 * The undefined #gem_EventType.
 */

/**
 * GEM_EVENT_TIME_NONE: (type gem_EventTime)
 *
 * The undefined #gem_EventTime.
 */

/**
 * GEM_EVENT_ID_IS_VALID:
 * @id: a #gem_EventID
 *
 * Returns: %TRUE if @id is a valid #gem_EventID, or %FALSE otherwise.
 */

/**
 * GEM_EVENT_TYPE_IS_VALID:
 * @type: a #gem_EventType
 *
 * Returns: %TRUE if @type is a valid #gem_EventType, or %FALSE otherwise.
 */

/**
 * GEM_EVENT_TIME_IS_VALID:
 * @time: a #gem_EventTime
 *
 * Returns: %TRUE if @time is a valid #gem_EventTime, or %FALSE otherwise.
 */

/**
 * gem_event_has_id:
 * @evt: a #gem_Event
 *
 * Returns: %TRUE if @evt has an id, or %FALSE otherwise.
 */

/**
 * gem_event_has_type:
 * @evt: a #gem_Event
 *
 * Returns: %TRUE if @evt has a type, or %FALSE otherwise.
 */

/**
 * gem_event_has_time:
 * @evt: a #gem_Event
 *
 * Returns: %TRUE if @evt has a time, or %FALSE otherwise.
 */

/**
 * gem_event_has_match:
 * @evt: a #gem_Event
 *
 * Returns: %TRUE if @evt has an associated match, or %FALSE otherwise.
 */

/**
 * gem_event_get_id:
 * @evt: a #gem_Event
 *
 * Returns: @evt's id.
 */

/**
 * gem_event_get_type:
 * @evt: a #gem_Event
 *
 * Returns: @evt's type.
 */

/**
 * gem_event_get_time:
 * @evt: a #gem_Event
 *
 * Returns: @evt's time.
 */

/**
 * gem_event_get_match:
 * @evt: a #gem_Event
 *
 * Returns: @evt's match or %NULL (no match).
 */

/**
 * gem_event_new:
 * @id: id for the event
 * @type: type for the event
 * @time: time for the event
 * @match: (optional): match content for the event
 *
 * Creates a new #gem_Event.
 *
 * This function references @match, so you can immediately call
 * gem_event_array_unref() on it if you don't need to maintain a separate
 * reference to it.
 *
 * Returns: (transfer full): A newly allocated #gem_Event with a reference
 *   count of 1.  Free with gem_event_unref().
 */
gem_Event *
gem_event_new (gem_EventID id,
               gem_EventType type,
               gem_EventTime time,
               gem_EventArray *match)
{
  gem_RealEvent *revt;

  revt = g_slice_new (gem_RealEvent);
  revt->id = id;
  revt->type = type;
  revt->time = time;
  revt->match = match ? gem_event_array_ref (match) : NULL;
  revt->flags = 0;
  revt->hash = 0;
  revt->win = NULL;
  revt->ref_count = 1;

  return (gem_Event *) revt;
}

/**
 * gem_event_ref:
 * @evt: a #gem_Event
 *
 * Increases the reference count on @evt by one.  This prevents @evt from
 * being freed until a matching call to gem_event_unref() is made.
 *
 * Returns: The referenced #gem_Event.
 */
gem_Event *
gem_event_ref (gem_Event *evt)
{
  gem_RealEvent *revt;

  g_return_val_if_fail (evt, NULL);

  revt = (gem_RealEvent *) evt;
  g_atomic_int_inc (&revt->ref_count);

  return (gem_Event *) revt;
}

/**
 * gem_event_unref:
 * @evt: a #gem_Event
 *
 * Decreases the reference count on @evt by one.  If the result is zero,
 * then frees @evt and all associated resources.  See gem_event_ref().
 */
void
gem_event_unref (gem_Event *evt)
{
  gem_RealEvent *revt;

  g_return_if_fail (evt);

  revt = (gem_RealEvent *) evt;
  if (g_atomic_int_dec_and_test (&revt->ref_count))
    {
      if (revt->match != NULL)
        g_ptr_array_unref (revt->match);
      g_slice_free (gem_RealEvent, revt);
    }
}

/**
 * gem_event_hash:
 * @evt: a #gem_Event
 *
 * Converts a #gem_Event to a hash value.
 *
 * Returns: A hash value corresponding to @evt.
 */

static guint
djb_hash (guint accum,
          gconstpointer ptr,
          guint size)
{
  const unsigned char *p = ptr;
  guint32 h = (accum == 0) ? 5381 : accum;

  while (size-- > 0)
    h = (h << 5) + h + *p++;

  return h;
}

guint
gem_event_hash (const gem_Event *evt)
{
  gem_RealEvent *revt;
  guint h;

  g_return_val_if_fail (evt, 0);

  revt = (gem_RealEvent *) evt;
  if (revt->flags & GEM_EVENT_FLAG_HASH)
    return revt->hash;

  h = djb_hash (0, &revt->id, sizeof (evt->id));
  h = djb_hash (h, &revt->type, sizeof (evt->type));
  h = djb_hash (h, &revt->time, sizeof (evt->time));

  if (evt->match != NULL)
    {
      guint m = gem_event_array_hash (evt->match);
      h = djb_hash (h, &m, sizeof (m));
    }

  revt->hash = h;
  revt->flags |= GEM_EVENT_FLAG_HASH;

  return revt->hash;
}

/**
 * gem_event_shallow_equal:
 * @evt1: a #gem_Event
 * @evt2: a #gem_Event
 *
 * Tests whether @evt1 and @evt2 have the same id, type and time.
 *
 * Returns: %TRUE if successful or %FALSE otherwise.
 */
gboolean
gem_event_shallow_equal (const gem_Event *evt1,
                         const gem_Event *evt2)
{
  g_return_val_if_fail (evt1, FALSE);
  g_return_val_if_fail (evt2, FALSE);

  return evt1->id == evt2->id
    && evt1->type == evt2->type
    && evt1->time == evt2->time;
}

/**
 * gem_event_equal:
 * @evt1: a #gem_Event
 * @evt2: a #gem_Event
 *
 * Tests whether @evt1 and @evt2 have the same id, type and time, and if
 * their matches are identical.
 *
 * Returns: %TRUE if successful or %FALSE otherwise.
 */
gboolean
gem_event_equal (const gem_Event *evt1,
                 const gem_Event *evt2)
{
  guint i;

  g_return_val_if_fail (evt1, FALSE);
  g_return_val_if_fail (evt2, FALSE);

  if (!gem_event_shallow_equal (evt1, evt2))
    return FALSE;

  if (!gem_event_has_match (evt1) && !gem_event_has_match (evt2))
    return TRUE;

  if (!(gem_event_has_match (evt1) && gem_event_has_match (evt2)))
    return FALSE;

  if (evt1->match->len != evt2->match->len)
    return FALSE;

  for (i = 0; i < evt1->match->len; i++)
    {
      gem_Event *m1;
      gem_Event *m2;

      m1 = gem_event_array_index (evt1->match, i);
      m2 = gem_event_array_index (evt2->match, i);
      if (!gem_event_equal (m1, m2))
        return FALSE;
    }

  return TRUE;
}

/**
 * gem_event_equal_hash:
 * @evt1: a #gem_Event
 * @evt2: a #gem_Event
 *
 * Tests whether the hashes of @evt1 and @evt2 are equal.
 *
 * Returns: %TRUE if successful or %FALSE otherwise.
 */
gboolean
gem_event_equal_hash (const gem_Event *evt1,
                      const gem_Event *evt2)
{
  g_return_val_if_fail (evt1, FALSE);
  g_return_val_if_fail (evt2, FALSE);

  return gem_event_hash (evt1) == gem_event_hash (evt2);
}

/**
 * gem_event_to_string:
 * @evt: a #gem_Event
 *
 * Formats @evt as a human-readable string.
 *
 * Returns: (transfer full): A newly allocated #GString.  Free with
 *   g_string_free().
 */
GString *
gem_event_to_string (const gem_Event *evt)
{
  g_return_val_if_fail (evt, NULL);

  return gem_event_append_to_string (g_string_new (NULL), evt);
}

/**
 * gem_event_append_to_string:
 * @str: the #GString to append the formatted event to
 * @evt: a #gem_Event
 *
 * Formats @evt as a human-readable string and appends it to @str.
 *
 * Returns: (transfer none): @str.
 */
GString *
gem_event_append_to_string (GString *str,
                            const gem_Event *evt)
{
  g_return_val_if_fail (str, NULL);
  g_return_val_if_fail (evt, NULL);

  if (gem_event_has_id (evt))
    g_string_append_printf (str, "%"G_GUINT64_FORMAT":", evt->id);

  if (gem_event_has_type (evt))
    g_string_append_printf (str, "%u", evt->type);

  if (gem_event_has_time (evt))
    g_string_append_printf (str, "[%"GEM_TIME_FORMAT"]",
                            GEM_TIME_ARGS (evt->time));

  if (gem_event_has_match (evt))
    gem_event_array_append_to_string (str, evt->match);

  return str;
}

/**
 * gem_event_array_new:
 *
 * Creates a new #gem_EventArray.
 *
 * Returns: (transfer full): A newly allocated #gem_EventArray with a
 *   reference count of 1.  Free with gem_event_array_unref().
 */
gem_EventArray *
gem_event_array_new (void)
{
  return gem_event_array_sized_new (0);
}

/**
 * gem_event_array_sized_new:
 * @reserved_size: number of pointers preallocated
 *
 * Creates a new #gem_EventArray.
 *
 * Returns: A newly allocated #gem_EventArray with @reserved_size pointers
 * preallocated and a reference count of 1.  Free with
 * gem_event_array_unref().
 */
gem_EventArray *
gem_event_array_sized_new (guint reserved_size)
{
  return g_ptr_array_new_full (reserved_size,
                               (GDestroyNotify) gem_event_unref);
}

/**
 * gem_event_array_ref:
 * @array: a #gem_EventArray
 *
 * Increases the reference count on @array by one.  This prevents @array
 * from being freed until a matching call to gem_event_array_unref() is
 * made.
 *
 * Returns: The referenced #gem_EventArray.
 */
gem_EventArray *
gem_event_array_ref (gem_EventArray *array)
{
  g_return_val_if_fail (array, NULL);

  return g_ptr_array_ref (array);
}

/**
 * gem_event_array_unref:
 * @array: a #gem_EventArray
 *
 * Decreases the reference count on @array by one.  If the result is zero,
 * then frees @array and all associated resources.  See
 * gem_event_array_ref().
 */
void
gem_event_array_unref (gem_EventArray *array)
{
  g_return_if_fail (array);

  g_ptr_array_unref (array);
}

/**
 * gem_event_array_hash:
 * @array: a #gem_EventArray
 *
 * Converts a #gem_EventArray to a hash value.
 *
 * Returns: A hash value corresponding to @array.
 */
guint
gem_event_array_hash (gem_EventArray *array)
{
  guint h, i, evt_hash;

  g_return_val_if_fail (array, 0);

  h = djb_hash (0, NULL, 0);
  for (i = 0; i < array->len; i++)
    {
      evt_hash = gem_event_hash (gem_event_array_index (array, i));
      h = djb_hash (h, &evt_hash, sizeof (evt_hash));
    }

  return h;
}

/**
 * gem_event_array_add:
 * @array: a #gem_EventArray
 * @evt: the #gem_Event to add
 *
 * Adds @evt to the end of @array.  The array will grow in size
 * automatically if necessary.
 *
 * This function references @evt, so you can immediately call
 * gem_event_unref() on it if you don't need to maintain a separate
 * reference to it.
 */
void
gem_event_array_add (gem_EventArray *array,
                     gem_Event *evt)
{
  g_return_if_fail (array);
  g_return_if_fail (evt);

  g_ptr_array_add (array, gem_event_ref (evt));
}

/**
 * gem_event_array_to_string:
 * @array: a #gem_EventArray
 *
 * Formats @array as a human-readable string.
 *
 * Returns: (transfer full): A newly allocated #GString.  Free with
 *   g_string_free().
 */
GString *
gem_event_array_to_string (const gem_EventArray *array)
{
  g_return_val_if_fail (array, NULL);

  return gem_event_array_append_to_string (g_string_new (NULL), array);
}

/**
 * gem_event_array_append_to_string:
 * @str: the #GString to append the formatter array to
 * @array: a #gem_EventArray
 *
 * Formats @array as a human-readable string and appends it to @str.
 *
 * Returns: (transfer none): @str.
 */
GString *
gem_event_array_append_to_string (GString *str,
                                  const gem_EventArray *array)
{
  guint i;

  g_return_val_if_fail (str, NULL);
  g_return_val_if_fail (array, NULL);

  if (array->len == 0)
    return g_string_append (str, "{}");

  g_string_append_c (str, '{');
  gem_event_append_to_string (str, gem_event_array_index (array, 0));
  for (i = 1; i < array->len; i++)
    {
      g_string_append_c (str, ',');
      gem_event_append_to_string (str, gem_event_array_index (array, i));
    }
  g_string_append_c (str, '}');

  return str;
}


/**** Private ****/

/**
 * gem_event_get_window:
 * @evt: a #gem_Event
 *
 * Returns: @evt's window or %NULL (no window).
 */
gem_Window *
gem_event_get_window (const gem_Event *evt)
{
  gem_RealEvent *revt;

  g_return_val_if_fail (evt, NULL);

  revt = (gem_RealEvent *) evt;
  return revt->win;
}

/**
 * gem_event_set_window:
 * @evt: a #gem_Event
 * @win: a #gem_Window
 *
 * Sets @evt window to @win.
 */
void
gem_event_set_window (gem_Event *evt, gem_Window *win)
{
  gem_RealEvent *revt;

  g_return_if_fail (evt);

  revt = (gem_RealEvent *) evt;
  revt-> win = win;
}

/**
 * gem_debug_event:
 * @evt: a #gem_Event
 *
 * Logs @evt using g_debug().
 */
void
gem_debug_event (const gem_Event *evt)
{
  GString *str;

  g_return_if_fail (evt);

  str = gem_event_to_string (evt);
  g_debug ("Event (%p): %s", evt, str->str);
  g_string_free (str, TRUE);
}

/**
 * gem_debug_event_array:
 * @array: a #gem_EventArray
 *
 * Logs @array using g_debug().
 */
void
gem_debug_event_array (const gem_EventArray *array)
{
  guint i;

  g_return_if_fail (array);

  g_debug ("EventArray (%p):", array);
  for (i = 0; i < array->len; i++)
    {
      gem_Event *evt;
      GString *str;

      evt = gem_event_array_index (array, i);
      str = gem_event_to_string (evt);
      g_debug ("#%u: Event (%p): %s", i, evt, str->str);
      g_string_free (str, TRUE);
    }
}
