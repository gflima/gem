/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#ifndef GEM_BENCHMARK
#define GEM_BENCHMARK

/**
 * SECTION: gem-benchmark
 * @Title: Benchmarks
 * @Short_Description: measuring libgem performance
 */

/**
 * gem_BenchmarkOptions:
 * @sample: test sample
 * @ack_countdown: number of acks to wait before saving
 * @ack_min: min difference between acks
 * @ack_max: max difference between acks
 * @time_min: min difference between input event times
 * @time_max: max difference between input event times
 * @type_min: min difference between input event types
 * @type_max: max difference between input event types
 * @test_pat: #gem_test_Pattern to search for
 * @output_file: name of file to write output to
 * @failure_points: queue of failure points (#gem_EventID)
 * @duplicates: whether to detect duplicate output events
 * @op_save: whether to enable saves
 * @op_output_ids: whether to enable output ids
 * @op_skip: whether to enable skip ranges
 * @op_stats: whether to enable statistics
 * @op_trace: whether to enable traces
 * @show_progress: whether to show progress information
 * @show_results: whether to show results
 * @custom_results: array with names of #gem_BenchmarkResults fields to show
 * @runs: number of repeated runs
 *
 * Benchmark options.
 */
typedef struct _gem_BenchmarkOptions gem_BenchmarkOptions;
struct _gem_BenchmarkOptions
{
  gem_test_RandSample sample;
  guint ack_countdown;
  gem_EventTime ack_min;
  gem_EventTime ack_max;
  gem_EventTime time_min;
  gem_EventTime time_max;
  gem_EventType type_min;
  gem_EventType type_max;
  gem_test_Pattern test_pat;
  gchar *output_file;
  GQueue *failure_points;
  gboolean duplicates;
  gboolean op_save;
  gboolean op_output_ids;
  gboolean op_skip;
  gboolean op_stats;
  gboolean op_trace;
  gboolean show_progress;
  gboolean show_results;
  gchar **custom_results;
  guint runs;
};

gboolean
gem_benchmark_options_parse (gint argc,
                             gchar **argv,
                             gem_BenchmarkOptions *opts,
                             GError **error);
void
gem_benchmark_options_clear (gem_BenchmarkOptions *opts);

/**
 * gem_benchmark_get_rand_from_options:
 * @opts: a #gem_BenchmarkOptions
 *
 * Allocates a new #gem_test_Rand initialized with the data in @opts.
 */
#define gem_benchmark_get_rand_from_options(opts)       \
  gem_test_rand_new_full_with_seed                      \
  ((opts)->sample.seed != 0 ? (opts)->sample.seed       \
   : (guint)(g_get_monotonic_time () % G_MAXUINT), 0,   \
   (opts)->type_min,                                    \
   (opts)->type_max,                                    \
   (opts)->time_min,                                    \
   (opts)->time_max)

/**
 * gem_Benchmark:
 *
 * Represents a benchmark run.
 */
typedef struct _gem_Benchmark gem_Benchmark;

/**
 * gem_BenchmarkResults:
 * @opts: the #gem_BenchmarkOptions associated with results
 * @stats: operator statistics (if enabled)
 * @evts_in: number of input events consumed
 * @evts_out: number of output events produced
 * @duplicates: number of duplicate output events
 * @inq_max_length: maximum length of input queue
 * @outq_max_length: maximum length of output queue
 * @accum_time: accumulated time of input events
 * @runtime: total runtime
 * @accum_runtime: accumulated runtime
 * @restores: number of restores
 * @replayed: number of input events replayed
 * @replayed_dups: number of duplicate output events produced during replay
 * @restore_runtime: total runtime of restores
 * @acks: number of acks
 * @saves: number of saves
 * @saves_failed: number of unsuccessful saves
 * @saves_out_ids: number of output ids in saves
 * @saves_skip_pairs: number of skip pairs in saves
 * @saves_skip_pairs_range: total range of skipped pairs
 * @saves_runtime: total runtime of saves
 *
 * Stores the results of a benchmark run.
 */
typedef struct _gem_BenchmarkResults gem_BenchmarkResults;
struct _gem_BenchmarkResults
{
  gem_BenchmarkOptions opts;
  gem_OperatorStats stats;
  guint evts_in;
  guint evts_out;
  guint duplicates;
  guint inq_max_length;
  guint outq_max_length;
  gem_EventTime accum_time;
  gem_EventTime runtime;
  gem_EventTime accum_runtime;
  guint restores;
  guint replayed;
  guint replayed_dups;
  gem_EventTime restore_runtime;
  guint acks;
  guint saves;
  guint saves_failed;
  guint saves_out_ids;
  guint saves_skip_pairs;
  guint saves_skip_pairs_range;
  gem_EventTime saves_runtime;
};

gem_Benchmark *
gem_benchmark_new (const gem_BenchmarkOptions *opts);

gem_Benchmark *
gem_benchmark_ref (gem_Benchmark *run);

void
gem_benchmark_unref (gem_Benchmark *run);

void
gem_benchmark_run (gem_Benchmark *run);

const gem_BenchmarkResults *
gem_benchmark_get_results (gem_Benchmark *run);

#endif /* GEM_BENCHMARK */
