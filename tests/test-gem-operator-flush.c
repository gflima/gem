/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  GEM_TEST ("No-op");
  {
    gem_Operator *op;
    const GQueue *open;

    op = gem_operator_new (NULL, 0, 1, 1);
    open = gem_operator_get_open_windows (op);
    g_assert (open->length == 0);

    g_assert (gem_operator_flush (op) == 0);
    g_assert (open->length == 0);

    gem_operator_unref (op);
  }

  GEM_TEST ("Close some open windows and complete their patterns");
  {
    gem_test_Rand *rand;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    const GQueue *open;
    gem_Event *evt;

    rand = gem_test_rand_new_full (0, 0, 0, 0, 0);
    cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY_EXPAND);
    op = gem_operator_new (&cfg, 0, 1, 1);
    open = gem_operator_get_open_windows (op);
    g_assert (open->length == 0);

    evt = gem_test_rand_event (rand);
    g_assert (gem_operator_push (op, evt) == 0);
    gem_event_unref (evt);

    evt = gem_test_rand_event (rand);
    evt->time = 1;
    g_assert (gem_operator_push (op, evt) == 0);
    gem_event_unref (evt);

    g_assert (gem_operator_flush (op) == 2);
    g_assert (open->length == 0);

    evt = gem_test_rand_event (rand);
    evt->time = 5;
    g_assert (gem_operator_push (op, evt) == 0);
    gem_event_unref (evt);

    g_assert (gem_operator_flush (op) == 1);
    g_assert (open->length == 0);

    g_assert (gem_operator_flush (op) == 0);
    g_assert (open->length == 0);

    gem_operator_unref (op);
    gem_test_rand_unref (rand);
  }

  GEM_TEST ("Completely random run");
  {
    gem_test_Rand *rand;
    gem_test_RandSample sample;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    const GQueue *open;
    guint i;

    rand = gem_test_rand_new ();
    sample = gem_test_rand_sample (rand, 0);

    cfg = gem_test_pattern (GEM_TEST_PATTERN_WACKY);
    op = gem_operator_new (&cfg, 0, sample.win_size, sample.win_slide);
    open = gem_operator_get_open_windows (op);
    g_assert (open->length == 0);

    for (i = 0; i < sample.input_size; i++)
      {
        gem_Event *evt = gem_test_rand_event (rand);
        gem_operator_push (op, evt);
        gem_event_unref (evt);

        if (g_random_boolean ())
          {
            gem_operator_flush (op);
            g_assert (open->length == 0);
          }
      }

    gem_operator_unref (op);
    gem_test_rand_unref (rand);
  }

  exit (EXIT_SUCCESS);
}
