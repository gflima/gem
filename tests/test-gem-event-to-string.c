/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  gem_Event *e1;
  gem_Event *e2;
  gem_Event *e3;

  GString *str;
  gem_EventArray *match;

  e1 = gem_event_new (2, 1, GEM_EVENT_TIME_NONE, NULL);
  str = gem_event_to_string (e1);
  g_print ("str(e1) == %s\n", str->str);
  g_assert (g_str_equal (str->str, "2:1"));
  g_string_free (str, TRUE);

  match = gem_event_array_new ();
  str = gem_event_array_to_string (match);
  g_assert (g_str_equal (str->str, "{}"));
  g_string_free (str, TRUE);

  gem_event_array_add (match, e1);
  gem_event_unref (e1);

  str = gem_event_array_to_string (match);
  g_print ("str(match) == %s\n", str->str);
  g_assert (g_str_equal (str->str, "{2:1}"));
  g_string_free (str, TRUE);

  e2 = gem_event_new (0, GEM_EVENT_TYPE_NONE, 2, NULL);
  str = gem_event_to_string (e2);
  g_print ("str(e2) == %s\n", str->str);
  g_assert (g_str_equal (str->str, "0:[0:00:00.000002]"));
  g_string_free (str, TRUE);

  gem_event_array_add (match, e2);
  gem_event_unref (e2);

  e3 = gem_event_new (GEM_EVENT_ID_NONE, 1, 2, match);
  gem_event_array_unref (match);

  str = gem_event_to_string (e3);
  g_print ("str(e3) == %s", str->str);
  g_assert (g_str_equal (str->str,
                         "1[0:00:00.000002]{2:1,0:[0:00:00.000002]}"));
  g_string_free (str, TRUE);
  gem_event_unref (e3);

  exit (EXIT_SUCCESS);
}
