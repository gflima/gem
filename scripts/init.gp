# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

set loadpath './gnuplot'
load 'xyborder.cfg'
load 'grid.cfg'

# Generate PDF.
set terminal pdf enhanced font 'Verdana,11'

# Use first line as titles (use "unset key" to disable).
set key autotitle columnhead

# Colors.
set linetype 1 lc rgb '#1B9E77' # dark teal
set linetype 2 lc rgb '#D95F02' # dark orange
set linetype 3 lc rgb '#7570B3' # dark lilac
set linetype 4 lc rgb '#E7298A' # dark magenta
set linetype 5 lc rgb '#66A61E' # dark lime green
set linetype 6 lc rgb '#E6AB02' # dark banana
set linetype 7 lc rgb '#A6761D' # dark tan
set linetype 8 lc rgb '#666666' # dark gray

# Simple histogram.
INIT_HIST = "\
unset xtics;\
set tics nomirror out scale .75;\
set style data histograms;\
set style histogram gap 1 title offset 0,.75;\
set style fill solid .7;\
"

# Error bars.
INIT_HIST_ERRORBARS = "\
unset xtics;\
set tics nomirror out scale .75;\
set style data histograms;\
set style histogram errorbars gap 1 title offset 0,.75;\
set style fill solid .7;\
"

# Row-stack.
INIT_HIST_ROWSTACK = "\
set style data histograms;\
set style histogram rowstack gap 1 title offset 0,1;\
set style fill solid .7;\
"
