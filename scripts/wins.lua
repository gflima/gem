#!/usr/bin/env lua
local w=tonumber (arg[1])
local s=tonumber (arg[2])
local d=math.floor (w/s)
local max = d + 1
local overlap = w * (2 * d + 1) - s * (d * d + d);

print ('w='..w)
print ('s='..s)
print ('d='..d)
print ('max='..max)
print ('overlap='..overlap)
