/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  /* Simple event.  */
  {
    gem_Event *evt = gem_event_new (0, 1, 2, NULL);
    g_assert (evt->id == 0);
    g_assert (evt->type == 1);
    g_assert (evt->time == 2);
    g_assert (evt->match == NULL);
    gem_event_unref (evt);
  }

  /* Random event.  */
  {
    gem_test_Rand *rand;
    gem_Event *evt;

    rand = gem_test_rand_new ();
    evt = gem_test_rand_event (rand);
    g_assert (gem_event_has_id (evt));
    g_assert (gem_event_has_type (evt));
    g_assert (gem_event_has_time (evt));
    g_assert_false (gem_event_has_match (evt));
    gem_event_unref (evt);
    gem_test_rand_free (rand);
  }

  exit (EXIT_SUCCESS);
}
