#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load "init.gp"
@INIT_HIST_ERRORBARS

NAME = 'volume'
set output NAME.'.pdf'

set size ratio 0.4
set xtics format ' '
set xlabel 'Types'
set ylabel 'Output / Input'
set format y '%g%%'
set yrange [0:*]
set key title 'Pattern'

PATS = '0 01 012 0123 01234'

TITLE = 'Match volume: Skip-till-next-match'
SUBTITLE = sprintf ('{/*.8 %s}', '(1k runs, 10k input events per run)')

prefix = NAME.'.out'
USING = 'using ($4/$1*100):($5/$1*100):($6/$1*100) with hist'


#set title TITLE."\n".SUBTITLE
plot\
  newhistogram '10' lt 1 at 0,\
  prefix.'/0-9.'.'0'.'.avg' @USING title 'a',\
  prefix.'/0-9.'.'01'.'.avg' @USING title 'ab',\
  prefix.'/0-9.'.'012'.'.avg' @USING title 'abc',\
  prefix.'/0-9.'.'0123'.'.avg' @USING title 'abcd',\
  prefix.'/0-9.'.'01234'.'.avg' @USING title 'abcde',\
  newhistogram '20' lt 1 at 1,\
  for [pat in PATS]\
    prefix.'/0-19.'.pat.'.avg' @USING notitle,\
  newhistogram '50' lt 1 at 2,\
  for [pat in PATS]\
    prefix.'/0-49.'.pat.'.avg' @USING notitle,\
  newhistogram '100' lt 1 at 3,\
  for [pat in PATS]\
    prefix.'/0-99.'.pat.'.avg' @USING notitle

  # for [pat in PATS]\
  #   prefix.'/0-9.'.pat.'.avg' @USING title pat,\
