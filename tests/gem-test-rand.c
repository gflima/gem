/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

/**
 * SECTION: gem-test-rand
 * @Title: Random Data
 * @Short_Description: generating random events
 */

struct _gem_test_Rand
{
  GRand *rand;                  /* PRNG */
  guint seed;                   /* PRNG seed */
  gem_EventID id;               /* initial id */
  gem_EventType type_min;       /* min type difference */
  gem_EventType type_max;       /* max type difference */
  gem_EventTime time_min;       /* min time difference */
  gem_EventTime time_max;       /* max time difference */
  volatile guint ref_count;     /* reference counter */
};

/* Gets an arbitrary guint.  */
#define MAGIC (guint)(g_get_monotonic_time () % G_MAXUINT)

/**
 * gem_test_rand_new:
 *
 * Creates a new #gem_test_Rand with an arbitrary seed and an arbitrary
 * configuration (initial id, type range, and time range).
 *
 * Returns: (transfer full): A newly allocated #gem_test_Rand.
 * Free with gem_test_rand_unref().
 */
gem_test_Rand *
gem_test_rand_new (void)
{
  return gem_test_rand_new_with_seed (MAGIC);
}

/**
 * gem_test_rand_new_with_seed:
 * @seed: the PRNG seed
 *
 * Creates a new #gem_test_Rand with the given seed but with an arbitrary
 * configuration (initial id, type range, and time range).
 *
 * Returns: (transfer full): A newly allocated #gem_test_Rand.
 * Free with gem_test_rand_unref().
 */
gem_test_Rand *
gem_test_rand_new_with_seed (guint seed)
{
  return gem_test_rand_new_full_with_seed (seed,         /* seed */
                                           0,            /* id */
                                           0,            /* min_type */
                                           G_MAXUINT,    /* max_type */
                                           0,            /* min_time */
                                           G_MAXUINT64); /* max_time */
}

/**
 * gem_test_rand_new_full:
 * @start_id: start id for the generated events
 * @type_min: minimal difference between event types
 * @type_max: maximal difference between event types
 * @time_min: minimal difference between event times
 * @time_max: maximal difference between event times
 *
 * Creates a new #gem_test_Rand with the given configuration (start id,
 * type range, and time range) but with an arbitrary seed.
 *
 * Returns: (transfer full): A newly allocated #gem_test_Rand.
 * Free with gem_test_rand_unref().
 */
gem_test_Rand *
gem_test_rand_new_full (gem_EventID start_id,
                        gem_EventType type_min,
                        gem_EventType type_max,
                        gem_EventTime time_min,
                        gem_EventTime time_max)
{
  return gem_test_rand_new_full_with_seed (MAGIC,
                                           start_id,
                                           type_min,
                                           type_max,
                                           time_min,
                                           time_max);
}

/**
 * gem_test_rand_new_full_with_seed:
 * @seed: the PRNG seed
 * @start_id: start id for the generated events
 * @type_min: minimal difference between event types
 * @type_max: maximal difference between event types
 * @time_min: minimal difference between event times
 * @time_max: maximal difference between event times
 *
 * Creates a new #gem_test_Rand with the given seed and configuration
 * (start id, type range, and time range).
 *
 * Returns: (transfer full): A newly allocated #gem_test_Rand.
 * Free with gem_test_rand_unref().
 */
gem_test_Rand *
gem_test_rand_new_full_with_seed (guint seed,
                                  gem_EventID start_id,
                                  gem_EventType type_min,
                                  gem_EventType type_max,
                                  gem_EventTime time_min,
                                  gem_EventTime time_max)
{
  gem_test_Rand *rand;

  rand = g_slice_new (gem_test_Rand);
  rand->rand = g_rand_new_with_seed (seed);
  rand->seed = seed;
  rand->id = start_id;
  rand->type_min = type_min;
  rand->type_max = type_max;
  rand->time_min = time_min;
  rand->time_max = time_max;
  rand->ref_count = 1;

  return rand;
}

/**
 * gem_test_rand_ref:
 * @rand: a #gem_test_Rand
 *
 * Increases the reference count on @rand by one.  This prevents @rand from
 * being freed until a matching call to gem_test_rand_unref() is made.
 *
 * Returns: The referenced #gem_test_Rand.
 */
gem_test_Rand *
gem_test_rand_ref (gem_test_Rand *rand)
{
  g_return_val_if_fail (rand, NULL);

  g_atomic_int_inc (&rand->ref_count);
  return rand;
}

/**
 * gem_test_rand_unref:
 * @rand: a #gem_test_Rand
 *
 * Decreases the reference count on @rand by one.  If the result is zero,
 * then frees @rand and all associated resources.  See gem_test_rand_ref().
 */
void
gem_test_rand_unref (gem_test_Rand *rand)
{
  g_return_if_fail (rand);

  if (g_atomic_int_dec_and_test (&rand->ref_count))
    {
      g_rand_free (rand->rand);
      g_slice_free (gem_test_Rand, rand);
    }
}

/**
 * gem_test_rand_get_seed:
 * @rand: a #gem_test_Rand
 *
 * Returns: @rand's seed.
 */
guint
gem_test_rand_get_seed (const gem_test_Rand *rand)
{
  g_return_val_if_fail (rand, 0);

  return rand->seed;
}

/**
 * gem_test_rand_get_rand:
 * @rand: a #gem_test_Rand
 *
 * Returns: @rand's underlying #GRand.
 */
GRand *
gem_test_rand_get_rand (const gem_test_Rand *rand)
{
  g_return_val_if_fail (rand, NULL);

  return rand->rand;
}

/**
 * gem_test_rand_event_type:
 * @rand: a #gem_test_Rand
 *
 * Returns: A random #gem_EventType value.
 */
gem_EventType
gem_test_rand_event_type (gem_test_Rand *rand)
{
  g_return_val_if_fail (rand, GEM_EVENT_TYPE_NONE);

  return gem_test_rand_event_type_full (rand,
                                        rand->type_min,
                                        rand->type_max);
}

/**
 * gem_test_rand_event_type_full:
 * @rand: a #gem_test_Rand
 * @type_min: minimal value for the returned #gem_EventType
 * @type_max: maximal value for the returned #gem_EventType
 *
 * Returns: A random #gem_EventType value between [@type_min,@type_max].
 */
gem_EventType
gem_test_rand_event_type_full (gem_test_Rand *rand,
                               gem_EventType type_min,
                               gem_EventType type_max)
{
  gint32 lo, hi;

  g_return_val_if_fail (rand, GEM_EVENT_TYPE_NONE);

  lo = (gint32)(type_min > G_MAXINT32 ? G_MAXINT32 : type_min);
  hi = (gint32)(CLAMP (type_max, type_min, G_MAXINT32 - 1));

  return (gem_EventType) g_rand_int_range (rand->rand, lo, hi + 1);
}

/**
 * gem_test_rand_event_time:
 * @rand: a #gem_test_Rand
 *
 * Returns: A random #gem_EventTime value.
 */
gem_EventTime
gem_test_rand_event_time (gem_test_Rand *rand)
{
  g_return_val_if_fail (rand, GEM_EVENT_TIME_NONE);

  return gem_test_rand_event_time_full (rand,
                                        rand->time_min,
                                        rand->time_max);
}

/**
 * gem_test_rand_event_time_full:
 * @rand: a #gem_test_Rand
 * @time_min: minimal value for the returned #gem_EventTime
 * @time_max: maximal value for the returned #gem_EventTime
 *
 * Returns: A random #gem_EventTime value between [@time_min,@time_max].
 */
gem_EventTime
gem_test_rand_event_time_full (gem_test_Rand *rand,
                               gem_EventTime time_min,
                               gem_EventTime time_max)
{
  gint32 lo, hi;

  g_return_val_if_fail (rand, GEM_EVENT_TIME_NONE);

  lo = (gint32)(time_min > G_MAXINT32 ? G_MAXINT32 : time_min);
  hi = (gint32)(CLAMP (time_max, time_min, G_MAXINT32 - 1));

  return (gem_EventTime) g_rand_int_range (rand->rand, lo, hi + 1);
}

/**
 * gem_test_rand_event:
 * @rand: a #gem_test_Rand
 *
 * Returns: (transfer full): A random primitive #gem_Event with no match.
 */
gem_Event *
gem_test_rand_event (gem_test_Rand *rand)
{
  g_return_val_if_fail (rand, NULL);

  return gem_test_rand_event_full (rand, rand->id++,
                                   rand->type_min, rand->type_max,
                                   rand->time_min, rand->time_max);
}

/**
 * gem_test_rand_event_full:
 * @rand: a #gem_test_Rand
 * @id: an id for the event
 * @type_min: minimal value for event type
 * @type_max: maximal value for event type
 * @time_min: minimal value for event time
 * @time_max: maximal value for event time
 *
 * Returns: A random primitive #gem_Event with id @id, type between
 * [@type_min,@type_max], time between [@time_min,@time_max] and no match.
 */
gem_Event *
gem_test_rand_event_full (gem_test_Rand *rand,
                          gem_EventID id,
                          gem_EventType type_min,
                          gem_EventType type_max,
                          gem_EventTime time_min,
                          gem_EventTime time_max)
{
  gem_EventType type;
  gem_EventTime time;

  g_return_val_if_fail (rand, NULL);

  type = gem_test_rand_event_type_full (rand, type_min, type_max);
  time = gem_test_rand_event_time_full (rand, time_min, time_max);

  return gem_event_new (id, type, time, NULL);
}

/**
 * gem_test_rand_event_array:
 * @rand: a #gem_test_Rand
 * @len: length of the returned array
 *
 * Returns: (transfer full): An #gem_EventArray filled with @len random
 * primitive events.
 */
gem_EventArray *
gem_test_rand_event_array (gem_test_Rand *rand,
                           guint len)
{
  gem_EventArray *array;

  g_return_val_if_fail (rand, NULL);

  array = gem_test_rand_event_array_full (rand, len, rand->id,
                                          rand->type_min, rand->type_max,
                                          rand->time_min, rand->time_max);
  rand->id += len;

  return array;
}

/**
 * gem_test_rand_event_array_full:
 * @rand: a #gem_test_Rand
 * @len: length of the returned array
 * @start_id: the start id for the events in the array
 * @type_min: minimal value for event type of events in the array
 * @type_max: maximal value for event type of events in the array
 * @time_min: minimal value for event time of events in the array
 * @time_max: maximal value for event time of events in the array
 *
 * Returns: (transfer full): An #gem_EventArray filled with @len random
 * primitive events.
 */
gem_EventArray *
gem_test_rand_event_array_full (gem_test_Rand *rand,
                                guint len,
                                gem_EventID start_id,
                                gem_EventType type_min,
                                gem_EventType type_max,
                                gem_EventTime time_min,
                                gem_EventTime time_max)
{
  gem_EventArray *array;
  guint i;

  g_return_val_if_fail (rand, NULL);

  array = gem_event_array_sized_new (len);
  for (i = 0; i < len; i++)
    {
      gem_Event *evt;

      evt = gem_test_rand_event_full (rand, start_id + i,
                                      type_min, type_max,
                                      time_min, time_max);
      gem_event_array_add (array, evt);
      gem_event_unref (evt);
    }

  return array;
}

/**
 * gem_test_rand_sample:
 * @rand: a #gem_test_Rand
 * @max: the upper limit for sample sizes (use 0 for the default)
 *
 * Returns: A random #gem_test_RandSample.
 */
gem_test_RandSample
gem_test_rand_sample (gem_test_Rand *rand, guint max)
{
  static gem_test_RandSample default_sample = {0, 0, 0, 0, 0., 0, 0};
  gem_test_RandSample sample;
  gdouble density, size, slide;
  gint n;
  guint d;

  g_return_val_if_fail (rand, default_sample);

  if (max == 0)
    max = 32 * 1024;            /* default sample size */
  else if (max > G_MAXINT)
    max = G_MAXINT;

  n = g_rand_int_range (rand->rand, 1, (gint) max);
  size = n;

  /* 1/4 change of producing holes.  */
  if (g_rand_int_range (rand->rand, 0, 5) == 0)
    {
      slide = size * g_rand_double_range (rand->rand, 1., 10.);
      density = 1 - (slide - 1) / size;
    }
  else
    {
      density = g_rand_int_range (rand->rand, 0, 101) / 100.;
      slide = (1 - density) * size + 1;
    }

  sample.seed = gem_test_rand_get_seed (rand);
  sample.input_size = (guint) g_rand_int_range (rand->rand, n, (gint) max);
  sample.win_size = (gem_EventTime) size;
  sample.win_slide = (gem_EventTime) slide;
  sample.win_density = density;
  sample.win_max_open = (guint)((sample.win_size / sample.win_slide) + 1);

  d = sample.win_max_open - 1;
  sample.win_overlap_area = (guint) size * (2 * d + 1)
    - (guint) slide * (d * d + d);

  g_print ("### seed: %u\n", sample.seed);
  g_print ("### input-size: %u\n", sample.input_size);
  g_print ("### win-size: %"G_GUINT64_FORMAT"\n", sample.win_size);
  g_print ("### win-slide: %"G_GUINT64_FORMAT"\n", sample.win_slide);
  g_print ("### win-density: %g\n", sample.win_density);
  g_print ("### win-max-open: %u\n", sample.win_max_open);
  g_print ("### win-overlap-area: %u\n", sample.win_overlap_area);
  g_print ("###\n");

  return sample;
}
