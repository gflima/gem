#!/bin/bash
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

. init.sh
RESULTS=""
RESULTS="$RESULTS runtime saves-runtime restore-runtime"
RESULTS="$RESULTS ack-countdown acks"
RESULTS="$RESULTS saves saves-out-ids saves-skip-pairs saves-skip-pairs-range"
RESULTS="$RESULTS evts-out duplicates"
RESULTS="$RESULTS inq-max-length replayed replayed-dups"
header=$(make_header $RESULTS)
results=$(make_results $RESULTS)

runs=50
size=1m

win="1000"
slides="50 800"
acks="8 16 32 64 128 256 512"

mkdir -p "$PREFIX"
for type in "0-9"; do
  for pat in "01234"; do
    for slide in $slides; do
      for ack in $acks; do
        out="$PREFIX/$type.$pat.$slide.$ack"
        if test -f "$out"; then
          continue
        fi

        base="$out"
        echo "$header" >"$base"

        skip="$out.skip"
        echo "$header" >"$skip"

        for ((i=1; i<=$runs; i++)); do
          rand=`shuf -i1-65536 -n 1`
          set -x
          $RUN --type "$type"\
               --pattern "$pat"\
               --time 1\
               --window "$win/$slide"\
               --input-size "$size"\
               --runs "1"\
               --ack-countdown "$ack"\
               --seed "$rand"\
               --fail "50%"\
               --duplicates\
               --no-skip\
               $results\
               >>"$base"

          $RUN --type "$type"\
               --pattern "$pat"\
               --time 1\
               --window "$win/$slide"\
               --input-size "$size"\
               --runs "1"\
               --ack-countdown "$ack"\
               --seed "$rand"\
               --fail "50%"\
               --duplicates\
               $results\
               >>"$skip"
          set +x
        done

        # Generate average.
        cat "$base" | ./average.lua - >"$base.avg"
        cat "$skip" | ./average.lua - >"$skip.avg"

        # Generate summary.
        cut -f 13,19,22,25,28,31,40 "$skip.avg" > "$skip.sum"

        # Generate runtime stack.
        stack="$base.stack"
        echo -e "mode\truntime\tsaves\trestore" >"$stack"
        echo -ne "base\t" >>"$stack"
        cut -f 1,4,7 "$base.avg" | tail -n 1 >>"$stack"
        echo -ne "skip\t" >>"$stack"
        cut -f 1,4,7 "$skip.avg" | tail -n 1 >>"$stack"
      done
    done
  done
done
