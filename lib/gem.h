/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#ifndef GEM_H
#define GEM_H

#include <glib.h>
#include <gemconf.h>

#if defined GEM_BUILDING && defined HAVE_VISIBILITY && HAVE_VISIBILITY
# define GEM_API __attribute__((__visibility__("default")))
#elif defined GEM_BUILDING && defined _MSC_VER && !defined GEM_STATIC
# define GEM_API __declspec(dllexport)
#elif defined _MSC_VER && !defined GEM_STATIC
# define GEM_API __declspec(dllimport)
#else
# define GEM_API
#endif

#define GEM_BEGIN_DECLS G_BEGIN_DECLS
#define GEM_END_DECLS   G_END_DECLS

GEM_BEGIN_DECLS

#define GEM_VERSION_ENCODE(major, minor, micro) \
  (((major)   * 10000)                          \
   + ((minor) * 100)                            \
   + ((micro) * 1))

#define GEM_VERSION                             \
  GEM_VERSION_ENCODE (GEM_VERSION_MAJOR,        \
                      GEM_VERSION_MINOR,        \
                      GEM_VERSION_MICRO)

#define GEM_VERSION_STRINGIZE_(major, minor, micro)\
  #major"."#minor"."#micro

#define GEM_VERSION_STRINGIZE(major, minor, micro)\
  GEM_VERSION_STRINGIZE_(major, minor, micro)

#define GEM_VERSION_STRING                      \
  GEM_VERSION_STRINGIZE (GEM_VERSION_MAJOR,     \
                         GEM_VERSION_MINOR,     \
                         GEM_VERSION_MICRO)

GEM_API gint
gem_version (void);

GEM_API const gchar *
gem_version_string (void);

/******************************* Event API ********************************/

/**
 * gem_EventID:
 *
 * Holds the id of a #gem_Event.
 */
typedef guint64 gem_EventID;

/**
 * gem_EventType:
 *
 * Holds the type of a #gem_Event.
 */
typedef guint32 gem_EventType;

/**
 * gem_EventTime:
 *
 * Holds the time (in microseconds) of a #gem_Event.
 */
typedef guint64 gem_EventTime;

#define GEM_EVENT_ID_NONE   ((gem_EventID) -1)
#define GEM_EVENT_TYPE_NONE ((gem_EventType) -1)
#define GEM_EVENT_TIME_NONE ((gem_EventTime) -1)

#define GEM_EVENT_ID_IS_VALID(id)\
  (((gem_EventID) id) != GEM_EVENT_ID_NONE)

#define GEM_EVENT_TYPE_IS_VALID(type)\
  (((gem_EventType) type) != GEM_EVENT_TYPE_NONE)

#define GEM_EVENT_TIME_IS_VALID(time)\
  (((gem_EventTime) time) != GEM_EVENT_TIME_NONE)

/**
 * gem_Event:
 * @id: id of the event
 * @type: type of the event
 * @time: time of the event
 * @match: array of matched events or %NULL if event is primitive
 *
 * A primitive or complex event.
 */
typedef struct _gem_Event gem_Event;

/**
 * gem_EventArray:
 *
 * Resizable array of pointers to #gem_Event.
 */
typedef GPtrArray gem_EventArray;

struct _gem_Event
{
  gem_EventID id;
  gem_EventType type;
  gem_EventTime time;
  gem_EventArray *match;
};

GEM_API gem_Event *
gem_event_new (gem_EventID id,
               gem_EventType type,
               gem_EventTime time,
               gem_EventArray *match);

GEM_API gem_Event *
gem_event_ref (gem_Event *evt);

GEM_API void
gem_event_unref (gem_Event *evt);

GEM_API guint
gem_event_hash (const gem_Event *evt);

#define gem_event_has_id(evt)    (GEM_EVENT_ID_IS_VALID ((evt)->id))
#define gem_event_has_type(evt)  (GEM_EVENT_TYPE_IS_VALID ((evt)->type))
#define gem_event_has_time(evt)  (GEM_EVENT_TIME_IS_VALID ((evt)->time))
#define gem_event_has_match(evt) ((evt)->match != NULL)

#define gem_event_get_id(evt)    ((evt)->id)
#define gem_event_get_type(evt)  ((evt)->type)
#define gem_event_get_time(evt)  ((evt)->time)
#define gem_event_get_match(evt) ((evt)->match)

GEM_API gboolean
gem_event_shallow_equal (const gem_Event *evt1,
                         const gem_Event *evt2);

GEM_API gboolean
gem_event_equal (const gem_Event *evt1,
                 const gem_Event *evt2);

GEM_API gboolean
gem_event_equal_hash (const gem_Event *evt1,
                      const gem_Event *evt2);

GEM_API GString *
gem_event_to_string (const gem_Event *evt);

GEM_API GString *
gem_event_append_to_string (GString *str,
                            const gem_Event *evt);

GEM_API gem_EventArray *
gem_event_array_new (void);

GEM_API gem_EventArray *
gem_event_array_sized_new (guint reserved_size);

GEM_API gem_EventArray *
gem_event_array_ref (gem_EventArray *array);

GEM_API void
gem_event_array_unref (gem_EventArray *array);

GEM_API guint
gem_event_array_hash (gem_EventArray *array);

GEM_API void
gem_event_array_add (gem_EventArray *array,
                     gem_Event *evt);

/**
 * gem_event_array_index:
 * @array: a #gem_EventArray
 * @index: an index in the array
 *
 * Returns: (transfer none): The #gem_Event stored at @index in @array.
 */
#define gem_event_array_index(array, index)\
  ((gem_Event *)(g_ptr_array_index (array, index)))

GEM_API GString *
gem_event_array_to_string (const gem_EventArray *array);

GEM_API GString *
gem_event_array_append_to_string (GString *str,
                                  const gem_EventArray *array);

/****************************** Pattern API *******************************/

/**
 * gem_Pattern:
 *
 * State machine for matching events in a sequence.
 */
typedef struct _gem_Pattern gem_Pattern;

/**
 * gem_PatternAction:
 * @GEM_PATTERN_SKIP: Skip event.
 * @GEM_PATTERN_COLLECT: Collect event into partial match.
 * @GEM_PATTERN_COLLECT_AND_COMPLETE: Collect event into partial match and
 *   complete the match, generating an output event.
 * @GEM_PATTERN_COLLECT_AND_EXPAND: Collect event into partial match and
 *   expand the match, that is, complete it but do not generate an output
 *   event yet.  This action is used to implement the Kleene plus operator.
 * @GEM_PATTERN_CLEAR: Remove all previously collected events from partial
 *   match.
 * @GEM_PATTERN_CLEAR_AND_COLLECT: Remove all previously collected events
 *   from partial match and then collect event.
 *
 * Action to be performed on the last event fed to #gem_Pattern.
 */
typedef enum
{
  GEM_PATTERN_SKIP,
  GEM_PATTERN_COLLECT,
  GEM_PATTERN_COLLECT_AND_COMPLETE,
  GEM_PATTERN_COLLECT_AND_EXPAND,
  GEM_PATTERN_CLEAR,
  GEM_PATTERN_CLEAR_AND_COLLECT,
} gem_PatternAction;

/**
 * gem_PatternFunc:
 * @pat: the #gem_Pattern
 * @evt: the input #gem_Event
 * @id: (out): the id of the output event
 * @type: (out): the type of the output event
 * @time: (out): the time of the output event
 * @data: user data attached to @pat
 *
 * Type of function called by a #gem_Pattern each time gem_pattern_match()
 * is called.  The #gem_Pattern calls this function to decide what to do
 * with @evt.  The parameters @id, @type, and @time are used only when the
 * match is completed.
 *
 * Returns: The action to be performed on @evt.
 */
typedef gem_PatternAction (*gem_PatternFunc) (gem_Pattern *pat,
                                              gem_Event *evt,
                                              gem_EventID *id,
                                              gem_EventType *type,
                                              gem_EventTime *time,
                                              gpointer data);
/**
 * gem_PatternDataInitFunc:
 * @pat: the #gem_Pattern
 * @data: the user data to be initialized
 *
 * Type of function called by a #gem_Pattern to initialize @data after
 * allocating it.
 */
typedef void (*gem_PatternDataInitFunc) (gem_Pattern *pat,
                                         gpointer data);

/**
 * gem_PatternDataFiniFunc:
 * @pat: the #gem_Pattern
 * @data: the user data to be finalized
 *
 * Type of function called by a #gem_Pattern to finalize @data before
 * releasing it.
 */
typedef void (*gem_PatternDataFiniFunc) (gem_Pattern *pat,
                                         gpointer data);

/**
 * gem_PatternConfig:
 * @func: the pattern function
 * @data_init: the function to initialize user data
 * @data_fini: the function to finalize user data
 * @data_size: the size (in bytes) that should be allocated for user data
 *
 * Holds the configuration of a #gem_Pattern.
 */
typedef struct _gem_PatternConfig gem_PatternConfig;

struct _gem_PatternConfig
{
  gem_PatternFunc func;
  gem_PatternDataInitFunc data_init;
  gem_PatternDataFiniFunc data_fini;
  gsize data_size;
};

/**
 * GEM_PATTERN_CONFIG_INIT:
 *
 * The empty initializer for a #gem_PatternConfig.
 */
#define GEM_PATTERN_CONFIG_INIT {NULL, NULL, NULL, 0}

GEM_API gem_Pattern *
gem_pattern_new (const gem_PatternConfig *cfg);

GEM_API gem_Pattern *
gem_pattern_ref (gem_Pattern *pat);

GEM_API void
gem_pattern_unref (gem_Pattern *pat);

GEM_API gem_PatternConfig
gem_pattern_get_config (const gem_Pattern *pat);

GEM_API gem_Event *
gem_pattern_match (gem_Pattern *pat,
                   gem_Event *evt,
                   gem_PatternAction *act);

GEM_API gem_Event *
gem_pattern_match_complete (gem_Pattern *pat);

GEM_API void
gem_pattern_reset (gem_Pattern *pat);

/****************************** Operator API ******************************/

/**
 * gem_Operator:
 *
 * A complex event a processor that searches for a pattern in parallel on
 * overlapping selections of the input sequence.  These selections, called
 * windows, depend only on the time of input events.
 */
typedef struct _gem_Operator gem_Operator;

/**
 * gem_OperatorFlags:
 * @GEM_OPERATOR_FLAG_SAVE: Keep the data of all windows closed until
 *   gem_operator_save() is called.  If this flag is not set,
 *   gem_operator_save() will fail.  On by default.
 * @GEM_OPERATOR_FLAG_OUTPUT_IDS:  Collect output ids in
 *   savepoints.  This is required to ensure the correct operation of
 *   gem_operator_restore().  On by default.
 * @GEM_OPERATOR_FLAG_SKIP_RANGES: Collect and store in savepoint the ranges
 *   of skipped input events.  Off by default.
 * @GEM_OPERATOR_FLAG_STATISTICS: Collect statistics.  Expect a slowdown.
 *   Off by default.
 * @GEM_OPERATOR_FLAG_TRACE: Print trace messages using g_debug().  Expect a
 *   slowdown.  Off by default.
 *
 * Flags used to control the behavior of a #gem_Operator.
 */
typedef enum
{
  GEM_OPERATOR_FLAG_SAVE        = 1 << 1,
  GEM_OPERATOR_FLAG_OUTPUT_IDS  = 1 << 2,
  GEM_OPERATOR_FLAG_SKIP_RANGES = 1 << 3,
  GEM_OPERATOR_FLAG_STATISTICS  = 1 << 4,
  GEM_OPERATOR_FLAG_TRACE       = 1 << 5,
} gem_OperatorFlags;

/**
 * GEM_OPERATOR_DEFAULT_FLAGS:
 *
 * The default flags for #gem_Operator.
 * Used implicitly by gem_operator_new().
 */
#define GEM_OPERATOR_DEFAULT_FLAGS\
  (GEM_OPERATOR_FLAG_SAVE | GEM_OPERATOR_FLAG_OUTPUT_IDS)

/**
 * gem_OperatorStats:
 * @evts_pushed: total number of events pushed
 * @evts_skipped: total number of events skipped
 * @evts_popped: total number of events popped
 * @wins_allocated: total number of windows allocated
 * @wins_opened: total number of windows opened
 * @wins_closed: total number of windows closed
 * @wins_disposed: total number of windows disposed
 * @wins_max_opened: maximum number of window open simultaneously
 * @wins_max_closed: maximum number of window closed simultaneously
 * @wins_max_disposed: maximum number of window disposed simultaneously
 *
 * Statistics collected by #gem_Operator when %GEM_OPERATOR_FLAG_STATISTICS
 * are on.
 */
typedef struct _gem_OperatorStats gem_OperatorStats;

struct _gem_OperatorStats
{
  guint evts_pushed;
  guint evts_skipped;
  guint evts_popped;
  guint wins_allocated;
  guint wins_opened;
  guint wins_closed;
  guint wins_disposed;
  guint wins_max_opened;
  guint wins_max_closed;
  guint wins_max_disposed;
};

/**
 * GEM_OPERATOR_STATS_INIT:
 *
 * The empty initializer for a #gem_OperatorStats.
 */
#define GEM_OPERATOR_STATS_INIT {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

/**
 * gem_Savepoint:
 *
 * A lightweight savepoint that can be used to restore the state of a
 * #gem_Operator in case it is lost.
 */
typedef struct _gem_Savepoint gem_Savepoint;

GEM_API gem_Operator *
gem_operator_new (const gem_PatternConfig *cfg,
                  gem_EventID start_id,
                  gem_EventTime win_size,
                  gem_EventTime win_slide);

GEM_API gem_Operator *
gem_operator_new_with_flags (const gem_PatternConfig *cfg,
                             gem_EventID start_id,
                             gem_EventTime win_size,
                             gem_EventTime win_slide,
                             gem_OperatorFlags flags);

GEM_API gem_Operator *
gem_operator_ref (gem_Operator *op);

GEM_API void
gem_operator_unref (gem_Operator *op);

GEM_API gem_PatternConfig
gem_operator_get_config (const gem_Operator *op);

GEM_API gem_EventID
gem_operator_get_start_id (const gem_Operator *op);

GEM_API gem_EventTime
gem_operator_get_window_size (const gem_Operator *op);

GEM_API gem_EventTime
gem_operator_get_window_slide (const gem_Operator *op);

GEM_API gem_OperatorFlags
gem_operator_get_flags (const gem_Operator *op);

GEM_API gboolean
gem_operator_has_flags (const gem_Operator *op,
                        gem_OperatorFlags flags);

GEM_API gboolean
gem_operator_set_flags (gem_Operator *op,
                        gem_OperatorFlags flags);

GEM_API gboolean
gem_operator_unset_flags (gem_Operator *op,
                          gem_OperatorFlags flags);

GEM_API gem_OperatorStats
gem_operator_get_statistics (const gem_Operator *op);

GEM_API guint
gem_operator_push (gem_Operator *op,
                   gem_Event *evt);

GEM_API guint
gem_operator_flush (gem_Operator *op);

GEM_API gem_Event *
gem_operator_pop (gem_Operator *op);

GEM_API gem_Savepoint *
gem_operator_save (gem_Operator *op,
                   gem_Event *evt,
                   gboolean clear);

GEM_API void
gem_operator_restore (gem_Operator *op,
                      gem_Savepoint *save);

GEM_API gboolean
gem_operator_is_restoring (const gem_Operator *op);

GEM_API gem_Savepoint *
gem_savepoint_new (gem_EventID start_id,
                   GArray *output_ids,
                   GArray *skip_ranges);

GEM_API gem_Savepoint *
gem_savepoint_ref (gem_Savepoint *save);

GEM_API void
gem_savepoint_unref (gem_Savepoint *save);

GEM_API gem_EventID
gem_savepoint_get_start_id (const gem_Savepoint *save);

GEM_API GArray *
gem_savepoint_get_output_ids (const gem_Savepoint *save);

GEM_API GArray *
gem_savepoint_get_skip_ranges (const gem_Savepoint *save);

#endif /* GEM_H */
