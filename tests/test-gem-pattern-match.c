/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

typedef struct _abc_State
{
  gem_EventID id;
  gem_EventTime time;
  gint state;
  gboolean expand;
  gboolean skip;
} abc_State;

static void
abc_state_init (unused (gem_Pattern *pat),
                abc_State *st)
{
  memset (st, 0, sizeof (*st));
}

static void
abc_state_init_expand (gem_Pattern *pat,
                       abc_State *st)
{
  abc_state_init (pat, st);
  st->expand = TRUE;
}

static void
abc_state_init_skip (gem_Pattern *pat,
                     abc_State *st)
{
  abc_state_init (pat, st);
  st->skip = TRUE;
}

static void
abc_state_init_expand_and_skip (gem_Pattern *pat,
                                abc_State *st)
{
  abc_state_init (pat, st);
  st->expand = TRUE;
  st->skip = TRUE;
}

static gem_PatternAction
abc_func (unused (gem_Pattern *pat),
          gem_Event *evt,
          gem_EventID *id,
          gem_EventType *type,
          gem_EventTime *time,
          gpointer data)
{
  abc_State *st = (abc_State *) data;
  switch (st->state)
    {
    case 0:
      switch (evt->type)
        {
        case 'a':
          st->state = 1;
          return GEM_PATTERN_COLLECT;
        default:
          break;
        }
      break;
    case 1:
      switch (evt->type)
        {
        case 'a':
          st->state = 1;
          return st->skip
            ? GEM_PATTERN_SKIP
            : GEM_PATTERN_CLEAR_AND_COLLECT;
        case 'b':
          st->state = 2;
          return GEM_PATTERN_COLLECT;
        default:
          break;
        }
      break;
    case 2:
      switch (evt->type)
        {
        case 'c':
          st->state = 0;
          *id = st->id++;
          *type = 0xabc;
          *time = st->time++;
          return st->expand
            ? GEM_PATTERN_COLLECT_AND_EXPAND
            : GEM_PATTERN_COLLECT_AND_COMPLETE;
        default:
          break;
        }
      break;
    default:
      g_assert_not_reached ();
    }
  return st->skip ? GEM_PATTERN_SKIP : (st->state = 0, GEM_PATTERN_CLEAR);
}

static gem_EventArray *
run (const gem_PatternConfig *cfg, gem_EventArray *input)
{
  gem_Pattern *pat;
  gem_EventArray *output;
  gem_Event *out;
  guint i;

  g_assert_nonnull (cfg);
  g_assert_nonnull (input);

  pat = gem_pattern_new (cfg);
  output = gem_event_array_new ();

  for (i = 0; i < input->len; i++)
    {
      out = gem_pattern_match (pat, gem_event_array_index (input, i), NULL);
      if (out != NULL)
        {
          gem_event_array_add (output, out);
          gem_event_unref (out);
        }
    }

  out = gem_pattern_match_complete (pat);
  if (out != NULL)
    {
      gem_event_array_add (output, out);
      gem_event_unref (out);
    }

  gem_pattern_unref (pat);

  return output;
}

static gem_EventArray *
make_event_array (gem_EventType *sample, guint len)
{
  gem_EventArray *array;
  guint i;

  g_assert_nonnull (sample);

  array = gem_event_array_sized_new (len);
  for (i = 0; i < len; i++)
    {
      gem_Event *evt = gem_event_new (i, sample[i], i, NULL);
      gem_event_array_add (array, evt);
      gem_event_unref (evt);
    }

  return array;
}

gint
main (void)
{
  gem_EventType sample1[] =
    {
     'x', 'a', 'b', 'c', 'x', 'a', 'b', 'c',
    };

  gem_EventType sample2[] =
    {
     'a', 'a', 'a', 'b', 'a', 'c', 'a', 'b', 'c', 'b', 'c', 'a', 'c',
     'a', 'a', 'a', 'b', 'c', 'a', 'b', 'c', 'c', 'c', 'a', 'b', 'c'
    };

  /* Make sure the default configuration skips everything.  */
  {
    gem_test_Rand *rand;
    gem_Pattern *pat;
    guint i;

    rand = gem_test_rand_new ();
    pat = gem_pattern_new (NULL);

    for (i = 0; i < 1000; i++)
      {
        gem_Event *evt;
        gem_PatternAction act;

        evt = gem_test_rand_event (rand);
        g_assert_null (gem_pattern_match (pat, evt, &act));
        g_assert (act == GEM_PATTERN_SKIP);
        gem_event_unref (evt);
      }

    gem_test_rand_unref (rand);
  }

  /* Simple match.  */
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_EventArray *input;
    gem_EventArray *output;
    gem_Event *out;

    cfg.func = abc_func;
    cfg.data_size = sizeof (abc_State);
    cfg.data_init = (gem_PatternDataInitFunc)
      abc_state_init_expand_and_skip;

    input = make_event_array (sample1, nelementsof (sample1));
    output = run (&cfg, input);
    gem_debug_event_array (output);

    g_ptr_array_remove_index (input, 0); /* remove 1st 'x' */
    g_ptr_array_remove_index (input, 3); /* remove 2nd 'x' */
    out = gem_event_new (1, 0xabc, 1, input);
    gem_debug_event (out);

    g_assert (output->len == 1);
    g_assert (gem_event_equal (out, gem_event_array_index (output, 0)));
    g_assert (gem_event_equal_hash (out,
                                    gem_event_array_index (output, 0)));
    gem_event_unref (out);
    gem_event_array_unref (output);
    gem_event_array_unref (input);
  }

  /* Search for "abc" in SAMPLE2.  */
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_EventArray *input;
    gem_EventArray *output;
    gem_Event *out;

    cfg.func = abc_func;
    cfg.data_size = sizeof (abc_State);
    cfg.data_init = (gem_PatternDataInitFunc) abc_state_init;

    input = make_event_array (sample2, nelementsof (sample2));
    output = run (&cfg, input);
    gem_debug_event_array (output);
    g_assert (output->len == 4);

    /* match 0 */
    out = gem_event_array_index (output, 0);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 6));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 7));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 8));

    /* match 1 */
    out = gem_event_array_index (output, 1);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 15));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 16));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 17));

    /* match 2 */
    out = gem_event_array_index (output, 2);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 18));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 19));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 20));

    /* match 2 */
    out = gem_event_array_index (output, 3);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 23));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 24));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 25));

    gem_event_array_unref (input);
    gem_event_array_unref (output);
  }

  /* Search for "(abc)+" in SAMPLE2.  */
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_EventArray *input;
    gem_EventArray *output;
    gem_Event *out;

    cfg.func = abc_func;
    cfg.data_size = sizeof (abc_State);
    cfg.data_init = (gem_PatternDataInitFunc) abc_state_init_expand;

    input = make_event_array (sample2, nelementsof (sample2));
    output = run (&cfg, input);
    gem_debug_event_array (output);
    g_assert (output->len == 3);

    /* match 0 */
    out = gem_event_array_index (output, 0);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 6));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 7));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 8));

    /* match 1 */
    out = gem_event_array_index (output, 1);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 6);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 15));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 16));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 17));
    g_assert (gem_event_array_index (out->match, 3)
              == gem_event_array_index (input, 18));
    g_assert (gem_event_array_index (out->match, 4)
              == gem_event_array_index (input, 19));
    g_assert (gem_event_array_index (out->match, 5)
              == gem_event_array_index (input, 20));

    /* match 2 */
    out = gem_event_array_index (output, 2);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 23));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 24));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 25));

    gem_event_array_unref (input);
    gem_event_array_unref (output);
  }

  /* Search for "abc" in SAMPLE2 with skip policy.  */
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_EventArray *input;
    gem_EventArray *output;
    gem_Event *out;

    cfg.func = abc_func;
    cfg.data_size = sizeof (abc_State);
    cfg.data_init = (gem_PatternDataInitFunc) abc_state_init_skip;

    input = make_event_array (sample2, nelementsof (sample2));
    output = run (&cfg, input);
    gem_debug_event_array (output);
    g_assert (output->len == 5);

    /* match 0 */
    out = gem_event_array_index (output, 0);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 0));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 3));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 5));

    /* match 1 */
    out = gem_event_array_index (output, 1);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 6));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 7));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 8));

    /* match 2 */
    out = gem_event_array_index (output, 2);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 11));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 16));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 17));

    /* match 3 */
    out = gem_event_array_index (output, 3);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 18));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 19));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 20));

    /* match 4 */
    out = gem_event_array_index (output, 4);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 3);
    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 23));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 24));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 25));

    gem_event_array_unref (input);
    gem_event_array_unref (output);
  }

  /* Search for "(abc)+" in SAMPLE2 with skip policy.  */
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_EventArray *input;
    gem_EventArray *output;
    gem_Event *out;

    cfg.func = abc_func;
    cfg.data_size = sizeof (abc_State);
    cfg.data_init = (gem_PatternDataInitFunc)
      abc_state_init_expand_and_skip;

    input = make_event_array (sample2, nelementsof (sample2));
    output = run (&cfg, input);
    gem_debug_event_array (output);
    g_assert (output->len == 1);

    /* match 0 */
    out = gem_event_array_index (output, 0);
    g_assert_nonnull (out->match);
    g_assert (out->match->len == 15);

    g_assert (gem_event_array_index (out->match, 0)
              == gem_event_array_index (input, 0));
    g_assert (gem_event_array_index (out->match, 1)
              == gem_event_array_index (input, 3));
    g_assert (gem_event_array_index (out->match, 2)
              == gem_event_array_index (input, 5));
    g_assert (gem_event_array_index (out->match, 3)
              == gem_event_array_index (input, 6));
    g_assert (gem_event_array_index (out->match, 4)
              == gem_event_array_index (input, 7));
    g_assert (gem_event_array_index (out->match, 5)
              == gem_event_array_index (input, 8));
    g_assert (gem_event_array_index (out->match, 6)
              == gem_event_array_index (input, 11));
    g_assert (gem_event_array_index (out->match, 7)
              == gem_event_array_index (input, 16));
    g_assert (gem_event_array_index (out->match, 8)
              == gem_event_array_index (input, 17));
    g_assert (gem_event_array_index (out->match, 9)
              == gem_event_array_index (input, 18));
    g_assert (gem_event_array_index (out->match, 10)
              == gem_event_array_index (input, 19));
    g_assert (gem_event_array_index (out->match, 11)
              == gem_event_array_index (input, 20));
    g_assert (gem_event_array_index (out->match, 12)
              == gem_event_array_index (input, 23));
    g_assert (gem_event_array_index (out->match, 13)
              == gem_event_array_index (input, 24));
    g_assert (gem_event_array_index (out->match, 14)
              == gem_event_array_index (input, 25));

    gem_event_array_unref (input);
    gem_event_array_unref (output);
  }

  exit (EXIT_SUCCESS);
}
