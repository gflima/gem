# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

ME="${0##*/}"; ME="${ME%.sh}"
PREFIX="$ME.out"
RUN="../tests/gem-benchmark"

PATS_SKIP="0 01 012 0123 01234"
PATS_STAR="0+ (01)+ (012)+"
TYPES="0-9 0-19 0-49 0-99"

make_header()
{
  str=""
  for w in "$@"; do
    str="$str$w\t"
  done
  echo -e "$str"
}

make_results()
{
  str=""
  for w in "$@"; do
    str="$str -r $w"
  done
  echo "$str"
}
