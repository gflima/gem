/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  GEM_TEST ("No-op");
  {
    gem_Operator *op;

    op = gem_operator_new (NULL, 0, 1, 1);
    g_assert_null (gem_operator_save (op, NULL, TRUE));

    gem_operator_unref (op);
  }

  GEM_TEST ("Always fail if SAVE flag is off");
  {
    gem_Operator *op;
    gem_Event *evt;
    guint i;

    op = gem_operator_new (NULL, 0, 1, 1);
    gem_operator_unset_flags (op, GEM_OPERATOR_FLAG_SAVE);

    for (i = 0; i < 10; i++)
      {
        evt = gem_event_new (i, 0, i, NULL);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    g_assert_null (gem_operator_save (op, NULL, TRUE));
    g_assert_null (gem_operator_save (op, NULL, FALSE));

    for (i = 10; i < 100; i++)
      {
        evt = gem_event_new (i, 0, i, NULL);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    g_assert_null (gem_operator_save (op, NULL, TRUE));
    g_assert_null (gem_operator_save (op, NULL, FALSE));

    gem_operator_unref (op);
  }

  GEM_TEST ("@evt has no window");
  {
    gem_Operator *op;
    gem_Event *evt;
    guint i;

    op = gem_operator_new (NULL, 0, 1, 1);

    for (i = 0; i < 10; i++)
      {
        evt = gem_event_new (i, 0, i, NULL);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    evt = gem_event_new (0, 0, 0, NULL);
    g_assert_null (gem_operator_save (op, evt, TRUE)); /* no win */
    gem_event_unref (evt);

    evt = gem_event_new (10, 0, 10, NULL);
    gem_operator_push (op, evt);
    g_assert_null (gem_operator_save (op, evt, TRUE)); /* no match */
    gem_event_unref (evt);

    gem_operator_unref (op);
  }

  GEM_TEST ("@evt NULL and there are no open windows");
  {
    gem_Operator *op;
    gem_Event *evt;
    gem_Savepoint *save;
    guint i;

    op = gem_operator_new (NULL, 0, 1, 5);

    evt = gem_event_new (1, 0, 1, NULL);
    gem_operator_push (op, evt);
    gem_event_unref (evt);

    evt = gem_event_new (2, 0, 2, NULL);
    gem_operator_push (op, evt);
    gem_event_unref (evt);

    evt = gem_event_new (4, 0, 4, NULL);
    gem_operator_push (op, evt);
    gem_event_unref (evt);

    for (i = 0; i < 100; i++)
      {
        save = gem_operator_save (op, NULL, FALSE);
        g_assert_nonnull (save);
        g_assert (gem_savepoint_get_start_id (save) == 1);
        g_assert_nonnull (gem_savepoint_get_output_ids (save));
        g_assert (gem_savepoint_get_output_ids (save)->len == 1);
        gem_savepoint_unref (save);
      }

    save = gem_operator_save (op, NULL, TRUE);
    g_assert_nonnull (save);
    g_assert (gem_savepoint_get_start_id (save) == 1);
    g_assert_nonnull (gem_savepoint_get_output_ids (save));
    g_assert (gem_savepoint_get_output_ids (save)->len == 1);
    gem_savepoint_unref (save);

    for (i = 0; i < 100; i++)
      {
        g_assert_null (gem_operator_save (op, NULL, TRUE));
        g_assert_null (gem_operator_save (op, NULL, FALSE));
      }

    gem_operator_unref (op);
  }

  GEM_TEST ("@evt NULL and no window to dispose");
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Event *evt;

    cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY);
    op = gem_operator_new (&cfg, 0, 1, 1);

    evt = gem_event_new (0, 0, 0, NULL);
    g_assert (gem_operator_push (op, evt) == 1);
    gem_event_unref (evt);

    evt = gem_event_new (1, 0, 1, NULL);
    g_assert (gem_operator_push (op, evt) == 2);
    gem_event_unref (evt);

    g_assert_null (gem_operator_save (op, NULL, TRUE));
    gem_operator_unref (op);
  }

  GEM_TEST ("@evt NULL and no output events");
  {
    gem_Operator *op;
    gem_Savepoint *save;
    guint i;

    op = gem_operator_new (NULL, 0, 1, 1);
    for (i = 0; i < 50; i++)
      {
        gem_Event *evt = gem_event_new (i, 0, i, NULL);
        g_assert (gem_operator_push (op, evt) == 0);
        gem_event_unref (evt);
      }

    for (i = 0; i < 100; i++)
      {
        save = gem_operator_save (op, NULL, FALSE);
        g_assert_nonnull (save);
        g_assert (gem_savepoint_get_start_id (save) == 48);
        g_assert_nonnull (gem_savepoint_get_output_ids (save));
        g_assert (gem_savepoint_get_output_ids (save)->len == 1);
        gem_savepoint_unref (save);
      }

    save = gem_operator_save (op, NULL, TRUE);
    g_assert_nonnull (save);
    g_assert (gem_savepoint_get_start_id (save) == 48);
    g_assert_nonnull (gem_savepoint_get_output_ids (save));
    g_assert (gem_savepoint_get_output_ids (save)->len == 1);
    gem_savepoint_unref (save);

    for (i = 0; i < 100; i++)
      {
        g_assert_null (gem_operator_save (op, NULL, TRUE));
        g_assert_null (gem_operator_save (op, NULL, FALSE));
      }

    gem_operator_unref (op);
  }

  GEM_TEST ("@evt NULL and output event in closed window");
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Event *evt;
    guint i;

    gem_Savepoint *save;
    const GArray *out_ids;

    cfg = gem_test_pattern (GEM_TEST_PATTERN_1);
    op = gem_operator_new (&cfg, 0, 1, 1);

    evt = gem_event_new (0, 0, 0, NULL);
    g_assert (gem_operator_push (op, evt) == 0);
    gem_event_unref (evt);

    evt = gem_event_new (1, 0, 1, NULL);
    g_assert (gem_operator_push (op, evt) == 0);
    gem_event_unref (evt);

    evt = gem_event_new (2, 1, 2, NULL);
    g_assert (gem_operator_push (op, evt) == 2);
    gem_event_unref (evt);

    for (i = 3; i < 50; i++)
      {
        evt = gem_event_new (i, 0, i, NULL);
        g_assert (gem_operator_push (op, evt) == 0);
        gem_event_unref (evt);
      }

    for (i = 0; i < 100; i++)
      {
        save = gem_operator_save (op, NULL, FALSE);
        g_assert (gem_savepoint_get_start_id (save) == 2);
        out_ids = gem_savepoint_get_output_ids (save);
        g_assert (out_ids->len == 1);
        g_assert (g_array_index (out_ids, gem_EventID, 0) == 1);
        gem_savepoint_unref (save);
      }

    save = gem_operator_save (op, NULL, TRUE);
    g_assert (gem_savepoint_get_start_id (save) == 2);
    out_ids = gem_savepoint_get_output_ids (save);
    g_assert (out_ids->len == 1);
    g_assert (g_array_index (out_ids, gem_EventID, 0) == 1);
    gem_savepoint_unref (save);

    for (i = 0; i < 100; i++)
      {
        g_assert_null (gem_operator_save (op, NULL, TRUE));
        g_assert_null (gem_operator_save (op, NULL, FALSE));
      }

    evt = gem_operator_pop (op);
    gem_event_unref (evt);

    evt = gem_operator_pop (op);
    gem_event_unref (evt);

    for (i = 0; i < 100; i++)
      {
        save = gem_operator_save (op, NULL, FALSE);
        g_assert (gem_savepoint_get_start_id (save) == 48);
        g_assert_nonnull (gem_savepoint_get_output_ids (save));
        g_assert (gem_savepoint_get_output_ids (save)->len == 1);
        gem_savepoint_unref (save);
      }

    save = gem_operator_save (op, NULL, TRUE);
    g_assert (gem_savepoint_get_start_id (save) == 48);
    g_assert_nonnull (gem_savepoint_get_output_ids (save));
    g_assert (gem_savepoint_get_output_ids (save)->len == 1);
    gem_savepoint_unref (save);

    for (i = 0; i < 100; i++)
      {
        g_assert_null (gem_operator_save (op, NULL, TRUE));
        g_assert_null (gem_operator_save (op, NULL, FALSE));
      }

    gem_operator_unref (op);
  }

  GEM_TEST ("@evt NULL and output event in open window");
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Event *evt;
    guint i;

    gem_Savepoint *save;
    const GArray *out_ids;

    cfg = gem_test_pattern (GEM_TEST_PATTERN_1);
    op = gem_operator_new (&cfg, 0, 2, 1);

    for (i = 0; i <= 5; i++)
      {
        evt = gem_event_new (i, i == 3 ? 1 : 0, i, NULL);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    for (i = 0; i < 100; i++)
      {
        save = gem_operator_save (op, NULL, FALSE);
        g_assert (gem_savepoint_get_start_id (save) == 2);
        out_ids = gem_savepoint_get_output_ids (save);
        g_assert (out_ids->len == 2);
        g_assert (g_array_index (out_ids, gem_EventID, 0) == 1);
        g_assert (g_array_index (out_ids, gem_EventID, 1) == 2);
        gem_savepoint_unref (save);
      }

    save = gem_operator_save (op, NULL, TRUE);
    g_assert (gem_savepoint_get_start_id (save) == 2);
    out_ids = gem_savepoint_get_output_ids (save);
    g_assert (out_ids->len == 2);
    g_assert (g_array_index (out_ids, gem_EventID, 0) == 1);
    g_assert (g_array_index (out_ids, gem_EventID, 1) == 2);
    gem_savepoint_unref (save);

    for (i = 0; i < 100; i++)
      {
        g_assert_null (gem_operator_save (op, NULL, TRUE));
        g_assert_null (gem_operator_save (op, NULL, FALSE));
      }

    gem_operator_unref (op);
  }

  GEM_TEST ("The canonical interleaved case");
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Event *evt;
    guint i;

    gem_Savepoint *save;
    const GArray *out_ids;

    cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY_MOD2);
    op = gem_operator_new (&cfg, 0, 3, 1);

    for (i = 0; i <= 7; i++)
      {
        evt = gem_event_new (i, i == 3 ? 1 : 0, i, NULL);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    while (TRUE)
      {
        evt = gem_operator_pop (op);
        g_assert_nonnull (evt);
        if (evt->id == 10)
          break;
        gem_event_unref (evt);
      }

    for (i = 0; i < 100; i++)
      {
        save = gem_operator_save (op, evt, FALSE);
        g_assert_nonnull (save);
        g_assert (gem_savepoint_get_start_id (save) == 4);
        out_ids = gem_savepoint_get_output_ids (save);
        g_assert (out_ids->len == 2);
        g_assert (g_array_index (out_ids, gem_EventID, 0) == 7);
        g_assert (g_array_index (out_ids, gem_EventID, 1) == 10);
        gem_savepoint_unref (save);
      }

    save = gem_operator_save (op, evt, TRUE);
    g_assert_nonnull (save);
    g_assert (gem_savepoint_get_start_id (save) == 4);
    out_ids = gem_savepoint_get_output_ids (save);
    g_assert (out_ids->len == 2);
    g_assert (g_array_index (out_ids, gem_EventID, 0) == 7);
    g_assert (g_array_index (out_ids, gem_EventID, 1) == 10);
    gem_savepoint_unref (save);

    for (i = 0; i < 100; i++)
      {
        g_assert_null (gem_operator_save (op, NULL, TRUE));
        g_assert_null (gem_operator_save (op, NULL, FALSE));
      }

    gem_operator_unref (op);
  }

  GEM_TEST ("Force some skipped ranges");
  {
    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Event *evt;
    gem_Savepoint *save;
    GArray *array;
    guint i;

    cfg = gem_test_pattern (GEM_TEST_PATTERN_1);
    op = gem_operator_new (&cfg, 0, 5, 3);
    gem_operator_set_flags (op, GEM_OPERATOR_FLAG_SKIP_RANGES);

    for (i = 0; i <= 15; i++)
      {
        evt = gem_event_new (i, i % 5 == 0 ? 1 : 0, i, NULL);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    save = gem_operator_save (op, NULL, TRUE);

    array = gem_savepoint_get_output_ids (save);
    g_assert (array->len == 2);
    g_assert (g_array_index (array, gem_EventID, 0) == 5);
    g_assert (g_array_index (array, gem_EventID, 1) == 6);

    array = gem_savepoint_get_skip_ranges (save);
    g_assert (array->len == 2);
    g_assert (g_array_index (array, gem_EventID, 0) == 13);
    g_assert (g_array_index (array, gem_EventID, 1) == 14);
    g_assert_nonnull (save);

    gem_savepoint_unref (save);
    gem_operator_unref (op);
  }

  exit (EXIT_SUCCESS);
}
