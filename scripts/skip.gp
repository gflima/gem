#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load 'init.gp'
@INIT_HIST

NAME = 'skip'
PREFIX = NAME.'.out/0-9.01234.'

set style histogram gap 1
set size ratio 0.45
set xtics format '%g'
set format y '%g'

TITLE = "Recovering from one failure at the middle of processing\n\
{/*.99 {/:Bold Pattern \"abcde\", Types a-j}}"

SUBTITLE = sprintf ('{/*.9 %s}',\
  '(50 runs, 1M events per run)')

# set title TITLE."\n".SUBTITLE

XTICLABELS = 'xticlabels(stringcolumn(12))'
