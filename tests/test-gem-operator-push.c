/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  GEM_TEST ("Never match");
  {
    gem_test_Rand *rand;
    gem_test_RandSample sample;

    gem_Operator *op;
    gem_EventArray *input;

    const GQueue *open;
    const GQueue *closed;
    const GQueue *output;
    guint i;

    gboolean save_off;
    gboolean skip_on;

    save_off = FALSE;
    skip_on = FALSE;

  redo_1:
    rand = gem_test_rand_new ();
    sample = gem_test_rand_sample (rand, 0);
    input = gem_test_rand_event_array_full (rand, sample.input_size,
                                            0, 0, 0, 0, 0);

    op = gem_operator_new (NULL, 0, sample.win_size, sample.win_slide);

    if (save_off)
      gem_operator_unset_flags (op, GEM_OPERATOR_FLAG_SAVE);

    if (skip_on)
      gem_operator_set_flags (op, GEM_OPERATOR_FLAG_SKIP_RANGES);

    open = gem_operator_get_open_windows (op);
    closed = gem_operator_get_closed_windows (op);
    output = gem_operator_get_output (op);

    g_assert (open->length == 0);
    g_assert (closed->length == 0);
    g_assert (output->length == 0);

    for (i = 0; i < input->len; i++)
      {
        gem_Event *evt;
        evt = gem_event_array_index (input, i);
        evt->time += i;

        g_assert (gem_operator_push (op, evt) == 0);
        g_assert (open->length <= sample.win_max_open);
        if (!save_off)
          {
            g_assert (open->length + closed->length
                      == (guint)(((gdouble) i
                                  / (gdouble) sample.win_slide) + 1));
          }
        else
          {
            g_assert (closed->length == 0);
          }
      }

    g_assert (output->length == 0);

    gem_operator_unref (op);
    gem_event_array_unref (input);
    gem_test_rand_unref (rand);

    if (!save_off)
      {
        save_off = TRUE;
        goto redo_1;
      }

    if (!skip_on)
      {
        skip_on = TRUE;
        goto redo_1;
      }
  }

  GEM_TEST ("Always match");
  {
    gem_test_Rand *rand;
    gem_test_RandSample sample;
    gem_EventArray *input;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    const GQueue *open;
    const GQueue *closed;
    const GQueue *output;

    guint i;
    GList *ell;
    guint total;

    gboolean save_off;
    gboolean skip_on;

    save_off = FALSE;
    skip_on = FALSE;
  redo_2:

    rand = gem_test_rand_new ();
    sample = gem_test_rand_sample (rand, 0);

    input = gem_test_rand_event_array_full (rand, sample.input_size,
                                            0, 0, 0, 0, 0);

    cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY);
    op = gem_operator_new (&cfg, 0, sample.win_size, sample.win_slide);

    if (save_off)
      gem_operator_unset_flags (op, GEM_OPERATOR_FLAG_SAVE);

    if (skip_on)
      gem_operator_set_flags (op, GEM_OPERATOR_FLAG_SKIP_RANGES);

    open = gem_operator_get_open_windows (op);
    closed = gem_operator_get_closed_windows (op);
    output = gem_operator_get_output (op);

    g_assert (open->length == 0);
    g_assert (closed->length == 0);
    g_assert (output->length == 0);

    for (i = 0; i < input->len; i++)
      {
        gem_Event *evt;
        guint n;

        evt = gem_event_array_index (input, i);
        evt->time += i;
        n = gem_operator_push (op, evt);

        if (sample.win_slide <= sample.win_size)
          g_assert (n >= 1);
        g_assert_cmpint (n, <=, sample.win_max_open);
      }

    /* Accumulate the number of events output per open window.  */
    total = 0;
    for (ell = open->head; ell != NULL; ell = ell->next)
      {
        const gem_Event *first = gem_window_get_first (ell->data);
        total += input->len - (guint) first->time;
      }

    g_print ("total in open windows = %u\n", total);
    g_print ("total in closed windows = %u\n",
             (guint)(closed->length * (sample.win_size + 1)));

    if (!save_off)
      {
        g_assert (output->length == total
                  + closed->length * (sample.win_size + 1));
      }

    gem_operator_unref (op);
    gem_event_array_unref (input);
    gem_test_rand_unref (rand);

    if (!save_off)
      {
        save_off = TRUE;
        goto redo_2;
      }

    if (!skip_on)
      {
        skip_on = TRUE;
        goto redo_2;
      }
  }

  GEM_TEST ("Completely random run");
  {
    gem_test_Rand *rand;
    gem_test_RandSample sample;
    gem_EventArray *input;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;

    const GQueue *open;
    const GQueue *closed;
    const GQueue *output;

    guint i;
    gem_EventTime ts;

    gboolean save_off;
    gboolean skip_on;

    save_off = FALSE;
    skip_on = FALSE;
  redo_3:

    rand = gem_test_rand_new ();
    sample = gem_test_rand_sample (rand, 0);
    input = gem_test_rand_event_array_full (rand, sample.input_size,
                                            0, 0, 0, 0, 0);
    cfg = gem_test_pattern (GEM_TEST_PATTERN_WACKY);
    op = gem_operator_new (&cfg, 0, sample.win_size, sample.win_slide);

    if (save_off)
      gem_operator_unset_flags (op, GEM_OPERATOR_FLAG_SAVE);

    if (skip_on)
      gem_operator_set_flags (op, GEM_OPERATOR_FLAG_SKIP_RANGES);

    open = gem_operator_get_open_windows (op);
    closed = gem_operator_get_closed_windows (op);
    output = gem_operator_get_output (op);

    g_assert (open->length == 0);
    g_assert (closed->length == 0);
    g_assert (output->length == 0);

    ts = 0;
    for (i = 0; i < input->len; i++)
      {
        gem_Event *evt;

        evt = gem_event_array_index (input, i);
        ts += (guint) g_random_int_range (0, (gint)(i + 1));
        evt->time = ts;
        gem_operator_push (op, evt);
        g_assert (open->length <= sample.win_max_open);
      }

    gem_operator_unref (op);
    gem_event_array_unref (input);
    gem_test_rand_unref (rand);

    if (!save_off)
      {
        save_off = TRUE;
        goto redo_3;
      }

    if (!skip_on)
      {
        skip_on = TRUE;
        goto redo_3;
      }
  }

  exit (EXIT_SUCCESS);
}
