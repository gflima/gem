#!/usr/bin/env lua
-- Copyright (C) 2018 Guilherme F. Lima
-- This file is part of Gem.
--
-- Gem is free software: you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Gem is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
-- License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Gem.  If not, see <https://www.gnu.org/licenses/>.

local optparse = require'optparse'
local ME = arg[0]

local function printerr (fmt, ...)
   assert (type (fmt) == 'string')
   io.stderr:write ('*** ERROR: ', fmt:format (...), '\n')
end

do
   -- Parse command-line options.
   local parser = optparse (([[
%s
Usage: %s [OPTIONS]

Options:
  --no-header     Don't print header
  --no-min-max    Don't print min and max
  --help          Display this help, then exit
]]):format (ME, ME))
   local arg, opt = parser:parse (arg)
   opt.no_header = opt.no_header or false
   opt.no_min_max = opt.no_min_max or false

   opt.header = not opt.no_header
   opt.min_max = not opt.no_min_max

   local file
   if #arg == 0 or arg[1] == '-' then
      file = io.stdin
   else
      local _file, errmsg = io.open (arg[1])
      if not _file then
         error (errmsg)
      end
      file = _file
   end

   local lines = 0
   local min = {}
   local max = {}
   local avg = {}

   if opt.header then
      local h = {}
      for w in file:read('l'):gmatch ('([%w-]+)') do
         h[#h+1] = w
         if opt.min_max then
            h[#h+1] = 'min'
            h[#h+1] = 'max'
         end
      end
      print (table.concat (h, '\t'))
   end

   for line in file:lines () do
      local i = 1
      for n in line:gmatch ('(%d+)') do
         n = tonumber (n)
         min[i] = math.min (min[i] or math.maxinteger, n)
         max[i] = math.max (max[i] or math.mininteger, n)
         avg[i] = (avg[i] or 0) + n
         i = i + 1
      end
      lines = lines + 1
   end
   if file ~= io.stdin then
      file:close ()
   end

   for i=1,#avg do
      avg[i] = avg[i]/lines
      if opt.min_max then
         io.stdout:write (avg[i]..'\t'..min[i]..'\t'..max[i]..'\t')
      else
         io.stdout:write (avg[i]..'\t')
      end
   end
   io.stdout:write ('\n')
end
