/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  gem_Event *e0;
  gem_Event *e1;
  gem_Event *e2;
  gem_Event *e3;
  gem_EventArray *match;

  e0 = gem_event_new (0, 0, 0, NULL);
  e1 = gem_event_new (0, 1, 2, NULL);
  g_assert (gem_event_shallow_equal (e1, e1));
  g_assert (gem_event_equal (e1, e1));
  g_assert (gem_event_equal_hash (e1, e1));

  g_assert_false (gem_event_shallow_equal (e1, e0));
  g_assert_false (gem_event_equal (e1, e0));
  g_assert_false (gem_event_equal_hash (e1, e0));
  gem_event_unref (e0);

  match = gem_event_array_new ();
  gem_event_array_add (match, e1);

  e2 = gem_event_new (0, 1, 2, match);
  g_assert (gem_event_shallow_equal (e2, e2));
  g_assert (gem_event_equal (e2, e2));
  g_assert (gem_event_equal_hash (e2, e2));

  g_assert (gem_event_shallow_equal (e1, e2));
  g_assert_false (gem_event_equal (e1, e2));
  g_assert_false (gem_event_equal_hash (e1, e2));

  e3 = gem_event_new (0, 1, 2, match);
  g_assert (gem_event_shallow_equal (e3, e3));
  g_assert (gem_event_equal (e3, e3));
  g_assert (gem_event_equal_hash (e3, e3));

  g_assert (gem_event_shallow_equal (e3, e1));
  g_assert_false (gem_event_equal (e3, e1));
  g_assert_false (gem_event_equal_hash (e3, e1));

  g_assert (gem_event_shallow_equal (e3, e2));
  g_assert (gem_event_equal (e3, e2));
  g_assert (gem_event_equal_hash (e3, e2));

  gem_event_unref (e1);
  gem_event_unref (e2);
  gem_event_unref (e3);
  gem_event_array_unref (match);

  exit (EXIT_SUCCESS);
}
