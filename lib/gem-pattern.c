/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include <config.h>
#include "aux-glib.h"
#include "gem.h"

/**
 * SECTION: gem-pattern
 * @Title: Patterns
 * @Short_Description: searching for patterns in event sequences
 */

/* Pattern data.  */
struct _gem_Pattern
{
  gem_PatternConfig cfg;        /* configuration */
  struct {
    gem_EventArray *buf;        /* the match buffer */
    guint pfx_len;              /* length of complete prefix */
    gem_EventID pfx_id;         /* id of complete prefix */
    gem_EventType pfx_type;     /* type of complete prefix */
    gem_EventTime pfx_time;     /* time of complete prefix */
  } match;
  gpointer data;                /* user data */
  volatile guint ref_count;     /* reference counter */
};

/* Default pattern function.  */
static gem_PatternAction
skip_func (unused (gem_Pattern *pat),
           unused (gem_Event *evt),
           unused (gem_EventID *id),
           unused (gem_EventType *type),
           unused (gem_EventTime *time),
           unused (gpointer data))
{
  return GEM_PATTERN_SKIP;
}

/* Default configuration.  */
static gem_PatternConfig default_cfg = {skip_func, NULL, NULL, 0};

/* Reset match.  */
#define pat_reset_match(pat)                            \
  G_STMT_START                                          \
  {                                                     \
    (pat)->match.pfx_len = 0;                           \
    (pat)->match.pfx_id = GEM_EVENT_ID_NONE;            \
    (pat)->match.pfx_type = GEM_EVENT_TYPE_NONE;        \
    (pat)->match.pfx_time = GEM_EVENT_TIME_NONE;        \
  }                                                     \
  G_STMT_END

/* Initialize data.  */
#define pat_data_init(pat)                              \
  G_STMT_START                                          \
  {                                                     \
    if ((pat)->cfg.data_init)                           \
      (pat)->cfg.data_init ((pat), (pat)->data);        \
  }                                                     \
  G_STMT_END

/* Finalize data.  */
#define pat_data_fini(pat)                              \
  G_STMT_START                                          \
  {                                                     \
    if ((pat)->cfg.data_fini)                           \
      (pat)->cfg.data_fini ((pat), (pat)->data);        \
  }                                                     \
  G_STMT_END

/**
 * gem_pattern_new:
 * @cfg: (optional): a pointer to a #gem_PatternConfig.
 *
 * Creates a new #gem_Pattern with configuration @cfg.  If @cfg is %NULL,
 * then the pattern is created with the default configuration and skips any
 * event fed to it.
 *
 * Returns: (transfer full): A newly allocated #gem_Pattern with a reference
 *   count of 1.  Free with gem_pattern_unref().
 */
gem_Pattern *
gem_pattern_new (const gem_PatternConfig *cfg)
{
  gem_Pattern *pat;

  pat = g_slice_new (gem_Pattern);
  pat->cfg = (cfg) ? *cfg : default_cfg;
  if (pat->cfg.func == NULL)
    pat->cfg.func = default_cfg.func;
  pat->match.buf = gem_event_array_new ();
  pat_reset_match (pat);
  pat->data = g_slice_alloc0 (pat->cfg.data_size);
  pat_data_init (pat);
  pat->ref_count = 1;

  return pat;
}

/**
 * gem_pattern_ref:
 * @pat: a #gem_Pattern
 *
 * Increases the reference count on @pat by one.  This prevents @pat from
 * being freed until a matching call to gem_pattern_unref() is made.
 *
 * Returns: The referenced #gem_Pattern.
 */
gem_Pattern *
gem_pattern_ref (gem_Pattern *pat)
{
  g_return_val_if_fail (pat, NULL);

  g_atomic_int_inc (&pat->ref_count);

  return pat;
}

/**
 * gem_pattern_unref:
 * @pat: a #gem_Pattern
 *
 * Decreases the reference count on @pat by one.  If the result is zero,
 * then frees @pat and all associated resources.  See gem_pattern_ref().
 */
void
gem_pattern_unref (gem_Pattern *pat)
{
  g_return_if_fail (pat);

  if (g_atomic_int_dec_and_test (&pat->ref_count))
    {
      pat_data_fini (pat);
      g_slice_free1 ((pat)->cfg.data_size, (pat)->data);
      gem_event_array_unref (pat->match.buf);
      g_slice_free (gem_Pattern, pat);
    }
}

/**
 * gem_pattern_get_config:
 * @pat: a #gem_Pattern
 *
 * Returns: A copy of @pat's #gem_PatternConfig.
 */
gem_PatternConfig
gem_pattern_get_config (const gem_Pattern *pat)
{
  gem_PatternConfig cfg;

  g_return_val_if_fail (pat, default_cfg);

  cfg = pat->cfg;
  if (cfg.func == default_cfg.func)
    cfg.func = NULL;

  return cfg;
}

/**
 * gem_pattern_match:
 * @pat: a #gem_Pattern
 * @evt: an input #gem_Event to be fed to @pat
 * @act: (out) (optional): the action performed on @evt
 *
 * Feeds input event @evt to @pat.
 *
 * This function references @evt, so you can immediately call
 * gem_event_unref() on it if you don't need to maintain a separate
 * reference to it.
 *
 * Returns: (transfer full): A newly allocated output event or %NULL (no
 * output).
 */

/* Collect event to partial match.  */
#define pat_match_collect(pat, evt)                     \
  G_STMT_START                                          \
  {                                                     \
    gem_event_array_add ((pat)->match.buf, (evt));      \
  }                                                     \
  G_STMT_END

/* Collect whole match as a complex event.  */
#define pat_match_complete(pat, evt)                    \
  G_STMT_START                                          \
  {                                                     \
    *(evt) = gem_event_new ((pat)->match.pfx_id,        \
                            (pat)->match.pfx_type,      \
                            (pat)->match.pfx_time,      \
                            (pat)->match.buf);          \
    gem_event_array_unref ((pat)->match.buf);           \
    (pat)->match.buf = gem_event_array_new ();          \
    (pat)->match.pfx_len = 0;                           \
    (pat)->match.pfx_id = GEM_EVENT_ID_NONE;            \
    (pat)->match.pfx_type = GEM_EVENT_TYPE_NONE;        \
    (pat)->match.pfx_time = GEM_EVENT_TIME_NONE;        \
  }                                                     \
  G_STMT_END

/* Clear partial match.  */
#define pat_match_clear(pat)                                    \
  G_STMT_START                                                  \
  {                                                             \
    gint i;    /* <-- Must be an int! */                        \
    for (i = (gint)((pat)->match.buf->len - 1);                 \
         i >= (gint)(pat)->match.pfx_len; i--)                  \
      {                                                         \
        g_ptr_array_remove_index_fast ((pat)->match.buf,        \
                                       (guint) i);              \
      }                                                         \
  }                                                             \
  G_STMT_END

gem_Event *
gem_pattern_match (gem_Pattern *pat,
                   gem_Event *evt,
                   gem_PatternAction *act)
{
  gem_PatternAction ret_act;
  gem_Event *out;

  g_return_val_if_fail (pat, NULL);
  g_return_val_if_fail (evt, NULL);

  out = NULL;
  ret_act = pat->cfg.func (pat, evt,
                           &pat->match.pfx_id,
                           &pat->match.pfx_type,
                           &pat->match.pfx_time,
                           pat->data);
  switch (ret_act)
    {
    case GEM_PATTERN_SKIP:
      break;
    case GEM_PATTERN_COLLECT:
      pat_match_collect (pat, evt);
      break;
    case GEM_PATTERN_COLLECT_AND_COMPLETE:
      pat_match_collect (pat, evt);
      pat_match_complete (pat, &out);
      break;
    case GEM_PATTERN_COLLECT_AND_EXPAND:
      pat_match_collect (pat, evt);
      pat->match.pfx_len = pat->match.buf->len;
      break;
    case GEM_PATTERN_CLEAR:
      pat_match_clear (pat);
      if (pat->match.pfx_len > 0)
        {
          pat_match_complete (pat, &out);
        }
      break;
    case GEM_PATTERN_CLEAR_AND_COLLECT:
      pat_match_clear (pat);
      if (pat->match.pfx_len > 0)
        {
          pat_match_complete (pat, &out);
        }
      pat_match_collect (pat, evt);
      break;
    default:
      g_assert_not_reached ();
    }

  tryset (act, ret_act);
  return out;
}

/**
 * gem_pattern_match_complete:
 * @pat: a #gem_Pattern
 *
 * Completes any partial match in @pat.
 *
 * Returns: (transfer full): A newly allocated output event or %NULL (no
 * output).
 */
gem_Event *
gem_pattern_match_complete (gem_Pattern *pat)
{
  gem_Event *out;

  g_return_val_if_fail (pat, NULL);

  if (pat->match.pfx_len == 0)
    return NULL;

  pat_match_clear (pat);
  pat_match_complete (pat, &out);
  return out;
}

/**
 * gem_pattern_reset:
 * @pat: a #gem_Pattern
 *
 * Resets @pat to its initial state, that is, makes @pat drop any partial
 * match, and reset (finalize and initialize) its data.
 */
void
gem_pattern_reset (gem_Pattern *pat)
{
  g_return_if_fail (pat);

  g_ptr_array_set_size (pat->match.buf, 0);
  pat_reset_match (pat);
  pat_data_fini (pat);
  pat_data_init (pat);
}
