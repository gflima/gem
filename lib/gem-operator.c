/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include <config.h>
#include "aux-glib.h"
#include "gem.h"
#include "gem-private.h"

/**
 * SECTION: gem-operator
 * @Title: Operators
 * @Short_Description: searching for a pattern in parallel on overlapping
 * windows
 */

/* Window data.  */
struct _gem_Window
{
  gem_Pattern *pat;             /* pattern associated with window */
  gem_Event *first;             /* input event that opened the window */
  gem_Event *last;              /* input event that closed the window */
  GQueue output;                /* output events produced by the window */
  GList *link;                  /* link of the window in operators queue */
};

/* Operator data.  */
struct _gem_Operator
{
  gem_PatternConfig cfg;        /* pattern configuration */
  gem_OperatorFlags flags;      /* operator flags */
  gem_OperatorStats stats;      /* operator statistics */
  struct {
    gem_EventTime size;         /* window size */
    gem_EventTime slide;        /* window slide */
    GQueue open;                /* queue with open windows */
    GQueue closed;              /* queue with closed windows */
    GQueue disposed;            /* queue with disposed windows */
    gem_EventTime latest;       /* time of last open window */
  } win;
  struct {
    gem_Event *last_evt;        /* last input event pushed */
    GQueue skipped;             /* ranges of skipped input events */
  } in;
  struct {
    GQueue queue;               /* output events waiting to be popped */
    gem_EventID start_id;       /* id of first output event */
    gem_EventID next_id;        /* id of next output event */
  } out;
  struct {
    gem_Savepoint *save;        /* the savepoint being restored */
    gem_EventID start_id;       /* the id of the first event in replay */
    const GArray *output_ids;   /* ids of events output in replay */
    guint output_ids_index;     /* index in output_ids */
    gboolean in_progress;       /* whether a restore is in progress */
  } restore;
  volatile guint ref_count;     /* reference counter */
};

/* Pair of ids -- used internally to store ranges of skipped events.  */
typedef struct _gem_EventIDPair
{
  gem_EventID first;
  gem_EventID second;
} gem_EventIDPair;

#if defined DEBUG && DEBUG

/* Prints a formatted trace message.  */
# define TRACE(op, fmt, ...)                    \
  G_STMT_START                                  \
  {                                             \
    if ((op)->flags & GEM_OPERATOR_FLAG_TRACE)  \
      g_debug (fmt, ## __VA_ARGS__);            \
  }                                             \
  G_STMT_END                                    \

/* Prints a formatted trace message if @cond is %TRUE.  */
# define TRACE_IF(op, cond, fmt, ...)           \
  G_STMT_START                                  \
  {                                             \
    if ((cond))                                 \
      TRACE ((op), fmt, ## __VA_ARGS__);        \
  }                                             \
  G_STMT_END

/* Prints a trace @msg followed by @evt string dump.  */
# define TRACE_EVENT(op, msg, evt)              \
  G_STMT_START                                  \
  {                                             \
    if ((op)->flags & GEM_OPERATOR_FLAG_TRACE)  \
      {                                         \
        GString *str;                           \
        str = gem_event_to_string ((evt));      \
        g_debug ("%s%s", msg, str->str);        \
        g_string_free (str, TRUE);              \
      }                                         \
  }                                             \
  G_STMT_END

/* Increments the given statistics.  */
# define STATS_INCR(op, stats)                  \
  G_STMT_START                                  \
  {                                             \
    if ((op)->flags                             \
        & GEM_OPERATOR_FLAG_STATISTICS)         \
      {                                         \
        (stats)++;                              \
      }                                         \
  }                                             \
  G_STMT_END

/* Updates the given "maximum" statistics.  */
# define STATS_MAX(op, stats, max)              \
  G_STMT_START                                  \
  {                                             \
    if ((op)->flags                             \
        & GEM_OPERATOR_FLAG_STATISTICS)         \
      {                                         \
        (stats) = MAX ((stats), (max));         \
      }                                         \
  }                                             \
  G_STMT_END

/* STATS_INCR followed by STATS_MAX.  */
# define STATS_INCR_MAX(op, stats1, stats2, max)        \
  G_STMT_START                                          \
  {                                                     \
    if ((op)->flags                                     \
        & GEM_OPERATOR_FLAG_STATISTICS)                 \
      {                                                 \
        (stats1)++;                                     \
        (stats2) = MAX ((stats2), (max));               \
      }                                                 \
  }                                                     \
  G_STMT_END

#else
# define TRACE(op, fmt, ...)                     /* nothing */
# define TRACE_IF(op, cond, fmt, ...)            /* nothing */
# define TRACE_EVENT(op, msg, evt)               /* nothing */
# define STATS_INCR(op, stats)                   /* nothing */
# define STATS_MAX(op, stats, max)               /* nothing */
# define STATS_INCR_MAX(op, stats1, stats2, max) /* nothing */
#endif /* DEBUG */


/* Window tests.  */
#define win_is_open(w)     ((w)->first != NULL && (w)->last == NULL)
#define win_is_closed(w)   ((w)->first != NULL && (w)->last != NULL)

/* Allocates a new gem_Window with configuration @cfg.  */
static gem_Window *
_win_new (const gem_PatternConfig *cfg)
{
  gem_Window *win;

  win = g_slice_new (gem_Window);
  win->pat = gem_pattern_new (cfg);
  g_queue_init (&win->output);
  win->first = NULL;
  win->last = NULL;
  win->link = NULL;

  return win;
}

/* Frees @win and all associated data.  */
static void
_win_free (gem_Window *win)
{
  g_return_if_fail (win);

  if (win->pat != NULL)
    gem_pattern_unref (win->pat);

  if (win->first != NULL)
    gem_event_unref (win->first);

  if (win->last != NULL)
    gem_event_unref (win->last);

  while (win->output.length > 0)
    gem_event_unref (g_queue_pop_head (&win->output));

  g_slice_free (gem_Window, win);
}

/* Collects @out as an output event produced in window @win.  */
static void
win_collect_output (gem_Operator *op, gem_Window *win, gem_Event *out)
{
  g_return_if_fail (op);
  g_return_if_fail (win);
  g_return_if_fail (out);

  /* @win must be open.  */
  g_assert (win_is_open (win));

  /* @evt must be associated with no window.  */
  g_assert_null (gem_event_get_window (out));

  if (!op->restore.in_progress)
    {
      out->id = op->out.next_id++;
    }
  else                          /* restoring */
    {
      if (op->restore.output_ids == NULL)
        {
          /* Restoration from savepoints generated by gem_operator_save()
             will never reach here.  At this point, @op->out.next_next was
             reset to start id by gem_operator_restore().  This only makes
             sense if no output events were generated prior to the
             savepoint.  */
          out->id = op->out.next_id++;
          op->restore.in_progress = FALSE;
        }
      else
        {
          out->id = g_array_index (op->restore.output_ids, gem_EventID,
                                   op->restore.output_ids_index++);
          if (op->restore.output_ids_index == op->restore.output_ids->len)
            {
              op->out.next_id = out->id + 1;
              g_clear_pointer (&op->restore.save, gem_savepoint_unref);
              op->restore.in_progress = FALSE;
            }
        }

      TRACE_IF (op, !op->restore.in_progress, "--- restore end ---");
    }

  gem_event_set_window (out, win);
  g_queue_push_tail (&win->output, gem_event_ref (out));
  g_queue_push_tail (&op->out.queue, out);

  TRACE_EVENT (op, "  > out ", out);
}

/* Opens a new window and sets @first as its first event.  */
static void
win_open (gem_Operator *op, gem_Event *first)
{
  gem_Window *win;

  g_return_if_fail (op);
  g_return_if_fail (first);

  if (op->win.disposed.length == 0)
    {
      win = _win_new (&op->cfg);
      STATS_INCR (op, op->stats.wins_allocated);
      g_queue_push_tail (&op->win.open, win);
      win->link = g_queue_peek_tail_link (&op->win.open);
    }
  else
    {
      GList *ell = g_queue_pop_head_link (&op->win.disposed);
      g_queue_push_tail_link (&op->win.open, ell);
      win = ell->data;
    }

  win->first = gem_event_ref (first);
  op->win.latest = win->first->time;

  TRACE (op, "  > opn [%"G_GUINT64_FORMAT"...", win->first->time);
  STATS_INCR_MAX (op, op->stats.wins_opened,
                  op->stats.wins_max_opened, op->win.open.length);
}

/* Disposes the window in @ell.  Window @ell cannot not be linked and its
   window must be closed.  */
static void
win_dispose (gem_Operator *op, GList *ell)
{
  gem_Window *win;

  g_return_if_fail (op);
  g_return_if_fail (ell);

  /* @link must not be linked.  */
  g_assert_null (ell->prev);
  g_assert_null (ell->next);

  /* @link must contain a closed window.  */
  win = ell->data;
  g_assert (ell == win->link);
  g_assert (win_is_closed (win));

  TRACE (op, "  > del [%"G_GUINT64_FORMAT"-%"G_GUINT64_FORMAT"]",
         win->first->time, win->last->time);

  gem_pattern_reset (win->pat);
  g_clear_pointer (&win->first, gem_event_unref);
  g_clear_pointer (&win->last, gem_event_unref);
  while (win->output.length > 0)
    gem_event_unref (g_queue_pop_head (&win->output));

  g_queue_push_tail_link (&op->win.disposed, ell);

  STATS_INCR_MAX (op, op->stats.wins_disposed,
                  op->stats.wins_max_disposed, op->win.disposed.length);
}

/* Closes @win and sets @last as its last event.  Window @win must be the
   earliest open window in @op.  */
static void
win_close (gem_Operator *op, gem_Window *win, gem_Event *last)
{
  gem_Event *out;
  GList *ell;

  g_return_if_fail (op);
  g_return_if_fail (win);
  g_return_if_fail (last);

  /* @win must be the first open window.  */
  g_assert (win_is_open (win));
  g_assert (win == g_queue_peek_head (&op->win.open));

  out = gem_pattern_match_complete (win->pat);
  if (out != NULL)
    win_collect_output (op, win, out);

  g_assert_null (win->last);
  win->last = gem_event_ref (last);

  TRACE (op, "  > cls [%"G_GUINT64_FORMAT"-%"G_GUINT64_FORMAT"]",
         win->first->time, win->last->time);
  STATS_INCR (op, op->stats.wins_closed);

  ell = g_queue_pop_head_link (&op->win.open);
  if (op->flags & GEM_OPERATOR_FLAG_SAVE)
    {
      g_queue_push_tail_link (&op->win.closed, ell);
      STATS_MAX (op, op->stats.wins_max_closed, op->win.closed.length);
    }
  else
    {
      win_dispose (op, win->link);
    }
}


/**
 * gem_operator_new:
 * @cfg: (optional): a pointer to a #gem_PatternConfig.
 * @start_id: the start id for output events
 * @win_size: the size of windows (in microseconds)
 * @win_slide: the offset between windows (in microseconds)
 *
 * Creates a new #gem_Operator with the default flags
 * %GEM_OPERATOR_DEFAULT_FLAGS.  See gem_operator_new_with_flags() for a
 * complete description of the parameters.
 *
 * Returns: (transfer full): A newly allocated #gem_Operator with a
 *   reference count of 1.  Free with gem_operator_unref().
 */
gem_Operator *
gem_operator_new (const gem_PatternConfig *cfg,
                  gem_EventID start_id,
                  gem_EventTime win_size,
                  gem_EventTime win_slide)
{
  return gem_operator_new_with_flags (cfg, start_id, win_size, win_slide,
                                      GEM_OPERATOR_DEFAULT_FLAGS);
}

/**
 * gem_operator_new_with_flags:
 * @cfg: (optional): a pointer to a #gem_PatternConfig.
 * @start_id: the start id for output events
 * @win_size: the size of windows (in microseconds)
 * @win_slide: the offset between windows (in microseconds)
 * @flags: the #gem_OperatorFlags to initialize operator with
 *
 * Creates a new #gem_Operator.
 *
 * The parameter @cfg is used to create a #gem_Pattern to process each
 * window.  If @cfg is %NULL, then the operator is created with the default
 * configuration and skips any event fed to it.
 *
 * The parameter @start_id determines the id of the first event output by
 * the operator.  The id of any subsequent output event is equal to that of
 * the previous output event incremented by one.
 *
 * The parameters @win_size and @win_slide determine the size and offset of
 * the windows used by the operator.  Both @win_size and @win_slide must be
 * greater than zero.
 *
 * Returns: (transfer full): A newly allocated #gem_Operator with a
 *   reference count of 1.  Free with gem_operator_unref().
 */
gem_Operator *
gem_operator_new_with_flags (const gem_PatternConfig *cfg,
                             gem_EventID start_id,
                             gem_EventTime win_size,
                             gem_EventTime win_slide,
                             gem_OperatorFlags flags)
{
  gem_PatternConfig default_cfg = GEM_PATTERN_CONFIG_INIT;
  gem_Operator *op;
  guint max, prealloc, i;

  g_return_val_if_fail (win_size > 0, NULL);
  g_return_val_if_fail (win_slide > 0, NULL);

  op = g_slice_new (gem_Operator);
  op->cfg = cfg ? *cfg : default_cfg;
  op->flags = flags;
  memset (&op->stats, 0, sizeof (op->stats));

  op->win.size = win_size;
  op->win.slide = win_slide;
  g_queue_init (&op->win.open);
  g_queue_init (&op->win.closed);
  g_queue_init (&op->win.disposed);

  /* Preallocate two times the maximum number of windows that can be open
     simultaneously or 4K windows, whichever is smaller.  */
  max = (guint)(((gdouble) win_size / (gdouble) win_slide) + 1);
  prealloc = MIN (2 * max, 4096);
  for (i = 0; i < prealloc; i++)
    {
      gem_Window *win = _win_new (&op->cfg);
      STATS_INCR (op, op->stats.wins_allocated);

      g_queue_push_head (&op->win.disposed, win);
      win->link = g_queue_peek_head_link (&op->win.disposed);
      STATS_INCR_MAX (op, op->stats.wins_disposed,
                      op->stats.wins_max_disposed, op->win.disposed.length);
    }

  /* Make sure that 1st input event always open a window.  */
  op->win.latest = -win_slide;

  op->in.last_evt = NULL;
  g_queue_init (&op->in.skipped);

  g_queue_init (&op->out.queue);
  op->out.start_id = start_id;
  op->out.next_id = start_id;

  op->restore.save = NULL;
  op->restore.start_id = GEM_EVENT_ID_NONE;
  op->restore.output_ids_index = 0;
  op->restore.in_progress = FALSE;

  op->ref_count = 1;

  return op;
}

/**
 * gem_operator_ref:
 * @op: a #gem_Operator
 *
 * Increases the reference count on @op by one.  This prevents @op from
 * being freed until a matching call to gem_operator_unref() is made.
 *
 * Returns: The referenced #gem_Operator.
 */
gem_Operator *
gem_operator_ref (gem_Operator *op)
{
  g_return_val_if_fail (op, NULL);

  g_atomic_int_inc (&op->ref_count);

  return op;
}

/**
 * gem_operator_unref:
 * @op: a #gem_Operator
 *
 * Decreases the reference count on @op by one.  If the result is zero, then
 * frees @op and all associated resources.  See gem_operator_ref().
 */
void
gem_operator_unref (gem_Operator *op)
{
  g_return_if_fail (op);

  if (g_atomic_int_dec_and_test (&op->ref_count))
    {
      /* Windows.  */
      while (op->win.open.length > 0)
        _win_free (g_queue_pop_head (&op->win.open));

      while (op->win.closed.length > 0)
        _win_free (g_queue_pop_head (&op->win.closed));

      while (op->win.disposed.length > 0)
        _win_free (g_queue_pop_head (&op->win.disposed));

      /* Input.  */
      while (op->in.skipped.length > 0)
        g_slice_free (gem_EventIDPair, g_queue_pop_head (&op->in.skipped));

      if (op->in.last_evt != NULL)
        gem_event_unref (op->in.last_evt);

      /* Output.  */
      while (op->out.queue.length > 0)
        gem_event_unref (g_queue_pop_head (&op->out.queue));

      /* Restore.  */
      if (op->restore.save != NULL)
        gem_savepoint_unref (op->restore.save);

      g_slice_free (gem_Operator, op);
    }
}

/**
 * gem_operator_get_config:
 * @op: a #gem_Operator
 *
 * Returns: A copy of @op's #gem_PatternConfig.
 */
gem_PatternConfig
gem_operator_get_config (const gem_Operator *op)
{
  static gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;

  g_return_val_if_fail (op, cfg);

  return op->cfg;
}

/**
 * gem_operator_get_start_id:
 * @op: a #gem_Operator
 *
 * Returns: @op's start id.
 */
gem_EventID
gem_operator_get_start_id (const gem_Operator *op)
{
  g_return_val_if_fail (op, GEM_EVENT_ID_NONE);

  return op->out.start_id;
}

/**
 * gem_operator_get_window_size:
 * @op: a #gem_Operator
 *
 * Returns: @op's window size.
 */
gem_EventTime
gem_operator_get_window_size (const gem_Operator *op)
{
  g_return_val_if_fail (op, GEM_EVENT_TIME_NONE);

  return op->win.size;
}

/**
 * gem_operator_get_window_slide:
 * @op: a #gem_Operator
 *
 * Returns: @op's window slide.
 */
gem_EventTime
gem_operator_get_window_slide (const gem_Operator *op)
{
  g_return_val_if_fail (op, GEM_EVENT_TIME_NONE);

  return op->win.slide;
}

/**
 * gem_operator_get_flags:
 * @op: a #gem_Operator.
 *
 * Returns: @op's current flags.
 */
gem_OperatorFlags
gem_operator_get_flags (const gem_Operator *op)
{
  g_return_val_if_fail (op, 0);

  return op->flags;
}

/**
 * gem_operator_has_flags:
 * @op: a #gem_Operator
 * @flags: the #gem_OperatorFlags to check
 *
 * Checks if @flags are set in @op.
 *
 * Returns: %TRUE if @flags are set in @op or %FALSE otherwise.
 */
gboolean
gem_operator_has_flags (const gem_Operator *op,
                        gem_OperatorFlags flags)
{
  g_return_val_if_fail (op, FALSE);

  return op->flags & flags ? TRUE : FALSE;
}

/**
 * gem_operator_set_flags:
 * @op: a #gem_Operator
 * @flags: the #gem_OperatorFlags to set
 *
 * Sets @flags in @op.
 *
 * Returns: %TRUE if successful or %FALSE otherwise.
 */
gboolean
gem_operator_set_flags (gem_Operator *op,
                        gem_OperatorFlags flags)
{
  g_return_val_if_fail (op, FALSE);

  op->flags |= flags;
  return TRUE;
}

/**
 * gem_operator_unset_flags:
 * @op: a #gem_Operator
 * @flags: the #gem_OperatorFlags to clear
 *
 * Clears @flags in @op.
 *
 * Returns: %TRUE if successful or %FALSE otherwise.
 */
gboolean
gem_operator_unset_flags (gem_Operator *op,
                          gem_OperatorFlags flags)
{
  g_return_val_if_fail (op, FALSE);

  op->flags &= ~flags;
  return TRUE;
}

/**
 * gem_operator_get_statistics:
 * @op: a #gem_Operator
 *
 * Returns: @op's statistics (#gem_OperatorStats).
 */
gem_OperatorStats
gem_operator_get_statistics (const gem_Operator *op)
{
  static gem_OperatorStats default_stats = GEM_OPERATOR_STATS_INIT;

  g_return_val_if_fail (op, default_stats);

  return op->stats;
}

/**
 * gem_operator_push:
 * @op: a #gem_Operator
 * @evt: a #gem_Event to be fed to @op
 *
 * Pushes the input event @evt to @op.
 *
 * This function references @evt, so you can immediately call
 * gem_event_unref() on it if you don't need to maintain a separate
 * reference to it.
 *
 * Returns: The number of output events produced and that are now waiting to
 * be popped via gem_operator_pop().
 */
guint
gem_operator_push (gem_Operator *op,
                   gem_Event *evt)
{
  gboolean SKIP;
  guint saved_output_len;
  GList *ell;

  g_return_val_if_fail (op, 0);
  g_return_val_if_fail (evt, 0);

  TRACE_EVENT (op, "evt ", evt);
  STATS_INCR (op, op->stats.evts_pushed);

  /* Old event while restoring.  */
  if (unlikely (op->restore.in_progress && evt->id < op->restore.start_id))
    {
      TRACE (op, "... old event, ignoring");
      return 0;
    }

  SKIP = TRUE;
  saved_output_len = op->out.queue.length;

  /* Close old windows (if any).  */
  while (TRUE)
    {
      gem_Window *win;

      win = g_queue_peek_head (&op->win.open);
      if (win == NULL)
        break;                  /* no open window */

      g_assert (win_is_open (win));
      if (evt->time - win->first->time <= op->win.size)
        break;                  /* no old window */

      win_close (op, win, op->in.last_evt);
    }

  /* Open a new window.  The first event will always open a window because
     @op->win.latest is a guint initialized with -@op->win.slide.  */
  if (evt->time - op->win.latest >= op->win.slide)
    {
      win_open (op, evt);
      SKIP = FALSE;
    }

  /* Feed @evt to each open window.  */
  if (op->win.open.length > 0)
    {
      for (ell = op->win.open.head; ell != NULL; ell = ell->next)
        {
          gem_Window *win;
          gem_PatternAction act;
          gem_Event *out;

          win = ell->data;
          out = gem_pattern_match (win->pat, evt, &act);
          if (act != GEM_PATTERN_SKIP)
            SKIP = FALSE;
          if (out != NULL)
            win_collect_output (op, win, out);
        }
    }
  /* @evt fell in a whole between windows.  */
  else
    {
      TRACE (op, "... fell in whole between windows");
    }

  /* Update skipped ranges.  */
  if ((op->flags & GEM_OPERATOR_FLAG_SKIP_RANGES)
      && !op->restore.in_progress
      && SKIP)
    {
      /**** FIXME: Make this faster! *****/
      gem_EventIDPair *last = g_queue_peek_tail (&op->in.skipped);
      if (last == NULL || evt->id > last->second + 1)
        {
          last = g_slice_new (gem_EventIDPair);
          last->first = last->second = evt->id;
          g_queue_push_tail (&op->in.skipped, last);
        }
      else
        {
          g_assert (evt->id == last->second + 1);
          last->second++;
        }

      TRACE (op, "... skipped");
      STATS_INCR (op, op->stats.evts_skipped);
    }

  /* Update last event seen.  */
  if (likely (op->in.last_evt != NULL))
    gem_event_unref (op->in.last_evt);
  op->in.last_evt = gem_event_ref (evt);

  /* Done.  */
  return op->out.queue.length - saved_output_len;
}

/**
 * gem_operator_flush:
 * @op: a #gem_Operator
 *
 * Closes all windows of @op and complete their patterns.
 *
 * Returns: The number of output events produced and that are now waiting to
 * be popped via gem_operator_pop().
 */
guint
gem_operator_flush (gem_Operator *op)
{
  guint saved_output_len;

  g_return_val_if_fail (op, 0);

  saved_output_len = op->out.queue.length;

  while (op->win.open.length > 0)
    win_close (op, g_queue_peek_head (&op->win.open), op->in.last_evt);

  if (op->in.last_evt != NULL)
    g_clear_pointer (&op->in.last_evt, gem_event_unref);

  /* Make sure that the next event push will open a window.  */
  op->win.latest = -op->win.slide;

  return op->out.queue.length - saved_output_len;
}

/**
 * gem_operator_pop:
 * @op: a #gem_Operator
 *
 * Pops an output event from @op.
 *
 * Returns: (transfer full): A newly allocated output event or %NULL (no
 * output).
 */
gem_Event *
gem_operator_pop (gem_Operator *op)
{
  g_return_val_if_fail (op, NULL);

  STATS_INCR (op, op->stats.evts_popped);
  return g_queue_pop_head (&op->out.queue);
}

/**
 * gem_operator_save:
 * @op: a #gem_Operator
 * @evt: (optional): a #gem_Event output by @op
 * @clear: whether windows closed before the savepoint should be disposed
 *
 * Creates a savepoint that can be used to restore the operator to the state
 * it was immediately after @evt was produced.
 *
 * If @evt is %NULL, then @evt is assumed to be the last event produced (if
 * any).
 *
 * If @clear is %TRUE, then all windows closed before the savepoint are
 * disposed.  This means that future calls to this function with the same
 * event @evt or events preceding it will fail.
 *
 * In case it is given, this function references @evt, so you can
 * immediately call gem_event_unref() on it if you don't need to maintain a
 * separate reference to it.
 *
 * Returns: (transfer full): A newly allocated #gem_Savepoint which can be
 * passed to gem_operator_restore(), or %NULL (not enough data).
 */

static gint
compare_int (gconstpointer a,
             gconstpointer b,
             unused (gpointer data))
{
  gint ai = GPOINTER_TO_INT (a);
  gint bi = GPOINTER_TO_INT (b);

  if (ai > bi)
    return 1;
  else if (ai == bi)
    return 0;
  else
    return -1;
}

gem_Savepoint *
gem_operator_save (gem_Operator *op,
                   gem_Event *evt,
                   gboolean clear)
{
  gem_EventID ret_start_id;
  GArray *ret_output_ids;
  GArray *ret_skip_ranges;
  gem_Savepoint *save;

  gem_Window *evt_win;
  gem_Event *evt_match_last;
  GList *ell;
  GList *ell_next;

  GSequence *seq;
  guint seq_size;
  GSequenceIter *it;

  g_return_val_if_fail (op, NULL);

  if (evt != NULL)
    TRACE (op, "--- save start: evt %"G_GUINT64_FORMAT" ---", evt->id);
  else
    TRACE (op, "--- save start: now ---");

  if (unlikely (op->restore.in_progress))
    {
      TRACE (op, "--- save failed: can't save during restore ---");
      return NULL;
    }

  if (op->win.closed.length == 0)
    {
      TRACE (op, "--- save failed: no window to dispose ---");
      return NULL;
    }

  if (evt == NULL)
    evt = g_queue_peek_tail (&op->out.queue);

  if (evt != NULL)
    {
      gem_event_ref (evt);
      evt_win = gem_event_get_window (evt);
      if (unlikely (evt_win == NULL))
        {
          TRACE (op, "--- save failed: evt %"G_GUINT64_FORMAT
                 " has no associated window ---", evt->id);
          goto fail;
        }

      if (unlikely (evt->match == NULL || evt->match->len == 0))
        {
          TRACE (op, "--- save failed: evt %"G_GUINT64_FORMAT
                 " has no no match ---", evt->id);
          goto fail;
        }
      evt_match_last = gem_event_array_index (evt->match,
                                              evt->match->len - 1);
    }
  else                          /* no output event */
    {
      evt_win = g_queue_peek_tail (&op->win.open);
      if (evt_win == NULL)
        evt_win = g_queue_peek_tail (&op->win.closed);
      evt_match_last = op->in.last_evt;
    }

  g_assert (evt_win);
  g_assert_nonnull (evt_match_last);

  /* PART I: Find the last window =W= closed before @evt was produced.
     Proceed backwards from @evt's window or from the last closed
     window.  */

  if (win_is_closed (evt_win))
    {
      ell = evt_win->link;
      g_assert_nonnull (ell);
    }
  else
    {
      ell = g_queue_peek_tail_link (&op->win.closed);
    }

  while (ell != NULL)
    {
      gem_Window *win = ell->data;;
      if (win->last->time <= evt_match_last->time)
        break;                  /* found =W= */
      ell = ell->prev;
    }

  if (ell == NULL)              /* no closed window before @evt */
    {
      TRACE (op, "--- save failed: no window to dispose ---");
      goto fail;
    }

  /* PART 2: Set RET_START_ID and RET_OUTPUT_IDS.  RET_START_ID is the
     sequence number of the first event in the first window =W'= (open or
     closed) after =W=.  RET_OUTPUT_IDS is the sequence numbers of all
     output events produced from =W'= until @evt.  */

  /* Optimization?  */
  while (ell->next != NULL)
    {
      /* There is no point in maintaining =W'= if it hasn't produced any
         output event.  So we advance =W= and =W'= until we find some window
         that has produced an output event.  */
      gem_Window *win = ell->next->data;
      if (win->output.length > 0)
        break;                  /* found =W'= */
      ell = ell->next;
    }

  ell_next = ell->next;
  if (ell->next == NULL)        /* =W'= is an open window */
    {
      ell_next = g_queue_peek_head_link (&op->win.open);
      if (ell_next == NULL)     /* =W'= is =W= itself! */
        ell_next = ell;
    }

  /* At this point, ELL is =W= and ELL_NEXT is =W'=. */
  g_assert_nonnull (ell);
  g_assert_nonnull (ell_next);

  /* RET_START_ID is the first event in =W'=.  */
  ret_start_id = ((gem_Window *) ell_next->data)->first->id;

  /* Collect skip ranges from RET_START_ID onward.  */
  ret_skip_ranges = NULL;
  if (op->flags & GEM_OPERATOR_FLAG_SKIP_RANGES)
    {
      gem_EventIDPair *range;

      while (op->in.skipped.length > 0)
        {
          range = g_queue_peek_head (&op->in.skipped);
          if (ret_start_id < range->second)
            break;
          g_slice_free (gem_EventIDPair,
                        g_queue_pop_head (&op->in.skipped));
        }

      if (op->in.skipped.length > 0)
        {
          GList *l;
          ret_skip_ranges = g_array_sized_new (FALSE, FALSE,
                                               sizeof (gem_EventID),
                                               op->in.skipped.length * 2);

          for (l = op->in.skipped.head; l != NULL; l = l->next)
            {
              range = l->data;
              g_array_append_val (ret_skip_ranges, range->first);
              g_array_append_val (ret_skip_ranges, range->second);
            }
        }
    }

  /* Collect the output events from =W'= onward in a binary tree, until @evt
     is found.  Then we traverse this tree to construct RET_OUTPUT_IDS.  */
  seq = g_sequence_new (NULL);
  seq_size = 0;

  if (!(op->flags & GEM_OPERATOR_FLAG_OUTPUT_IDS))
    goto found;                 /* skip */

  while (ell_next != NULL)
    {
      gem_Window *win;
      GList *l;

      win = ell_next->data;
      for (l = win->output.head; l != NULL; l = l->next)
        {
          gem_EventID id = ((gem_Event *) l->data)->id;
          g_sequence_insert_sorted (seq, GINT_TO_POINTER (id),
                                    compare_int, NULL);
          seq_size++;
          if (evt == l->data)  /* if @evt is NULL we collect every event  */
            {
              goto found;
            }
        }

      /* Continue search on the open windows.  */
      if (ell_next == g_queue_peek_tail_link (&op->win.closed))
        {
          ell_next = g_queue_peek_head_link (&op->win.open);
          continue;
        }

      ell_next = ell_next->next;
    }

 found:
  ret_output_ids = g_array_sized_new (FALSE, FALSE, sizeof (gem_EventID),
                                      seq_size + 1); /* +1 for sentinel */

  for (it = g_sequence_get_begin_iter (seq);
       !g_sequence_iter_is_end (it);
       it = g_sequence_iter_next (it))
    {
      gpointer p = g_sequence_get (it);
      gem_EventID id = (gem_EventID) GPOINTER_TO_INT (p);
      g_array_append_val (ret_output_ids, id);
    }
  g_sequence_free (seq);

  /* If no output events were collected, save as a sentinel the next output
     id.  This ensures that the saves produced by this call always contain a
     non-empty output_ids array, which can be used to determine when the
     restoration process is done.  See win_collect_output().  */
  if (ret_output_ids->len == 0)
    {
      g_array_append_val (ret_output_ids, op->out.next_id);
    }

  /* PART 3: If @clear was given, dispose all windows closed before =W= plus
     =W= itself.  */

  if (clear)
    {
      while (op->win.closed.head != ell)
        {
          win_dispose (op, g_queue_pop_head_link (&op->win.closed));
        }
      g_assert (g_queue_peek_head_link (&op->win.closed) == ell);
      win_dispose (op, g_queue_pop_head_link (&op->win.closed));
    }

  /* Done.  */
  if (evt != NULL)
    gem_event_unref (evt);

  save = gem_savepoint_new (ret_start_id, ret_output_ids, ret_skip_ranges);
  g_array_unref (ret_output_ids);
  if (ret_skip_ranges != NULL)
    g_array_unref (ret_skip_ranges);

#if defined DEBUG && DEBUG
  if (op->flags & GEM_OPERATOR_FLAG_TRACE)
    gem_debug_savepoint (save);
#endif

  TRACE (op, "--- save end ---");

  return save;

 fail:
  if (evt != NULL)
    gem_event_unref (evt);
  return NULL;
}

/**
 * gem_operator_restore:
 * @op: a #gem_Operator
 * @save: (optional): a #gem_Savepoint to be restored
 *
 * Restores @op to the state described by @save.
 *
 * If @save is %NULL, restore @op to its initial state.
 *
 * In case it is given, function references @save, so you can immediately
 * call gem_savepoint_unref() on it if you don't need to maintain a separate
 * reference to it.
 *
 * After this function is called, to finish the restoration process, the
 * original sequence of input events should be replayed to the operator via
 * gem_operator_push().  The replayed sequence should start from the id
 * returned by gem_savepoint_get_start_id().  Any event in the sequence
 * whose id belong to one of the ranges returned by
 * gem_savepoint_get_skip_ranges() can be safely skipped.  The restoration
 * process is done when the last id in the array
 * gem_savepoint_get_output_ids() occurs in some event output by the
 * operator.
 */
void
gem_operator_restore (gem_Operator *op,
                      gem_Savepoint *save)
{
  g_return_if_fail (op);

  TRACE (op, "--- restore start ---");

  gem_operator_flush (op);

  /* Dispose all windows.  */
  g_assert (op->win.open.length == 0);
  while (op->win.closed.length > 0)
    win_dispose (op, g_queue_pop_head_link (&op->win.closed));

  /* Clear output queue.  */
  while (op->out.queue.length > 0)
    gem_event_unref (g_queue_pop_head (&op->out.queue));

  /* Clear skipped queue.  */
  while (op->in.skipped.length > 0)
    g_slice_free (gem_EventIDPair, g_queue_pop_head (&op->in.skipped));

  /* Clear previous save.  */
  if (op->restore.save)
    gem_savepoint_unref (op->restore.save);

  if (save != NULL)
    {
#if defined DEBUG && DEBUG
      if (op->flags & GEM_OPERATOR_FLAG_TRACE)
        gem_debug_savepoint (save);
#endif
      op->restore.save = gem_savepoint_ref (save);
      op->restore.start_id = gem_savepoint_get_start_id (save);
      op->restore.output_ids = gem_savepoint_get_output_ids (save);
      op->restore.output_ids_index = 0;
    }
  else
    {
      op->restore.start_id = 0;
      op->restore.output_ids = NULL;
      op->restore.output_ids_index = 0;

     }

  op->restore.in_progress = TRUE;

  /* Reset @op->out.next_id to the start id.  This value will be used only
     if the @save->output_ids is NULL, which cannot happen for savepoints
     generated by gem_operator_save().  See the comment
     win_collect_output().  */
  op->out.next_id = op->out.start_id;
}

/**
 * gem_operator_is_restoring:
 * @op: a #gem_Operator
 *
 * Returns: %TRUE if @op is restoring or %FALSE otherwise.
 */
gboolean
gem_operator_is_restoring (const gem_Operator *op)
{
  g_return_val_if_fail (op, FALSE);

  return op->restore.in_progress;
}


/**** Private ****/

/**
 * gem_window_get_first:
 * @win: a #gem_Window
 *
 * Returns: (transfer none): @win's first event or %NULL (no first event).
 */
const gem_Event *
gem_window_get_first (const gem_Window *win)
{
  g_return_val_if_fail (win, NULL);

  return win->first;
}

/**
 * gem_window_get_last:
 * @win: a #gem_Window
 *
 * Returns: (transfer none): @win's last event or %NULL (no last event).
 */
const gem_Event *
gem_window_get_last (const gem_Window *win)
{
  g_return_val_if_fail (win, NULL);

  return win->first;
}

/**
 * gem_window_get_output:
 * @win: a #gem_Window
 *
 * Returns: (transfer none): @win's output events.
 */
const GQueue *
gem_window_get_output (const gem_Window *win)
{
  g_return_val_if_fail (win, NULL);

  return &win->output;
}

/**
 * gem_debug_window:
 * @win: a #gem_Window
 *
 * Logs @win using g_debug().
 */

/* Helper function to log an event queue.  */
static void
debug_event_queue (const gchar *label, const GQueue *queue)
{
  g_return_if_fail (label);
  g_return_if_fail (queue);

  if (queue->length > 0)
    {
      GList *ell;
      guint i;

      g_debug ("  %s (%u):", label, queue->length);
      for (i = 0, ell = queue->head; ell != NULL; ell = ell->next)
        {
          GString *str;
          str = gem_event_to_string (ell->data);
          g_debug ("  #%u: Event (%p): %s", i++, ell->data, str->str);
          g_string_free (str, TRUE);
        }
    }
  else
    {
      g_debug ("  %s: (none)", label);
    }
}

void
gem_debug_window (const gem_Window *win)
{
  g_return_if_fail (win);

  g_debug ("Window (%p):", win);

  if (win->first != NULL)
    {
      GString *str = gem_event_to_string (win->first);
      g_debug ("  first: Event (%p): %s", win->first, str->str);
      g_string_free (str, TRUE);
    }
  else
    {
      g_debug ("  first: (none)");
    }

  if (win->last != NULL)
    {
      GString *str = gem_event_to_string (win->last);
      g_debug ("  last: Event (%p): %s", win->last, str->str);
      g_string_free (str, TRUE);
    }
  else
    {
      g_debug ("  last: (none)");
    }

  debug_event_queue ("output", &win->output);
}

/**
 * gem_operator_get_open_windows:
 * @op: a #gem_Operator
 *
 * Returns: (transfer none): @op's open windows.
 */
const GQueue *
gem_operator_get_open_windows (const gem_Operator *op)
{
  g_return_val_if_fail (op, NULL);

  return &op->win.open;
}

/**
 * gem_operator_get_closed_windows:
 * @op: a #gem_Operator
 *
 * Returns: (transfer none): @op's closed windows.
 */
const GQueue *
gem_operator_get_closed_windows (const gem_Operator *op)
{
  g_return_val_if_fail (op, NULL);

  return &op->win.closed;
}

/**
 * gem_operator_get_output:
 * @op: a #gem_Operator
 *
 * Returns: (transfer none): @op's output events which are waiting to be
 * popped.
 */
const GQueue *
gem_operator_get_output (const gem_Operator *op)
{
  g_return_val_if_fail (op, NULL);

  return &op->out.queue;
}

/**
 * gem_debug_operator:
 * @op: a #gem_Operator
 *
 * Logs @op using g_debug().
 */

/* Helper function to log a window queue.  */
static void
debug_window_queue (const gchar *label, const GQueue *queue)
{
  g_return_if_fail (label);
  g_return_if_fail (queue);

  if (queue->length > 0)
    {
      GList *ell;

      g_debug ("  %s (%u):", label, queue->length);
      for (ell = queue->head; ell != NULL; ell = ell->next)
        {
          gem_Window *win;
          GString *str;

          win = ell->data;
          str = g_string_new ("    ");
          if (win_is_open (win))
            {
              g_string_append_printf (str, "[%"G_GUINT64_FORMAT"-...",
                                      win->first->time);
            }
          else
            {
              g_string_append_printf (str, "[%"G_GUINT64_FORMAT
                                      "-%"G_GUINT64_FORMAT"]",
                                      win->first->time,
                                      win->last->time);
            }
          if (win->output.length > 0)
            {
              GList *l;

              l = win->output.head;
              g_string_append_printf (str, "\t-> %"G_GUINT64_FORMAT,
                                      ((gem_Event *) l->data)->id);

              for (l = l->next; l != NULL; l = l->next)
                {
                  g_string_append_printf (str, ", %"G_GUINT64_FORMAT,
                                          ((gem_Event *) l->data)->id);
                }
            }

          g_debug ("%s", str->str);
          g_string_free (str, TRUE);
        }
    }
  else
    {
      g_debug ("  %s: (none)", label);
    }
}

void
gem_debug_operator (const gem_Operator *op)
{
  g_return_if_fail (op);

  g_debug ("Operator (%p):", op);

  debug_window_queue ("open", &op->win.open);
  debug_window_queue ("closed", &op->win.closed);

  if (op->in.last_evt != NULL)
    {
      GString *str = gem_event_to_string (op->in.last_evt);
      g_debug ("  last event: Event (%p): %s", op->in.last_evt, str->str);
      g_string_free (str, TRUE);
    }
  else
    {
      g_debug ("  last event: (none)");
    }

  if (op->in.skipped.length > 0)
    {
      GString *str;
      GList *ell;
      gem_EventIDPair *pair;

      str = g_string_new ("  skipped: ");
      ell = op->in.skipped.head;
      pair = ell->data;
      g_string_append_printf (str, "%"G_GUINT64_FORMAT"-%"G_GUINT64_FORMAT,
                              pair->first, pair->second);

      for (ell = ell->next; ell != NULL; ell = ell->next)
        {
          pair = ell->data;
          g_string_append_printf (str,
                                  ", %"G_GUINT64_FORMAT"-%"G_GUINT64_FORMAT,
                                  pair->first, pair->second);
        }

      g_debug ("%s", str->str);
      g_string_free (str, TRUE);
    }
  else
    {
      g_debug ("  skipped: (none)");
    }

  debug_event_queue ("output", &op->out.queue);
}
