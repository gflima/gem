# cfg.mk -- Setup maintainer's makefile.
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

COPYRIGHT_YEAR= 2018
COPYRIGHT_HOLDER= Guilherme F. Lima

BOOTSTRAP_EXTRA=\
  --enable-coverage\
  --enable-gtk-doc\
  --disable-shared\
  --enable-valgrind\
  $(NULL)

INDENT_EXCLUDE=\
  lib/gem.h\
  $(NULL)

INDENT_JOIN_EMPTY_LINES_EXCLUDE=\
  $(NULL)

INDENT_TYPES=\
  $(NULL)

SC_USELESS_IF_BEFORE_FREE_ALIASES=\
  g_free\
  $(NULL)

SYNTAX_CHECK_EXCLUDE=\
  $(NULL)

SC_COPYRIGHT_EXCLUDE=\
  build-aux/Makefile.am.common\
  build-aux/Makefile.am.coverage\
  build-aux/Makefile.am.env\
  build-aux/Makefile.am.gitlog\
  build-aux/Makefile.am.valgrind\
  lib/aux-glib.h\
  maint.mk\
  $(NULL)

UPDATE_COPYRIGHT_EXCLUDE=\
  $(NULL)

SC_RULES+= sc-copyright
sc-copyright:
	$(V_at)$(build_aux)/syntax-check-copyright\
	  -b='/*' -e='*/' -t=cfg.mk\
	  $(call vc_list_exclude, $(VC_LIST_C), $(SC_COPYRIGHT_EXCLUDE))
	$(V_at)$(build_aux)/syntax-check-copyright\
	  -b='#' -t=cfg.mk\
	  $(call vc_list_exclude,\
	    $(VC_LIST_AC)\
	    $(VC_LIST_AM)\
	    $(VC_LIST_MK)\
	    $(VC_LIST_PL)\
	    $(VC_LIST_SH),\
	    $(SC_COPYRIGHT_EXCLUDE))

# Copy utility stuff from gflima/autoutils project.
util:= https://gitlab.com/gflima/autoutils/raw/master
UTIL_FILES+= build-aux/Makefile.am.common
UTIL_FILES+= build-aux/Makefile.am.coverage
UTIL_FILES+= build-aux/Makefile.am.env
UTIL_FILES+= build-aux/Makefile.am.gitlog
UTIL_FILES+= build-aux/Makefile.am.valgrind
UTIL_FILES+= build-aux/util.m4
UTIL_FILES+= lib/aux-glib.h
UTIL_FILES+= maint.mk
UTIL_SCRIPTS+= build-aux/syntax-check
UTIL_SCRIPTS+= build-aux/syntax-check-copyright
REMOTE_FILES+= $(UTIL_FILES)
REMOTE_SCRIPTS+= $(UTIL_SCRIPTS)

fetch-remote-local:
	$(V_at)for path in $(UTIL_FILES) $(UTIL_SCRIPTS); do\
	  dir=`dirname "$$path"`;\
	  $(FETCH) -dir="$$dir" "$(util)/$$path" || exit 1;\
	done
