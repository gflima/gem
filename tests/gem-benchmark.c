/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"
#include "gem-benchmark.h"

/* Benchmark run data. */
struct _gem_Benchmark
{
  const gem_BenchmarkOptions *opts; /* options (read-only) */
  gem_test_Rand *rand;          /* random data generator */
  gem_Operator *op;             /* operator */
  GQueue *input;                /* input queue */
  GQueue *output;               /* output queue */
  FILE *outsink;                /* output sink */
  gem_BenchmarkResults results; /* results */
  struct {
    guint evts_left;            /* input events left */
    GSequence *out_ids;         /* ids of events output */
    gem_EventTime accum_time;   /* accumulated time of input events */
    gdouble accum_runtime;      /* accumulated runtime of input events */
    GTimer *runtime;            /* total runtime */
    GTimer *iter_runtime;       /* iteration runtime */
    GTimer *restore_runtime;    /* restore runtime */
    GList *curr_failure_point;  /* pointer in failure points */
    gboolean restoring;         /* true if we're restoring  */
    GList *curr_input;          /* pointer in input queue */
    GSequence *acks;            /* pending acks */
    gem_Savepoint *save;        /* last save */
    GTimer *save_runtime;       /* save runtime */
  } st;
  volatile guint ref_count;     /* reference counter */
};

/* Compares two integers.  */
static gint
compare_int (gconstpointer a,
             gconstpointer b,
             unused (gpointer data))
{
  gint ai = GPOINTER_TO_INT (a);
  gint bi = GPOINTER_TO_INT (b);

  if (ai > bi)
    return 1;
  else if (ai == bi)
    return 0;
  else
    return -1;
}

/* Compares two events by time.  */
static gint
compare_event_time (gconstpointer a,
                    gconstpointer b,
                    unused (gpointer data))
{
  const gem_Event *ea = a;
  const gem_Event *eb = b;

  if (ea->time > eb->time)
    return 1;
  else if (ea->time == eb->time)
    return 0;
  else
    return -1;
}

/* Resets @run to its initial state.
   This function doesn't touch on the counter.  */
static void
run_reset (gem_Benchmark *run)
{
  guint n;

  g_return_if_fail (run);

  /* Flush operator.  */
  n = gem_operator_flush (run->op);
  while (n-- > 0)
    gem_event_unref (gem_operator_pop (run->op));

  /* Clear queues.  */
  while (run->input->length > 0)
    gem_event_unref (g_queue_pop_head (run->input));
  while (run->output->length > 0)
    gem_event_unref (g_queue_pop_head (run->output));

  /* Reset results.  */
  run->results.evts_in = 0;
  run->results.evts_out = 0;
  run->results.duplicates = 0;
  run->results.inq_max_length = 0;
  run->results.outq_max_length = 0;
  run->results.accum_time = 0;
  run->results.runtime = 0;
  run->results.accum_runtime = 0;
  run->results.restores = 0;
  run->results.replayed = 0;
  run->results.replayed_dups = 0;
  run->results.restore_runtime = 0;
  run->results.acks = 0;
  run->results.saves = 0;
  run->results.saves_failed = 0;
  run->results.saves_out_ids = 0;
  run->results.saves_skip_pairs = 0;
  run->results.saves_skip_pairs_range = 0;
  run->results.saves_runtime = 0;

  /* Reset state.  */
  run->st.evts_left = run->opts->sample.input_size;
  if (run->st.out_ids != NULL)
    g_sequence_free (run->st.out_ids);
  run->st.accum_time = 0;
  if (run->st.runtime != NULL)
    g_timer_destroy (run->st.runtime);
  if (run->st.iter_runtime != NULL)
    g_timer_destroy (run->st.iter_runtime);
  if (run->st.restore_runtime != NULL)
    g_timer_destroy (run->st.restore_runtime);
  run->st.curr_failure_point = run->opts->failure_points->head;
  run->st.restoring = FALSE;
  run->st.curr_input = NULL;
  if (run->st.acks != NULL)
    g_sequence_free (run->st.acks);
  if (run->st.save != NULL)
    gem_savepoint_unref (run->st.save);
  if (run->st.save_runtime != NULL)
    g_timer_destroy (run->st.save_runtime);
}

/* Pauses the timers of @run.  */
static void
run_pause_time (gem_Benchmark *run)
{
  g_return_if_fail (run);
  g_return_if_fail (run->st.runtime);
  g_return_if_fail (run->st.iter_runtime);

  g_timer_stop (run->st.runtime);
  g_timer_stop (run->st.iter_runtime);
  if (run->st.restoring)
    g_timer_stop (run->st.restore_runtime);
}

/* Resumes the timers of @run.  */
static void
run_resume_time (gem_Benchmark *run)
{
  g_return_if_fail (run);
  g_return_if_fail (run->st.runtime);
  g_return_if_fail (run->st.iter_runtime);

  g_timer_continue (run->st.runtime);
  g_timer_continue (run->st.iter_runtime);
  if (run->st.restoring)
    g_timer_continue (run->st.restore_runtime);
}

/* Gets the next input id that should cause a failure.  */
static gem_EventID
run_get_next_failure (gem_Benchmark *run)
{
  gem_EventID id;

  g_return_val_if_fail (run, GEM_EVENT_ID_NONE);

  if (run->st.curr_failure_point == NULL)
    return GEM_EVENT_ID_NONE;

  id = (gem_EventID) GPOINTER_TO_INT (run->st.curr_failure_point->data);
  run->st.curr_failure_point = run->st.curr_failure_point->next;

  return id;
}

/* Gets the next input event for @run.  */
static gem_Event *
run_get_next_input (gem_Benchmark *run)
{
  gem_Event *evt;

  g_return_val_if_fail (run, NULL);

  run_pause_time (run);

  if (!run->st.restoring)
    {
      evt = gem_test_rand_event (run->rand);
      run->st.accum_runtime += (gdouble) evt->time / G_USEC_PER_SEC;
      run->st.accum_time += evt->time;
      evt->time = run->st.accum_time;

      g_queue_push_tail (run->input, evt);
      run->results.evts_in++;
      run->results.inq_max_length = MAX (run->results.inq_max_length,
                                         run->input->length);
      g_assert (run->st.evts_left > 0);
      run->st.evts_left--;
    }
  else
    {
      g_assert_nonnull (run->st.curr_input);
      evt = run->st.curr_input->data;
      run->st.curr_input = run->st.curr_input->next;
      run->results.replayed++;
    }

  run_resume_time (run);

  return evt;
}

/* Collects output event @out.  */
static void
run_collect_output (gem_Benchmark *run,
                    gem_Event *out)
{
  g_return_if_fail (run);
  g_return_if_fail (out);

  run_pause_time (run);

  if (!run->st.restoring)
    {
      g_queue_push_tail (run->output, out);
      run->results.evts_out++;
      run->results.outq_max_length = MAX (run->results.outq_max_length,
                                          run->output->length);

      /* Check if it is a duplicate.  */
      if (run->st.out_ids != NULL && out->match)
        {
          guint hash = gem_event_array_hash (out->match);
          if (g_sequence_lookup (run->st.out_ids, GINT_TO_POINTER (hash),
                                 compare_int, NULL) != NULL)
            {
              run->results.duplicates++;
            }
          else
            {
              g_sequence_insert_sorted (run->st.out_ids,
                                        GINT_TO_POINTER (hash),
                                        compare_int, NULL);
            }
        }

      /* Schedule ack for event.  */
      if (run->opts->ack_countdown > 0)
        {
          gem_Event *last;
          gem_EventTime salt;

          g_assert (out->match->len > 0);
          last = gem_event_array_index (out->match, out->match->len - 1);
          salt = gem_test_rand_event_time_full (run->rand,
                                                run->opts->ack_min,
                                                run->opts->ack_max);
          out->time = last->time + salt;
          g_sequence_insert_sorted (run->st.acks, out,
                                    compare_event_time, NULL);
        }

      /* Print event to sink file.  */
      if (run->outsink != NULL)
        {
          GString *str;
          guint hash;

          str = gem_event_to_string (out);
          hash = gem_event_array_hash (out->match);
          g_string_append_printf (str, "\t%u\n", hash);
          fputs (str->str, run->outsink);
          g_string_free (str, TRUE);
        }
    }
  else
    {
      gem_event_unref (out);    /* duplicate */
      run->results.replayed_dups++;
    }

  run_resume_time (run);
}

/* Prints progress to stdout.  */
static void
run_print_progress (gem_Benchmark *run, gint64 dt)
{
  guint left;
  guint size;

  run_pause_time (run);

  left = run->st.evts_left;
  size = run->opts->sample.input_size;

  if (size < 20)
    goto done;                  /* nothing to do */

  if (left % (size / 20) == 0)
    {
      dt = g_get_monotonic_time () - dt;
      g_printerr ("%"GEM_TIME_FORMAT"\t%g%%\t%u\n",
                  GEM_TIME_ARGS ((guint64) dt),
                  100. * (size - left) / (gdouble) size, size - left);
    }

 done:
  run_resume_time (run);
}

/* Checks for pending ack.  */
static gem_Event *
run_check_pending_ack (gem_Benchmark *run,
                       gem_Event *evt)
{
  GSequenceIter *it;
  gem_Event *out;
  gem_Event *next;

  g_return_val_if_fail (run, NULL);
  g_return_val_if_fail (evt, NULL);

  if (run->st.restoring)
    return NULL;                /* nothing to do  */

  run_pause_time (run);

  /* Find latest output event to ack.  */
  out = NULL;
  while (TRUE)
    {
      it = g_sequence_get_begin_iter (run->st.acks);
      if (g_sequence_iter_is_end (it))
        break;

      next = g_sequence_get (it);
      if (next->time > evt->time)
        break;

      out = next;
      g_sequence_remove (it);
    }

  /* Trim output queue.  */
  if (out != NULL)
    {
      while (run->output->length > 0)
        {
          gem_Event *head = g_queue_peek_head (run->output);
          if (head == out)
            break;
          gem_event_unref (g_queue_pop_head (run->output));
        }
      g_assert (run->output->length > 0);
    }

  run_resume_time (run);

  return out;
}

/* Trims input queue based on savepoint.  */
static void
run_trim_input (gem_Benchmark *run,
                gem_Savepoint *save)
{
  gem_EventID start_id;
  GArray *skip;

  g_return_if_fail (run);
  g_return_if_fail (save);

  g_assert_false (run->st.restoring);
  run_pause_time (run);

  /* Delete everything before start id.  */
  start_id = gem_savepoint_get_start_id (save);
  while (run->input->length > 0)
    {
      gem_Event *evt = g_queue_peek_head (run->input);
      if (evt->id >= start_id)
        break;
      gem_event_unref (g_queue_pop_head (run->input));
    }

  /* Delete skip ranges.  */
  skip = gem_savepoint_get_skip_ranges (save);
  if (skip != NULL && skip->len > 0)
    {
      guint i = 0;
      GList *ell = run->input->head;
      while (ell != NULL && i < skip->len)
        {
          gem_Event *evt;
          gem_EventID lo;
          gem_EventID hi;

          evt = ell->data;
          lo = g_array_index (skip, gem_EventID, i);
          hi = g_array_index (skip, gem_EventID, i + 1);

          if (evt->id >= lo && evt->id <= hi)
            {
              GList *next = ell->next;
              gem_event_unref (ell->data);
              g_queue_delete_link (run->input, ell);
              ell = next;
            }
          else if (evt->id > hi)
            {
              i += 2;
            }
          else
            {
              ell = ell->next;
            }
        }
    }

  run_resume_time (run);
}

/* Makes savepoint and trims input queue accordingly.  */
static void
run_save (gem_Benchmark *run,
          gem_Event *out)
{
  gem_Savepoint *save;

  g_return_if_fail (run);
  g_return_if_fail (out);

  if (run->st.restoring)
    return;                     /* nothing to do  */

  g_timer_continue (run->st.save_runtime);
  save = gem_operator_save (run->op, out, TRUE);
  run->results.saves++;
  g_timer_stop (run->st.save_runtime);

  if (save != NULL)
    {
      GArray *array;

      if (run->st.save != NULL)
        gem_savepoint_unref (run->st.save);
      run->st.save = save;

      run_trim_input (run, save);

      run_pause_time (run);

      array = gem_savepoint_get_output_ids (save);
      g_assert_nonnull (array);
      run->results.saves_out_ids += array->len;
      array = gem_savepoint_get_skip_ranges (save);
      if (array != NULL)
        {
          guint i;
          guint sum;

          run->results.saves_skip_pairs += array->len / 2;
          sum = 0;
          for (i = 0; i < array->len; i += 2)
            {
              gem_EventID lo;
              gem_EventID hi;

              lo = g_array_index (array, gem_EventID, i);
              hi = g_array_index (array, gem_EventID, i + 1);
              sum += (guint)((hi - lo) + 1);
            }
          run->results.saves_skip_pairs_range += sum;
        }

      run_resume_time (run);
    }
  else
    {
      run->results.saves_failed++;
    }
}


/**
 * gem_benchmark_new:
 * @opts: a #gem_BenchmarkOptions
 *
 * Creates a new #gem_Benchmark initialized with @opts.
 * Returns: (transfer full): A newly allocated #gem_Benchmark.
 * Free with gem_benchmark_unref().
 */
gem_Benchmark *
gem_benchmark_new (const gem_BenchmarkOptions *opts)
{
  gem_Benchmark *run;
  gem_PatternConfig cfg;
  gem_OperatorFlags flags;

  g_return_val_if_fail (opts, NULL);

  run = g_slice_new0 (gem_Benchmark);
  run->opts = opts;
  run->rand = gem_benchmark_get_rand_from_options (opts);
  cfg = gem_test_pattern (opts->test_pat);

  flags = 0;
  if (opts->op_save)
    flags |= GEM_OPERATOR_FLAG_SAVE;
  if (opts->op_output_ids)
    flags |= GEM_OPERATOR_FLAG_OUTPUT_IDS;
  if (opts->op_skip)
    flags |= GEM_OPERATOR_FLAG_SKIP_RANGES;
  if (opts->op_stats)
    flags |= GEM_OPERATOR_FLAG_STATISTICS;
  if (opts->op_trace)
    flags |= GEM_OPERATOR_FLAG_TRACE;
  run->op = gem_operator_new_with_flags (&cfg, 0,
                                         opts->sample.win_size,
                                         opts->sample.win_slide,
                                         flags);
  run->input = g_queue_new ();
  run->output = g_queue_new ();
  run->outsink = NULL;
  if (opts->output_file != NULL)
    {
      const gchar *name = opts->output_file;
      run->outsink = g_str_equal (name, "-") ? stdout : fopen (name, "w");
      if (unlikely (run->outsink == NULL))
        {
          g_warning ("Couldn't create file '%s'", name);
        }
    }
  run->results.opts = *opts;
  run->ref_count = 1;

  return run;
}

/**
 * gem_benchmark_ref:
 * @run: a #gem_Benchmark
 *
 * Increases the reference count on @run by one.  This prevents @run from
 * being freed until a matching call to gem_benchmark_unref() is made.
 *
 * Returns: The referenced #gem_Benchmark.
 */
gem_Benchmark *
gem_benchmark_ref (gem_Benchmark *run)
{
  g_return_val_if_fail (run, NULL);

  g_atomic_int_inc (&run->ref_count);
  return run;
}

/**
 * gem_benchmark_unref:
 * @run: a #gem_Benchmark
 *
 * Decreases the reference count on @run by one.  If the result is zero,
 * then frees @run and all associated resources.  See
 * gem_benchmark_ref().
 */
void
gem_benchmark_unref (gem_Benchmark *run)
{
  g_return_if_fail (run);

  run_reset (run);
  gem_test_rand_unref (run->rand);
  gem_operator_unref (run->op);
  g_queue_free_full (run->input, (GDestroyNotify) gem_event_unref);
  g_queue_free_full (run->output, (GDestroyNotify) gem_event_unref);
  if (run->outsink != NULL && run->outsink != stdout)
    fclose (run->outsink);
}

/**
 * gem_benchmark_run:
 * @run: a #gem_Benchmark
 *
 * Runs @run once.
 */
void
gem_benchmark_run (gem_Benchmark *run)
{
  gint64 dt = g_get_monotonic_time ();
  gem_EventID next_failure;
  guint n;

  g_return_if_fail (run);

  run_reset (run);
  next_failure = run_get_next_failure (run);

  if (run->opts->duplicates)
    run->st.out_ids = g_sequence_new (NULL);

  run->st.acks = g_sequence_new (NULL);
  run->st.save = NULL;

  run->st.restore_runtime = g_timer_new ();
  g_timer_stop (run->st.restore_runtime);
  run->st.save_runtime = g_timer_new ();
  g_timer_stop (run->st.save_runtime);

  run->st.runtime = g_timer_new ();
  run->st.iter_runtime = g_timer_new ();
  while (run->st.evts_left > 0 || run->st.restoring)
    {
      gem_Event *evt;

      /* Get next input event.  */
      g_timer_reset (run->st.iter_runtime);
      evt = run_get_next_input (run);
      g_assert_nonnull (evt);

      /* Process input event and collect output.  */
      n = gem_operator_push (run->op, evt);
      while (n-- > 0)
        {
          gem_Event *out = gem_operator_pop (run->op);
          g_assert_nonnull (out);
          run_collect_output (run, out);
        }

      /* Update accumulated runtime.  */
      run->st.accum_runtime += g_timer_elapsed (run->st.iter_runtime, NULL);

      /* Print progress */
      if (!run->st.restoring && run->opts->show_progress)
        {
          run_print_progress (run, dt);
        }

      /* Check for pending acks.  */
      if (run->opts->ack_countdown > 0)
        {
          gem_Event *out = run_check_pending_ack (run, evt);
          if (out != NULL)
            {
              run->results.acks++;
              if (run->results.acks % run->opts->ack_countdown == 0)
                run_save (run, out);
            }
        }

      /* Check if we should fail and start a restore.  */
      if (evt->id == next_failure)
        {
          g_timer_continue (run->st.restore_runtime);
          next_failure = run_get_next_failure (run);
          gem_debug_savepoint (run->st.save);
          gem_operator_restore (run->op, run->st.save);
          run->st.curr_input = run->input->head;
          run->st.restoring = TRUE;
          run->results.restores++;
        }
      else if (evt->id > next_failure)
        {
          next_failure = run_get_next_failure (run);
        }

      /* Check if we should end the current restore.  */
      if (run->st.restoring && run->st.curr_input == NULL)
        {
          run->st.restoring = FALSE;
          g_timer_stop (run->st.restore_runtime);
        }
    }

  /* Flush operator and collect any events output.  */
  n = gem_operator_flush (run->op);
  while (n-- > 0)
    {
      gem_Event *out = gem_operator_pop (run->op);
      g_assert_nonnull (out);
      run_collect_output (run, out);
    }

  g_timer_stop (run->st.runtime);
}

/**
 * gem_benchmark_get_results:
 * @run: a #gem_Benchmark
 *
 * Returns: the #gem_BenchmarkResults of @run.
 */
const gem_BenchmarkResults *
gem_benchmark_get_results (gem_Benchmark *run)
{
  g_return_val_if_fail (run, NULL);

  run->results.stats = gem_operator_get_statistics (run->op);
  if (run->st.runtime != NULL)
    {
      run->results.runtime = (gem_EventTime)
        (g_timer_elapsed (run->st.runtime, NULL) * G_USEC_PER_SEC);
    }
  else
    {
      run->results.runtime = 0;
    }
  run->results.accum_time = run->st.accum_time;
  run->results.accum_runtime
    = (gem_EventTime)(run->st.accum_runtime * G_USEC_PER_SEC);

  /* Restores.  */
  if (run->st.restore_runtime != NULL)
    {
      run->results.restore_runtime = (gem_EventTime)
        (g_timer_elapsed (run->st.restore_runtime, NULL) * G_USEC_PER_SEC);
    }
  else
    {
      run->results.restore_runtime = 0;
    }

  /* Saves.  */
  if (run->st.save_runtime != NULL)
    {
      run->results.saves_runtime = (gem_EventTime)
        (g_timer_elapsed (run->st.save_runtime, NULL) * G_USEC_PER_SEC);
    }
  else
    {
      run->results.saves_runtime = 0;
    }

  return &run->results;
}
