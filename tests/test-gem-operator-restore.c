/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  GEM_TEST ("Unref operator while restoring");
  {
    gem_Operator *op;
    gem_Savepoint *save;
    gem_EventArray *input;
    guint i, j;

    op = gem_operator_new (NULL, 0, 1, 1);
    gem_operator_set_flags (op, GEM_OPERATOR_FLAG_TRACE);
    g_assert_false (gem_operator_is_restoring (op));

    input = gem_event_array_new ();
    for (i = 0; i < 100; i++)
      {
        gem_Event *evt = gem_event_new (i, 0, i, NULL);
        gem_event_array_add (input, evt);
        gem_operator_push (op, evt);
        gem_event_unref (evt);
      }

    save = gem_operator_save (op, NULL, TRUE);
    g_assert_nonnull (save);
    g_assert_false (gem_operator_is_restoring (op));

    for (j = 0; j < 50; j++)
      {
        gem_operator_restore (op, save);
        g_assert (gem_operator_is_restoring (op));

        for (i = 0; i < 50; i++)
          {
            gem_Event *evt = gem_event_array_index (input, i);
            gem_operator_push (op, evt);
          }
        g_assert (gem_operator_is_restoring (op));
      }

    gem_savepoint_unref (save);
    gem_event_array_unref (input);
    gem_operator_unref (op);
  }

  GEM_TEST ("One save/restore; random data");
  {
    gem_test_Rand *rand;
    gem_test_RandSample sample;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;
    gem_Savepoint *save;

    gem_EventArray *input;
    gem_EventArray *output_1;
    gem_EventArray *output_2;

    GArray *out_ids;
    GArray *skipped;

    gem_EventTime dt;
    guint i, j, k;

    gboolean skip_on;

    rand = gem_test_rand_new ();
  resample:
    sample = gem_test_rand_sample (rand, 0);

    if (sample.input_size % 2)
      cfg = gem_test_pattern (GEM_TEST_PATTERN_0);
    else if (sample.input_size % 3)
      cfg = gem_test_pattern (GEM_TEST_PATTERN_NONE);
    else
      cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY);

    op = gem_operator_new (&cfg, 0, sample.win_size, sample.win_slide);
    gem_operator_set_flags (op, GEM_OPERATOR_FLAG_SKIP_RANGES);

    dt = 0;
    input = gem_event_array_new ();
    for (i = 0; i < sample.input_size; i++)
      {
        gem_Event *evt = gem_test_rand_event (rand);
        evt->id = i;
        dt += (evt->time % sample.win_slide);
        evt->time = dt;
        if (evt->type % 5)
          evt->type = 0;
        gem_event_array_add (input, evt);
        gem_event_unref (evt);
      }

    for (i = 0; i < sample.input_size / 2; i++)
      {
        gem_Event *evt = gem_event_array_index (input, i);
        if (evt->time == 3560826)
          G_DEBUG_HERE ();
        gem_operator_push (op, evt);
      }

    save = gem_operator_save (op, NULL, TRUE);
    if (save == NULL)
      {
        gem_event_array_unref (input);
        gem_operator_unref (op);
        g_print ("resampling :(\n");
        goto resample;
      }

    for (i = sample.input_size / 2; i < sample.input_size; i++)
      gem_operator_push (op, gem_event_array_index (input, i));

    output_1 = gem_event_array_new ();
    while (TRUE)
      {
        gem_Event *out = gem_operator_pop (op);
        if (out == NULL)
          break;
        gem_event_array_add (output_1, out);
        gem_event_unref (out);
      }

    skipped = NULL;
    skip_on = FALSE;

  restore_1:
    gem_operator_restore (op, save);
    g_assert (gem_operator_is_restoring (op));
    g_assert_null (gem_operator_save (op, NULL, TRUE)); /* can't save now */
    if (skip_on)
      {
        skipped = gem_savepoint_get_skip_ranges (save);
      }

    for (i = 0, j = 0; i < sample.input_size; i++)
      {
        gem_Event *evt = gem_event_array_index (input, i);
        if (skip_on && skipped && j < skipped->len)
          {
            gem_EventID lo;
            gem_EventID hi;

            lo = g_array_index (skipped, gem_EventID, j);
            hi = g_array_index (skipped, gem_EventID, j + 1);

            if (evt->id >= lo && evt->id <= hi)
              {
                continue;       /* skip */
              }
            else if (evt->id > hi)
              {
                j += 2;         /* next range */
              }
          }
        gem_operator_push (op, evt);
      }

    output_2 = gem_event_array_new ();
    while (TRUE)
      {
        gem_Event *out = gem_operator_pop (op);
        if (out == NULL)
          break;
        gem_event_array_add (output_2, out);
        gem_event_unref (out);
      }

    /* g_debug ("----    ----    ----    ----"); */
    /* gem_debug_event_array (output_1); */
    /* gem_debug_event_array (output_2); */

    out_ids = gem_savepoint_get_output_ids (save);
    g_assert_nonnull (out_ids);

    for (i = 0, j = 0, k = 0; i < output_1->len; i++)
      {
        gem_Event *e1;
        gem_Event *e2;

      retry:
        if (k < out_ids->len)
          {
            if (i < g_array_index (out_ids, gem_EventID, k))
              {
                continue;
              }
            else if (i > g_array_index (out_ids, gem_EventID, k))
              {
                k++;
                goto retry;
              }
          }

        e1 = gem_event_array_index (output_1, i);
        e2 = gem_event_array_index (output_2, j++);

        /* gem_debug_event (e1); */
        /* gem_debug_event (e2); */

        g_assert (gem_event_equal (e1, e2));
      }

    if (!skip_on)
      {
        skip_on = TRUE;
        gem_event_array_unref (output_2);
        goto restore_1;
      }

    gem_savepoint_unref (save);
    gem_operator_unref (op);
    gem_event_array_unref (output_1);
    gem_event_array_unref (output_2);
    gem_event_array_unref (input);
    gem_test_rand_unref (rand);
  }

  GEM_TEST ("Restore from the beginning");
  {
    gem_test_Rand *rand;
    gem_test_RandSample sample;

    gem_PatternConfig cfg = GEM_PATTERN_CONFIG_INIT;
    gem_Operator *op;

    gem_EventArray *input;
    gem_EventArray *output_1;
    gem_EventArray *output_2;

    gem_EventTime dt;
    guint i;

    guint restore;

    rand = gem_test_rand_new ();
    sample = gem_test_rand_sample (rand, 0);

    input = gem_test_rand_event_array (rand, sample.input_size);
    if (sample.input_size % 2)
      cfg = gem_test_pattern (GEM_TEST_PATTERN_0);
    else if (sample.input_size % 3)
      cfg = gem_test_pattern (GEM_TEST_PATTERN_NONE);
    else
      cfg = gem_test_pattern (GEM_TEST_PATTERN_ANY);

    op = gem_operator_new (&cfg, 0, sample.win_size, sample.win_slide);
    output_1 = gem_event_array_new ();
    output_2 = gem_event_array_new ();

    restore = 0;
    dt = 0;
  restore_2:

    for (i = 0; i < input->len; i++)
      {
        gem_Event *evt;
        gem_Event *out;

        evt = gem_event_array_index (input, i);
        if (restore == 0)
          {
            dt += evt->time;
            evt->time = dt;
            if (evt->type % 5)
              evt->type = 0;
          }

        gem_operator_push (op, evt);

        out = gem_operator_pop (op);
        if (out != NULL)
          {
            gem_event_array_add (restore ? output_2 : output_1, out);
            gem_event_unref (out);
          }
      }

    /* gem_debug_operator (op); */
    /* gem_debug_event_array (restore ? output_2 : output_1); */

    if (restore++ == 0)
      {
        gem_operator_restore (op, NULL);
        goto restore_2;
      }

    if (restore < 10)
      {
        g_assert (output_1->len == output_2->len);
        for (i = 0; i < output_1->len; i++)
          {
            gem_Event *e1 = gem_event_array_index (output_1, i);
            gem_Event *e2 = gem_event_array_index (output_2, i);
            g_assert (gem_event_equal (e1, e2));
          }

        gem_event_array_unref (output_2);
        output_2 = gem_event_array_new ();
        gem_operator_restore (op, NULL);
        goto restore_2;
      }

    gem_event_array_unref (input);
    gem_event_array_unref (output_1);
    gem_event_array_unref (output_2);
    gem_operator_unref (op);
    gem_test_rand_unref (rand);
  }

  exit (EXIT_SUCCESS);
}
