/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

gint
main (void)
{
  /* Primitive events.  */
  {
    gem_Event *e1;
    gem_Event *e2;

    guint h1;
    guint h2;

    e1 = gem_event_new (0, 1, 2, NULL);
    e2 = gem_event_new (0, 1, 2, NULL);

    h1 = gem_event_hash (e1);
    h1 = gem_event_hash (e1);
    g_assert (h1 == h1);

    h1 = gem_event_hash (e1);
    h2 = gem_event_hash (e2);
    g_assert (h1 == h2);

    gem_event_unref (e1);
    gem_event_unref (e2);
  }

  /* Complex events.  */
  {
    gem_test_Rand *rand;
    gem_Event *e1;
    gem_Event *e2;
    gem_Event *e3;
    gem_EventArray *m1;
    gem_EventArray *m2;
    guint i, h;

    rand = gem_test_rand_new ();
    m1 = gem_test_rand_event_array (rand, 5);
    m2 = gem_event_array_new ();
    for (i = 0; i < m1->len; i++)
      gem_event_array_add (m2, gem_event_array_index (m1, i));

    h = gem_event_array_hash (m1);
    g_assert (h == gem_event_array_hash (m1));
    g_assert (h == gem_event_array_hash (m2));

    e1 = gem_event_new (0, 1, 2, m1);
    e2 = gem_event_new (0, 1, 2, m2);
    e3 = gem_event_new (0, 2, 1, m1);

    gem_event_array_unref (m1);
    gem_event_array_unref (m2);

    g_print ("\
hash(m1) == %u\n\
hash(m2) == %u\n\
hash(e1) == %u\n\
hash(e2) == %u\n\
hash(e3) == %u\n\
",
             gem_event_array_hash (m1),
             gem_event_array_hash (m2),
             gem_event_hash (e1),
             gem_event_hash (e2),
             gem_event_hash (e3));

    h = gem_event_hash (e1);
    g_assert (h == gem_event_hash (e1));
    g_assert (h == gem_event_hash (e2));
    g_assert (h != gem_event_hash (e3));

    gem_event_unref (e1);
    gem_event_unref (e2);
    gem_event_unref (e3);
    gem_test_rand_unref (rand);
  }

  exit (EXIT_SUCCESS);
}
