#!/bin/bash
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

. init.sh
RESULTS="win-slide win-max-open win-overlap-area evts-out duplicates"
header=$(make_header $RESULTS)
results=$(make_results $RESULTS)

pats="$PATS_SKIP"
runs=1
size=100k

mkdir -p "$PREFIX"
for win in 1000; do
  for type in $TYPES; do
    for pat in $PATS_SKIP; do
      out="$PREFIX/$win.$type.$pat"
      if test -f "$out"; then
        continue
      fi
      echo "$header" >"$out"
      for ((i=1; i<=$win; i++)); do
        set -x
        $RUN --type "$type"\
             --pattern "$pat"\
             --time 1\
             --window "$win/$i"\
             --input-size "$size"\
             --duplicates\
             --runs "$runs"\
             $results\
             >>"$out"
        set +x
      done
    done
  done
done
