/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include <config.h>
#include "aux-glib.h"
#include "gem.h"
#include "gem-private.h"

/**
 * SECTION: gem-savepoint
 * @Title: Savepoints
 * @Short_Description: saving and restoring the state of operators
 */

/* Savepoint data.  */
struct _gem_Savepoint
{
  gem_EventID start_id;         /* id of first input event in replay  */
  GArray *output_ids;           /* ids of all events output during replay */
  GArray *skip_ranges;          /* id ranges of skipped input events */
  volatile guint ref_count;     /* reference counter */
};

/**
 * gem_savepoint_new:
 * @start_id: the id of the first input event in replay
 * @output_ids: (optional): a #GArray with the ids of all events output
 * during replay
 * @skip_ranges: (optional): a #GArray where each consecutive pair of ids
 * denotes a input event range
 *
 * Creates a new #gem_Savepoint.
 *
 * The parameter @start_id determines the id of the first input event that
 * should be replayed to the operator after the savepoint is restored.
 *
 * The parameter @output_ids contains the ids of the output events to be
 * (re-)generated during a replay after the savepoint is restored.  Each
 * element in @output_ids is a #gem_EventID of some event output by the
 * operator.
 *
 * The parameter @skip_ranges contains the ranges of ids of input events
 * that can be skipped during a replay after the savepoint is restored.
 * Each element in @skip_ranges is a #gem_EventID of some event previously
 * fed to the operator. The ranges are encoded in @skip_ranges as
 * consecutive pairs of elements.  This means that the array must have an
 * even number of elements.
 *
 * This function references @output_ids and @skip_ranges when these are
 * given, so you can immediately call g_array_unref() on them if you don't
 * need to maintain a separate reference to them.
 *
 * Returns: (transfer full): A newly allocated #gem_Savepoint with a
 *   reference count of 1.  Free with gem_savepoint_unref().
 */
gem_Savepoint *
gem_savepoint_new (gem_EventID start_id,
                   GArray *output_ids,
                   GArray *skip_ranges)
{
  gem_Savepoint *save;

  save = g_slice_new (gem_Savepoint);
  save->start_id = start_id;
  save->output_ids = output_ids ? g_array_ref (output_ids) : NULL;
  save->skip_ranges = skip_ranges ? g_array_ref (skip_ranges) : NULL;
  save->ref_count = 1;

  return save;
}

/**
 * gem_savepoint_ref:
 * @save: a #gem_Savepoint
 *
 * Increases the reference count on @save by one.  This prevents @save from
 * being freed until a matching call to gem_savepoint_unref() is made.
 *
 * Returns: The referenced #gem_Savepoint.
 */
gem_Savepoint *
gem_savepoint_ref (gem_Savepoint *save)
{
  g_return_val_if_fail (save, NULL);

  g_atomic_int_inc (&save->ref_count);

  return save;
}

/**
 * gem_savepoint_unref:
 * @save: a #gem_Savepoint
 *
 * Decreases the reference count on @save by one.  If the result is zero,
 * then frees @save and all associated resources.  See gem_savepoint_ref().
 */
void
gem_savepoint_unref (gem_Savepoint *save)
{
  g_return_if_fail (save);

  if (g_atomic_int_dec_and_test (&save->ref_count))
    {
      if (save->output_ids != NULL)
        g_array_unref (save->output_ids);

      if (save->skip_ranges != NULL)
        g_array_unref (save->skip_ranges);

      g_slice_free (gem_Savepoint, save);
    }
}

/**
 * gem_savepoint_get_start_id:
 * @save: a #gem_Savepoint
 *
 * Returns: @save's start id.
 */
gem_EventID
gem_savepoint_get_start_id (const gem_Savepoint *save)
{
  g_return_val_if_fail (save, GEM_EVENT_ID_NONE);

  return save->start_id;
}

/**
 * gem_savepoint_get_output_ids:
 * @save: a #gem_Savepoint
 *
 * Returns: (transfer none): @save's output ids.
 */
GArray *
gem_savepoint_get_output_ids (const gem_Savepoint *save)
{
  g_return_val_if_fail (save, NULL);

  return save->output_ids;
}

/**
 * gem_savepoint_get_skip_ranges:
 * @save: a #gem_Savepoint
 *
 * Returns: (transfer none): @save's skipped ranges.
 */
GArray *
gem_savepoint_get_skip_ranges (const gem_Savepoint *save)
{
  g_return_val_if_fail (save, NULL);

  return save->skip_ranges;
}


/**** Private ****/

/**
 * gem_debug_savepoint:
 * @save: a #gem_Savepoint
 *
 * Logs @save using g_debug().
 */
void
gem_debug_savepoint (const gem_Savepoint *save)
{
  g_return_if_fail (save);

  g_debug ("Savepoint (%p):", save);
  g_debug ("  start id: %"G_GUINT64_FORMAT, save->start_id);

  if (save->output_ids != NULL && save->output_ids->len > 0)
    {
      GString *str;
      guint i;

      str = g_string_new ("  output ids: ");
      g_string_append_printf (str, "%"G_GUINT64_FORMAT,
                              g_array_index (save->output_ids,
                                             gem_EventID, 0));
      for (i = 1; i < save->output_ids->len; i++)
        {
          g_string_append_printf (str, ", %"G_GUINT64_FORMAT,
                                  g_array_index (save->output_ids,
                                                 gem_EventID, i));
        }

      g_debug ("%s", str->str);
      g_string_free (str, TRUE);
    }
  else
    {
      g_debug ("  output ids: (none)");
    }

  if (save->skip_ranges != NULL && save->skip_ranges->len > 0)
    {
      GString *str;
      gem_EventID lo;
      gem_EventID hi;
      guint sum;
      guint i;

      g_assert (save->skip_ranges->len % 2 == 0);

      str = g_string_new (NULL);

      lo = g_array_index (save->skip_ranges, gem_EventID, 0);
      hi = g_array_index (save->skip_ranges, gem_EventID, 1);
      g_string_append_printf (str, "%"G_GUINT64_FORMAT"-%"G_GUINT64_FORMAT,
                              lo, hi);

      sum = (guint)(hi - lo + 1);
      for (i = 2; i < save->skip_ranges->len; i += 2)
        {
          lo = g_array_index (save->skip_ranges, gem_EventID, i);
          hi = g_array_index (save->skip_ranges, gem_EventID, i + 1);

          g_string_append_printf (str,
                                  ", %"G_GUINT64_FORMAT"-%"G_GUINT64_FORMAT,
                                  lo, hi);

          sum += (guint)(hi - lo + 1);
        }

      g_debug ("  skipped (%u): %s", sum, str->str);
      g_string_free (str, TRUE);

    }
  else
    {
      g_debug ("  skipped: (none)");
    }
}
