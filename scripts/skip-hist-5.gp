#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load 'skip.gp'
set output NAME.'-hist-5.pdf'

#set yrange[126:127]
#set format y '%.0s%c'

set xlabel 'Save interval'
set ylabel 'Skipped events per save'

USING = 'using ($25/$16):@XTICLABELS with hist'
plot\
  newhistogram lt 1 at 0,\
    PREFIX.'50.8.skip.avg' @USING notitle,\
    PREFIX.'800.8.skip.avg' @USING notitle,\
  newhistogram lt 1 at 1,\
    PREFIX.'50.16.skip.avg' @USING notitle,\
    PREFIX.'800.16.skip.avg' @USING notitle,\
  newhistogram lt 1 at 2,\
    PREFIX.'50.32.skip.avg' @USING notitle,\
    PREFIX.'800.32.skip.avg' @USING notitle,\
  newhistogram lt 1 at 3,\
    PREFIX.'50.64.skip.avg' @USING notitle,\
    PREFIX.'800.64.skip.avg' @USING notitle,\
  newhistogram lt 1 at 4,\
    PREFIX.'50.128.skip.avg' @USING notitle,\
    PREFIX.'800.128.skip.avg' @USING notitle,\

  #   PREFIX.'50.8.skip.avg' @USING notitle,\
  # newhistogram lt 2 at 1,\
  #   PREFIX.'50.16.avg' @USING notitle,\
  #   PREFIX.'50.16.skip.avg' @USING notitle,\
  # newhistogram lt 2 at 2,\
  #   PREFIX.'50.32.avg' @USING notitle,\
  #   PREFIX.'50.32.skip.avg' @USING notitle,\
  # newhistogram lt 2 at 3,\
  #   PREFIX.'50.64.avg' @USING notitle,\
  #   PREFIX.'50.64.skip.avg' @USING notitle,\
  # newhistogram lt 2 at 4,\
  #   PREFIX.'50.128.avg' @USING notitle,\
  #   PREFIX.'50.128.skip.avg' @USING notitle
