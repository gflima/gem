dnl suppfile.m4 -- Generate Valgrind suppressions.
changequote([,])dnl
define([suppress],[dnl
{
    $1: $2
    Memcheck:Leak
    ...
    fun:$1
}])dnl

dnl GObject.
dnl suppress(g_object_do_class_init,            GObject type system)
dnl suppress(g_type_add_interface_static,       GObject type system)
dnl suppress(g_type_register_static,            GObject type system)
dnl suppress(gobject_init_ctor,                 GObject initialization)
dnl suppress(type_iface_vtable_base_init_Wm,    GObject initialization)
