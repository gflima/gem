#!/usr/bin/env lua
-- Copyright (C) 2018 Guilherme F. Lima
-- This file is part of Gem.
--
-- Gem is free software: you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Gem is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
-- License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Gem.  If not, see <https://www.gnu.org/licenses/>.

if #arg ~= 1 then
   print (('usage: %s FILE'):format (arg[0]))
   os.exit (1)
end

local fp, errmsg = io.open (arg[1])
if not fp then
   error (errmsg)
end

local t = {}
local n = 0
local dups = 0
for line in fp:lines () do
   local id = tonumber (line:match ('^.-\t(.*)$'))
   if t[id] then
      dups = dups + 1
   else
      t[id] = true
   end
   n = n + 1
end
fp:close ()

print (('%d events, %d duplicates (%.1f%%)')
      :format (n, dups, dups / n * 100.))
