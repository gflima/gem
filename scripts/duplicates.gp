#!/usr/bin/env gnuplot
# Copyright (C) 2018 Guilherme F. Lima
#
# This file is part of Gem.
#
# Gem is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gem is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Gem.  If not, see <https://www.gnu.org/licenses/>.

load 'init.gp'

NAME = 'duplicates'
set output NAME.'.pdf'

set style data lines
set ytics font ',8'
set xtics font ',8'
set format y '%g'

PATS = '0 01 012 0123 01234'

TITLE = 'Duplicates vs window layout'
SUBTITLE = sprintf ('{/*.8 %s}', '(100k input events)')

KEY_INIT = "title 'Pattern' spacing .85 samplen 2.5"
USING_PERC = 'using (1000-$1):($5/$4*100)'
USING_DUPS = 'using (1000-$1):($5)'
USING_OVER = 'using (1000-$1):($3-1000) lw 3 lc "red" dashtype 2'


set key at 0.3,100 left maxrows 3
set label 1 at screen .5, screen .05 'Window size - Window slide' center

set multiplot layout 2,2 rowsfirst\
  margins .1, .95, .15, .95 spacing .065,.065

set xtics format ''
set xlabel '10 types' right offset 16.5,2.5
set ylabel '% Duplicates' offset 0,-5.
#plot for [pat in PATS] NAME.'.out/1000.0-9.'.pat @USING_PERC title pat
plot NAME.'.out/1000.0-9.'.'0' @USING_PERC title 'a',\
     NAME.'.out/1000.0-9.'.'01' @USING_PERC title 'ab',\
     NAME.'.out/1000.0-9.'.'012' @USING_PERC title 'abc',\
     NAME.'.out/1000.0-9.'.'0123' @USING_PERC title 'abcd',\
     NAME.'.out/1000.0-9.'.'01234' @USING_PERC title 'abcde'

unset key
set xlabel '0-19 types'
set ytics format ''
unset ylabel
#plot for [pat in PATS] NAME.'.out/1000.0-19.'.pat @USING_PERC title pat
plot NAME.'.out/1000.0-19.'.'0' @USING_PERC title 'a',\
     NAME.'.out/1000.0-19.'.'01' @USING_PERC title 'ab',\
     NAME.'.out/1000.0-19.'.'012' @USING_PERC title 'abc',\
     NAME.'.out/1000.0-19.'.'0123' @USING_PERC title 'abcd',\
     NAME.'.out/1000.0-19.'.'01234' @USING_PERC title 'abcde'

set xtics format '%g'
set xlabel '50 types' offset 16.5,3.5
set ytics format '%g'
#plot for [pat in PATS] NAME.'.out/1000.0-49.'.pat @USING_PERC title pat
plot NAME.'.out/1000.0-49.'.'0' @USING_PERC title 'a',\
     NAME.'.out/1000.0-49.'.'01' @USING_PERC title 'ab',\
     NAME.'.out/1000.0-49.'.'012' @USING_PERC title 'abc',\
     NAME.'.out/1000.0-49.'.'0123' @USING_PERC title 'abcd',\
     NAME.'.out/1000.0-49.'.'01234' @USING_PERC title 'abcde'

set xlabel '100 types'
set ytics format ''
#plot for [pat in PATS] NAME.'.out/1000.0-99.'.pat @USING_PERC title pat
plot NAME.'.out/1000.0-99.'.'0' @USING_PERC title 'a',\
     NAME.'.out/1000.0-99.'.'01' @USING_PERC title 'ab',\
     NAME.'.out/1000.0-99.'.'012' @USING_PERC title 'abc',\
     NAME.'.out/1000.0-99.'.'0123' @USING_PERC title 'abcd',\
     NAME.'.out/1000.0-99.'.'01234' @USING_PERC title 'abcde'

unset multiplot


set logscale y
set key at .2,10000000 maxrows 3

set multiplot layout 2,2 rowsfirst\
  margins .1, .95, .15, .95 spacing .065,.065

set xtics format ''
set xlabel '10 types' right offset 16.5,2.5
set ytics format '%.0s%c'
set ylabel 'Duplicates' offset 0,-5.
# plot for [pat in PATS]\
#   NAME.'.out/1000.0-9.'.pat @USING_DUPS title pat,\
#                          '' @USING_OVER title 'overlap'
plot NAME.'.out/1000.0-9.'.'0' @USING_DUPS title 'a',\
     NAME.'.out/1000.0-9.'.'01' @USING_DUPS title 'ab',\
     NAME.'.out/1000.0-9.'.'012' @USING_DUPS title 'abc',\
     NAME.'.out/1000.0-9.'.'0123' @USING_DUPS title 'abcd',\
     NAME.'.out/1000.0-9.'.'01234' @USING_DUPS title 'abcde',\
                         '' @USING_OVER title 'overlap'

unset key
set xlabel '20 types'
set ytics format ''
unset ylabel
# plot for [pat in PATS]\
#   NAME.'.out/1000.0-19.'.pat @USING_DUPS,\
#                           '' @USING_OVER
plot NAME.'.out/1000.0-19.'.'0' @USING_DUPS title 'a',\
     NAME.'.out/1000.0-19.'.'01' @USING_DUPS title 'ab',\
     NAME.'.out/1000.0-19.'.'012' @USING_DUPS title 'abc',\
     NAME.'.out/1000.0-19.'.'0123' @USING_DUPS title 'abcd',\
     NAME.'.out/1000.0-19.'.'01234' @USING_DUPS title 'abcde',\
                         '' @USING_OVER title 'overlap'

set xtics format '%g'
set xlabel '50 types' offset 16.5,3.5
set ytics format '%.0s%c'
# plot for [pat in PATS]\
#   NAME.'.out/1000.0-49.'.pat @USING_DUPS,\
#                           '' @USING_OVER
plot NAME.'.out/1000.0-49.'.'0' @USING_DUPS title 'a',\
     NAME.'.out/1000.0-49.'.'01' @USING_DUPS title 'ab',\
     NAME.'.out/1000.0-49.'.'012' @USING_DUPS title 'abc',\
     NAME.'.out/1000.0-49.'.'0123' @USING_DUPS title 'abcd',\
     NAME.'.out/1000.0-49.'.'01234' @USING_DUPS title 'abcde',\
                         '' @USING_OVER title 'overlap'

set xlabel '100 types'
set ytics format ''
# plot for [pat in PATS]\
#   NAME.'.out/1000.0-99.'.pat @USING_DUPS,\
#                           '' @USING_OVER
plot NAME.'.out/1000.0-99.'.'0' @USING_DUPS title 'a',\
     NAME.'.out/1000.0-99.'.'01' @USING_DUPS title 'ab',\
     NAME.'.out/1000.0-99.'.'012' @USING_DUPS title 'abc',\
     NAME.'.out/1000.0-99.'.'0123' @USING_DUPS title 'abcd',\
     NAME.'.out/1000.0-99.'.'01234' @USING_DUPS title 'abcde',\
                         '' @USING_OVER title 'overlap'
unset multiplot
