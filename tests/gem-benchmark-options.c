/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"
#include "gem-benchmark.h"

/* The global options.  */
static gem_BenchmarkOptions OPTIONS;

/* Global options used only internally.  */
static gchar **opt_failure_points = NULL;
static gboolean opt_version = FALSE;
static gboolean opt_dump_options = FALSE;


/* Compares two integers.  */
static gint
compare_int (gconstpointer a,
             gconstpointer b,
             unused (gpointer data))
{
  gint ai = GPOINTER_TO_INT (a);
  gint bi = GPOINTER_TO_INT (b);

  if (ai > bi)
    return 1;
  else if (ai == bi)
    return 0;
  else
    return -1;
}

/* Parses number value with unit suffix (k, m, g).  */
static gdouble
parse_number (const gchar *arg,
              gchar **end)
{
  gdouble d;
  gchar *c;

  g_return_val_if_fail (arg, 0.);

  d = g_ascii_strtod (arg, &c);
  if (*c == '\0')
    goto done;

  if (g_ascii_strncasecmp (c, "k", 1) == 0) /* kilo */
    {
      d *= 1000.;
      c++;
    }
  else if (g_ascii_strncasecmp (c, "m", 1) == 0) /* mega */
    {
      d *= 1000000.;
      c++;
    }
  else if (g_ascii_strncasecmp (c, "g", 1) == 0) /* giga */
    {
      d *= 1000000000.;
      c++;
    }

 done:
  tryset (end, c);
  return d;
}

/* Parses number range.  */
static gboolean
parse_number_range (const gchar *arg,
                    gdouble *min,
                    gdouble *max)
{
  gdouble lo;
  gdouble hi;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

  lo = parse_number (arg, &end);
  if (*end == '\0')
    {
      hi = lo;
      goto done;
    }

  if (unlikely (*end != '-'))
    return FALSE;

  hi = parse_number (++end, &end);
  if (unlikely (*end != '\0'))
    return FALSE;

done:
  *min = lo;
  *max = MAX (lo, hi);

  return TRUE;
}

/* Parses time value with unit suffix (h, m, s, ms, us).  */
static gem_EventTime
parse_time (const gchar *arg,
            gchar **end)
{
  gdouble d;
  gchar *c;

  g_return_val_if_fail (arg, GEM_EVENT_TIME_NONE);

  d = g_ascii_strtod (arg, &c);
  if (*c == '\0')
    goto done;

  if (g_ascii_strncasecmp (c, "us", 2) == 0) /* microseconds */
    {
      c += 2;
    }
  else if (g_ascii_strncasecmp (c, "ms", 2) == 0) /* milliseconds */
    {
      d *= 1000;
      c += 2;
    }
  else if (g_ascii_strncasecmp (c, "s", 1) == 0) /* seconds */
    {
      d *= G_USEC_PER_SEC;
      c++;
    }
  else if (g_ascii_strncasecmp (c, "m", 1) == 0) /* minutes */
    {
      d *= G_USEC_PER_SEC * 60;
      c++;
    }
  else if (g_ascii_strncasecmp (c, "h", 1) == 0) /* hours */
    {
      d *= G_USEC_PER_SEC * (gdouble) 3600;
      c++;
    }

 done:
  tryset (end, c);
  return (gem_EventTime) MAX (d, 0.);
}

/* Parses time range.  */
static gboolean
parse_time_range (const gchar *arg,
                  gem_EventTime *min,
                  gem_EventTime *max)
{
  gem_EventTime lo;
  gem_EventTime hi;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

  lo = parse_time (arg, &end);
  if (*end == '\0')
    {
      hi = lo;
      goto done;
    }

  if (unlikely (*end != '-'))
    return FALSE;

  hi = parse_time (++end, &end);
  if (unlikely (*end != '\0'))
    return FALSE;

done:
  *min = lo;
  *max = MAX (lo, hi);

  return TRUE;
}

/* Parses failure points string array.  */
static void
parse_failure_points (gchar **str_failure_points,
                      GQueue *failure_points,
                      guint input_size)
{
  guint i;

  g_return_if_fail (str_failure_points);
  g_return_if_fail (failure_points);

  for (i = 0; str_failure_points[i] != NULL; i++)
    {
      gdouble d;
      gchar *end;
      gint id;

      d = parse_number (str_failure_points[i], &end);
      if (*end == '%')
        {
          d = CLAMP (d, 0., 100.);
          id = (gint) CLAMP (input_size * (d / 100.) - 1, 0, input_size - 1);
        }
      else
        {
          id = (gint) CLAMP (d, 0., G_MAXINT);
        }
      g_queue_insert_sorted (failure_points, GINT_TO_POINTER (id),
                             (GCompareDataFunc) compare_int, NULL);
    }
}


/* Parses ack countdown.  */
static gboolean
opt_parse_ack_countdown (unused (const gchar *opt),
                         const gchar *arg,
                         unused (gpointer data),
                         unused (GError *err))
{
  gdouble d;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

  d = round (parse_number (arg, &end));
  if (unlikely (*end != '\0'))
    return FALSE;

  OPTIONS.ack_countdown = (guint) MIN (d, G_MAXUINT);
  return TRUE;
}

/* Parses ack diff.  */
static gboolean
opt_parse_ack_diff (unused (const gchar *opt),
                    const gchar *arg,
                    unused (gpointer data),
                    unused (GError *err))
{
  g_return_val_if_fail (arg, FALSE);

  return parse_time_range (arg, &OPTIONS.ack_min, &OPTIONS.ack_max);
}

/* Parses input size.  */
static gboolean
opt_parse_input_size (unused (const gchar *opt),
                      const gchar *arg,
                      unused (gpointer data),
                      unused (GError *err))
{
  gdouble d;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

  d = round (parse_number (arg, &end));
  if (unlikely (*end != '\0'))
    return FALSE;

  OPTIONS.sample.input_size = (guint) MIN (d, G_MAXUINT);
  return TRUE;
}

/* Parses test pattern name.  */
static gboolean
opt_parse_pattern (unused (const gchar *opt),
                   const gchar *arg,
                   unused (gpointer data),
                   unused (GError *err))
{
  g_return_val_if_fail (arg, FALSE);

  return gem_test_pattern_by_name (arg, &OPTIONS.test_pat);
}

/* Parses time diff.  */
static gboolean
opt_parse_time_diff (unused (const gchar *opt),
                     const gchar *arg,
                     unused (gpointer data),
                     unused (GError *err))
{
  g_return_val_if_fail (arg, FALSE);

  return parse_time_range (arg, &OPTIONS.time_min, &OPTIONS.time_max);
}

/* Parses window specification.  */
static gboolean
opt_parse_window (unused (const gchar *opt),
                  const gchar *arg,
                  unused (gpointer data),
                  unused (GError *err))
{
  gem_EventTime w;
  gem_EventTime s;
  gdouble d;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

# define win_slide_from_density(w, d)\
  ((1 - ((gdouble)(d))) * ((gdouble)(w)) + 1)

# define win_density_from_slide(w, s)\
  (1 - (((gdouble)(s)) - 1) / ((gdouble)(w)))

# define win_d(w, s)\
  ((guint)((w)/(s)))

# define win_max_open(w, s)\
  (win_d ((w), (s)) + 1)

# define win_overlap_area(w, s)                                 \
  (((guint)(w)) * (2 * win_d ((w), (s)) + 1)                    \
   - ((guint)(s)) * (win_d ((w), (s)) * win_d ((w), (s))        \
                     + win_d ((w), (s))))

  w = parse_time (arg, &end);

  if (unlikely (w == 0))
    return FALSE;

  if (unlikely (*end == '\0'))
    return FALSE;

  if (*end == '/')
    {
      s = end[1] == '\0' ? 1 : parse_time (&end[1], &end);
      d = win_density_from_slide (w, s);
    }
  else if (*end == '%')
    {
      d = (end[1] == '\0' ? 1. : g_ascii_strtod (&end[1], &end)) / 100.;
      d = CLAMP (d, 0., 1.);
      s = (gem_EventTime) win_slide_from_density (w, d);
    }
  else
    {
      return FALSE;
    }

  if (unlikely (*end != '\0'))
    return FALSE;

  OPTIONS.sample.win_size = w;
  OPTIONS.sample.win_slide = s;
  OPTIONS.sample.win_density = d;

  OPTIONS.sample.win_max_open
    = win_max_open (OPTIONS.sample.win_size, OPTIONS.sample.win_slide);

  OPTIONS.sample.win_overlap_area
    = win_overlap_area (OPTIONS.sample.win_size, OPTIONS.sample.win_slide);

  return TRUE;
}

/* Parses type diff.  */
static gboolean
opt_parse_type_diff (unused (const gchar *opt),
                     const gchar *arg,
                     unused (gpointer data),
                     unused (GError *err))
{
  gdouble min;
  gdouble max;

  g_return_val_if_fail (arg, FALSE);

  if (unlikely (!parse_number_range (arg, &min, &max)))
    return FALSE;

  min = round (min);
  max = round (max);
  OPTIONS.type_min = (gem_EventType) min;
  OPTIONS.type_max = (gem_EventType) max;
  return TRUE;
}

/* The global option entries.  */
static GOptionEntry OPTION_ENTRIES[] =
{
 {"ack-countdown", 'A', 0, G_OPTION_ARG_CALLBACK,
  pointerof (opt_parse_ack_countdown),
  "Number of acks to wait before saving [1]", NULL},
 {"ack", 'a', 0, G_OPTION_ARG_CALLBACK, pointerof (opt_parse_ack_diff),
  "Time difference between acks [0-0]", NULL},
 {"duplicates", 'd', 0, G_OPTION_FLAG_NONE, &OPTIONS.duplicates,
  "Detect duplicate output [false]", NULL},
 {"fail", 'f', 0, G_OPTION_ARG_STRING_ARRAY, &opt_failure_points,
  "Event id to trigger failure", NULL},
 {"input-size", 'n', 0, G_OPTION_ARG_CALLBACK,
  pointerof (opt_parse_input_size),
  "Input size in number of events [1000]", NULL},
 {"output", 'o', 0, G_OPTION_ARG_FILENAME, &OPTIONS.output_file,
  "File to write output to", NULL},
 {"pattern", 'p', 0, G_OPTION_ARG_CALLBACK, pointerof (opt_parse_pattern),
  "Pattern to search for [none]", NULL},
 {"runs", 'R', 0, G_OPTION_ARG_INT, &OPTIONS.runs,
  "Number of repeated runs [1]", NULL},
 {"result", 'r', 0, G_OPTION_ARG_STRING_ARRAY,
  &OPTIONS.custom_results,
  "Name of field to add to custom results", NULL},
 {"seed", 's', 0, G_OPTION_ARG_INT, &OPTIONS.sample.seed,
  "PRNG seed [0: arbitrary]", NULL},
 {"time", 't', 0, G_OPTION_ARG_CALLBACK, pointerof (opt_parse_time_diff),
  "Time difference between input events [0-0]", NULL},
 {"window", 'w', 0, G_OPTION_ARG_CALLBACK, pointerof (opt_parse_window),
  "Window specification: W/S or W%D [1/1]", NULL},
 {"trace", 'x', 0, G_OPTION_FLAG_NONE, &OPTIONS.op_trace,
  "Enable operator tracing", NULL},
 {"type", 'y', 0, G_OPTION_ARG_CALLBACK, pointerof (opt_parse_type_diff),
  "Type difference between input events [0-0]", NULL},
 {"dump-options", 0, 0, G_OPTION_FLAG_NONE, &opt_dump_options,
  "Print options and exit", NULL},
 {"no-results", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
  &OPTIONS.show_results,
  "Do not print results", NULL},
 {"no-save", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &OPTIONS.op_save,
  "Disable operator saves", NULL},
 {"no-output-ids", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
  &OPTIONS.op_output_ids,
  "Disable output ids (breaks restore output)", NULL},
 {"no-skip", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE, &OPTIONS.op_skip,
  "Disable operator skip-ranges", NULL},
 {"no-stats", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
  &OPTIONS.op_stats,
  "Disable operator statistics", NULL},
 {"progress", 0, 0, G_OPTION_FLAG_NONE, &OPTIONS.show_progress,
  "Print progress information", NULL},
 {"version", 0, 0, G_OPTION_FLAG_NONE, &opt_version,
  "Print version information and exit", NULL},
 {NULL},
};

/* Initializes @opts with defaults.  */
static void
opt_init (gem_BenchmarkOptions *opts)
{
  memset (opts, 0, sizeof (*opts));
  opts->sample.seed = 0;
  opts->sample.input_size = 1000;
  opts->sample.win_size = 1;
  opts->sample.win_slide = 1;
  opts->sample.win_density = 1;
  opts->sample.win_max_open = 2;
  opts->sample.win_overlap_area = 1;
  opts->ack_countdown = 1;
  opts->ack_min = 0;
  opts->ack_max = 0;
  opts->time_min = 0;
  opts->time_max = 0;
  opts->type_min = 0;
  opts->type_max = 0;
  opts->test_pat = GEM_TEST_PATTERN_NONE;
  opts->output_file = NULL;
  opts->failure_points = g_queue_new ();
  opts->duplicates = FALSE;
  opts->op_save = TRUE;
  opts->op_output_ids = TRUE;
  opts->op_skip = TRUE;
  opts->op_stats = TRUE;
  opts->op_trace = FALSE;
  opts->show_progress = FALSE;
  opts->show_results = TRUE;
  opts->custom_results = NULL;
  opts->runs = 1;
}

/* Dumps @opts to stdout.  */
static void
opt_print (const gem_BenchmarkOptions *opts)
{
  GString *str_failure_points;
  GString *str_custom_results;

  g_return_if_fail (opts);

  str_failure_points = g_string_new (NULL);
  g_assert_nonnull (opts->failure_points);
  if (opts->failure_points->length > 0)
    {
      GList *ell;

      ell = opts->failure_points->head;
      g_string_append_printf (str_failure_points, "%d",
                              GPOINTER_TO_INT (ell->data));

      for (ell = ell->next; ell != NULL; ell = ell->next)
        {
          g_string_append_printf (str_failure_points, ", %d",
                                  GPOINTER_TO_INT (ell->data));
        }
    }
  else
    {
      g_string_append (str_failure_points, "(none)");
    }

  str_custom_results = g_string_new (NULL);
  if (opts->custom_results != NULL && opts->custom_results[0] != NULL)
    {
      guint i;

      g_string_append (str_custom_results, opts->custom_results[0]);
      for (i = 1; opts->custom_results[i] != NULL; i++)
        {
          g_string_append (str_custom_results, ", ");
          g_string_append (str_custom_results, opts->custom_results[i]);
        }
    }
  else
    {
      g_string_append (str_custom_results, "(none)");
    }

  g_print ("\
sample-seed: %u\n\
sample-input-size: %u\n\
sample-win-size: %"GEM_TIME_FORMAT"\n\
sample-win-slide: %"GEM_TIME_FORMAT"\n\
sample-density: %g\n\
sample-max-open: %u\n\
sample-overlap-area: %u\n\
ack-countdown: %u\n\
ack-min: %"GEM_TIME_FORMAT"\n\
ack-max: %"GEM_TIME_FORMAT"\n\
time-min: %"GEM_TIME_FORMAT"\n\
time-max: %"GEM_TIME_FORMAT"\n\
type-min: %u\n\
type-max: %u\n\
test-pat: \"%s\"\n\
output-file: %s\n\
failure-points: %s\n\
duplicates: %s\n\
op-save: %s\n\
op-output-ids: %s\n\
op-skip: %s\n\
op-stats: %s\n\
op-trace: %s\n\
show-results: %s\n\
custom-results: %s\n\
runs: %u\n\
",
           opts->sample.seed,
           opts->sample.input_size,
           GEM_TIME_ARGS (opts->sample.win_size),
           GEM_TIME_ARGS (opts->sample.win_slide),
           opts->sample.win_density,
           opts->sample.win_max_open,
           opts->sample.win_overlap_area,
           opts->ack_countdown,
           GEM_TIME_ARGS (opts->ack_min),
           GEM_TIME_ARGS (opts->ack_max),
           GEM_TIME_ARGS (opts->time_min),
           GEM_TIME_ARGS (opts->time_max),
           opts->type_min,
           opts->type_max,
           gem_test_pattern_get_name (opts->test_pat),
           opts->output_file != NULL ? opts->output_file : "(none)",
           str_failure_points->str,
           strbool (opts->duplicates),
           strbool (opts->op_save),
           strbool (opts->op_output_ids),
           strbool (opts->op_skip),
           strbool (opts->op_stats),
           strbool (opts->op_trace),
           strbool (opts->show_results),
           str_custom_results->str,
           opts->runs);

  g_string_free (str_failure_points, TRUE);
  g_string_free (str_custom_results, TRUE);
}


/**
 * gem_benchmark_options_parse:
 * @argc: (inout) (optional): a pointer to the number of command line
 *   arguments
 * @argv: (inout) (array length=argc) (optional): a pointer to the array of
 *   command line arguments
 * @opts: (out): a #gem_BenchmarkOptions to fill
 * @error: (out): a return location for errors
 *
 * Parses command-line options and fills in @ops accordingly.  You must
 * clear @opts with gem_benchmark_options_clear() when done with it.
 *
 * Returns: %TRUE if successful, or %FALSE otherwise.
 */
gboolean
gem_benchmark_options_parse (gint argc,
                             gchar **argv,
                             gem_BenchmarkOptions *opts,
                             GError **error)
{
  GOptionContext *ctx;
  gboolean status;

  g_return_val_if_fail (opts, FALSE);

  ctx = g_option_context_new (NULL);
  g_option_context_add_main_entries (ctx, OPTION_ENTRIES, NULL);
  opt_init (&OPTIONS);

  status = g_option_context_parse (ctx, &argc, &argv, error);
  g_option_context_free (ctx);
  if (unlikely (!status))
    {
      goto fail;
    }

  if (unlikely (!OPTIONS.op_save && OPTIONS.ack_countdown > 0))
    {
      g_set_error (error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                   "--no-save requires --ack-coundown=0");
      goto fail;
    }

  if (unlikely (argc != 1))
    {
      g_set_error (error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
                   "too many arguments");
      goto fail;
    }

  if (opt_version)
    {
      gem_benchmark_options_clear (&OPTIONS);
      puts (PACKAGE_STRING);
      exit (EXIT_SUCCESS);
    }

  if (opt_failure_points != NULL)
    {
      parse_failure_points (opt_failure_points, OPTIONS.failure_points,
                            OPTIONS.sample.input_size);
    }

  if (opt_dump_options)
    {
      opt_print (&OPTIONS);
      gem_benchmark_options_clear (&OPTIONS);
      exit (EXIT_SUCCESS);
    }

  *opts = OPTIONS;
  return TRUE;

 fail:
  gem_benchmark_options_clear (&OPTIONS);
  return FALSE;
}

/**
 * gem_benchmark_options_clear:
 * @opts: the #gem_BenchmarkOptions to clear
 *
 * Clears the given benchmark options.
 */
void
gem_benchmark_options_clear (gem_BenchmarkOptions *opts)
{
  g_return_if_fail (opts);

  g_queue_free (opts->failure_points);
}
