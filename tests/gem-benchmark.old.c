/* gem -- The Grand Event Mower.
   Copyright (C) 2018 Guilherme F. Lima

This file is part of Gem.

Gem is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Gem is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Gem.  If not, see <https://www.gnu.org/licenses/>.  */

#include "gem-test.h"

/* The name of this program.  */
#define PROGRAM_NAME "gem-benchmark"

/* Forward declarations.  */
static void error (const gchar *, ...);
static void usage (gint);
static gboolean version (void);
static GString *id_array_to_string (const GArray *);
static GArray *opt_parse_fail_points_array (gchar **);


/**** Options ****/

typedef struct _BenchmarkOptions
{
  gem_test_RandSample sample;   /* test sample */
  struct {
    gem_EventTime min;          /* min ack diff */
    gem_EventTime max;          /* max ack diff */
    guint counter;              /* #acks before save */
  } ack;
  struct {
    gem_EventTime min;          /* min time diff */
    gem_EventTime max;          /* max time diff */
  } time;
  struct {
    gem_EventType min;          /* min type diff */
    gem_EventType max;          /* max type diff */
  } type;
  struct {
    gboolean save;              /* save op flag */
    gboolean skip;              /* skip op flag */
    gboolean stats;             /* stats op flag */
    gboolean trace;             /* trace op flag */
  } flags;
  struct {
    GArray *points;             /* failure points, array of gem_EventID */
    gchar **str_points;         /* string version of points array */
  } fail;
  gem_test_Pattern test_pat;    /* test pattern */
  gboolean detail;              /* detail results */
  gboolean dump_options;        /* dump options */
  gboolean output_cache;        /* cache output in memory */
  gchar *output_file;           /* file to write output  */
} BenchmarkOptions;

/* Global options.  */
static BenchmarkOptions OPTIONS;

/* Initialize global options.  */
static void
benchmark_options_init (void)
{
  OPTIONS.sample.seed = (guint)(g_get_monotonic_time () % G_MAXUINT);
  OPTIONS.sample.input_size = 1000;
  OPTIONS.sample.win_size = 1;
  OPTIONS.sample.win_slide = 1;
  OPTIONS.sample.win_density = 1;
  OPTIONS.sample.win_max_open = 1;
  OPTIONS.ack.min = 0;
  OPTIONS.ack.max = 0;
  OPTIONS.ack.counter = 1;
  OPTIONS.time.min = 0;
  OPTIONS.time.max = 0;
  OPTIONS.type.min = 0;
  OPTIONS.type.max = 0;
  OPTIONS.flags.save = TRUE;
  OPTIONS.flags.skip = TRUE;
  OPTIONS.flags.stats = TRUE;
  OPTIONS.flags.trace = FALSE;
  OPTIONS.fail.str_points = NULL;
  OPTIONS.fail.points = NULL;
  OPTIONS.test_pat = GEM_TEST_PATTERN_NONE;
  OPTIONS.detail = FALSE;
  OPTIONS.dump_options = FALSE;
  OPTIONS.output_cache = TRUE;
  OPTIONS.output_file = NULL;
}

/* Finalize global options.  */
static void
benchmark_options_fini (void)
{
  g_array_free (OPTIONS.fail.points, TRUE);
}

/* Process global options.  */
static void
benchmark_options_process (void)
{
  OPTIONS.fail.points
    = opt_parse_fail_points_array (OPTIONS.fail.str_points);
}

/* Prints global options to stdout.  */
static void
benchmark_options_print (void)
{
  /* sample */
  g_print ("sample.seed:\t%u\n", OPTIONS.sample.seed);
  g_print ("sample.input_size:\t%u\n", OPTIONS.sample.input_size);
  g_print ("sample.win_size:\t%"G_GUINT64_FORMAT"\n",
           OPTIONS.sample.win_size);
  g_print ("sample.win_slide:\t%"G_GUINT64_FORMAT"\n",
           OPTIONS.sample.win_slide);
  g_print ("sample.win_density:\t%g\n", OPTIONS.sample.win_density);
  g_print ("sample.win_max_open:\t%u\n", OPTIONS.sample.win_max_open);

  /* ack */
  g_print ("ack.min:\t%"GEM_TIME_FORMAT"\n",
           GEM_TIME_ARGS (OPTIONS.ack.min));
  g_print ("ack.max:\t%"GEM_TIME_FORMAT"\n",
           GEM_TIME_ARGS (OPTIONS.ack.max));
  g_print ("ack.counter:\t%u\n", OPTIONS.ack.counter);

  g_print ("time.min:\t%"GEM_TIME_FORMAT"\n",
           GEM_TIME_ARGS (OPTIONS.time.min));
  g_print ("time.max:\t%"GEM_TIME_FORMAT"\n",
           GEM_TIME_ARGS (OPTIONS.time.max));

  /* type */
  g_print ("type.min:\t%u\n", OPTIONS.type.min);
  g_print ("type.max:\t%u\n", OPTIONS.type.max);

  /* flags */
  g_print ("flags.save:\t%s\n", strbool (OPTIONS.flags.save));
  g_print ("flags.skip:\t%s\n", strbool (OPTIONS.flags.skip));
  g_print ("flags.stats:\t%s\n", strbool (OPTIONS.flags.stats));
  g_print ("flags.trace:\t%s\n", strbool (OPTIONS.flags.trace));
  if (OPTIONS.fail.points->len > 0)
    {
      GString *str = id_array_to_string (OPTIONS.fail.points);
      g_print ("fail.points (%u):\t%s\n", OPTIONS.fail.points->len,
               str->str);
      g_string_free (str, TRUE);
    }
  else
    {
      g_print ("fail.points:\t%s\n", (gchar *) OPTIONS.fail.points);
    }

  /* other */
  g_print ("pattern:\t%s\n", gem_test_pattern_get_name (OPTIONS.test_pat));
  g_print ("detail:\t%s\n", strbool (OPTIONS.detail));
  g_print ("dump_options:\t%s\n", strbool (OPTIONS.dump_options));
  g_print ("output_cache:\t%s\n", strbool (OPTIONS.output_cache));
  g_print ("output_file:\t%s\n", OPTIONS.output_file);
}

/* Parses test pattern name.  */
static gboolean
opt_parse_pattern (unused (const gchar *opt),
                   const gchar *arg,
                   unused (gpointer data),
                   unused (GError *err))
{
  return gem_test_pattern_by_name (arg, &OPTIONS.test_pat);
}

/* Parses a time value with unit suffix (h, m, s, ms, us).  */
static gem_EventTime
opt_parse_time (const gchar *arg,
                gchar **end)
{
  gdouble d;
  gchar *c;

  g_return_val_if_fail (arg, FALSE);

  d = g_ascii_strtod (arg, &c);
  if ((c[0] == 'u' && c[1] == 's')
      || (c[0] == 'U' && c[1] == 'S'))
    {
      c += 2;
    }
  else if ((c[0] == 'm' && c[1] == 's')
           || (c[0] == 'M' && c[1] == 'S'))
    {
      d *= 1000;
      c += 2;
    }
  else if (*c == 's' || *c == 'S')
    {
      d *= G_USEC_PER_SEC;
      c++;
    }
  else if (*c == 'm' || *c == 'M')
    {
      d *= G_USEC_PER_SEC * 60;
      c++;
    }
  else if (*c == 'h' || *c == 'H')
    {
      d *= G_USEC_PER_SEC * (gdouble) 3600;
      c++;
    }

  tryset (end, c);

  return (gem_EventTime) d;
}

/* Parses an unsigned integer with unit suffix (K, M, G).  */
static guint64
opt_parse_uint64 (const gchar *arg,
                  gchar **end)
{
  guint64 i;
  gchar *c;

  g_return_val_if_fail (arg, FALSE);

  i = g_ascii_strtoull (arg, &c, 10);
  if (*c == 'K' || *c == 'k')
    {
      i *= 1000;
      c++;
    }
  else if (*c == 'M' || *c == 'm')
    {
      i *= 1000000;
      c++;
    }
  else if (*c == 'G' || *c == 'g')
    {
      i *= 1000000000;
      c++;
    }

  tryset (end, c);

  return i;
}

/* Parses time range.  */
static gboolean
opt_parse_time_range (const gchar *arg,
                      gem_EventTime *min,
                      gem_EventTime *max)
{
  gem_EventTime lo, hi;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

  lo = opt_parse_time (arg, &end);
  if (*end == '\0')
    {
      hi = lo;
      goto done;
    }

  if (unlikely (*end != '-'))
    return FALSE;

  hi = opt_parse_time (++end, &end);
  if (unlikely (*end != '\0'))
    return FALSE;

done:
  *min = lo;
  *max = MAX (lo, hi);

  return TRUE;
}

/* Parses integer range.  */
static gboolean
opt_parse_uint64_range (const gchar *arg,
                        guint64 *min,
                        guint64 *max)
{
  guint64 lo, hi;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

  lo = opt_parse_uint64 (arg, &end);
  if (*end == '\0')
    {
      hi = lo;
      goto done;
    }

  if (unlikely (*end != '-'))
    return FALSE;

  hi = opt_parse_uint64 (++end, &end);
  if (unlikely (*end != '\0'))
    return FALSE;

done:
  *min = lo;
  *max = MAX (lo, hi);

  return TRUE;
}

/* Parses input size.  */
static gboolean
opt_parse_input_size (unused (const gchar *opt),
                      const gchar *arg,
                      unused (gpointer data),
                      unused (GError *err))
{
  guint64 size;

  g_return_val_if_fail (arg, FALSE);

  size = opt_parse_uint64 (arg, NULL);
  OPTIONS.sample.input_size = (guint) MIN (size, G_MAXUINT);

  return TRUE;
}

/* Parses string array with points of failures.  */
static GArray *
opt_parse_fail_points_array (gchar **str_array)
{
  GArray *array;
  guint i;

  array = g_array_new (FALSE, FALSE, sizeof (gem_EventID));
  if (str_array != NULL)
    {
      for (i = 0; str_array[i] != NULL; i++)
        {
          gem_EventID id;

          id = (gem_EventID) opt_parse_uint64 (str_array[i], NULL);
          g_array_append_val (array, id);
        }
    }

  return array;
}

/* Parses ack counter.  */
static gboolean
opt_parse_ack_counter (unused (const gchar *opt),
                       const gchar *arg,
                       unused (gpointer data),
                       unused (GError *err))
{
  guint64 size;

  g_return_val_if_fail (arg, FALSE);

  size = opt_parse_uint64 (arg, NULL);
  OPTIONS.ack.counter = (guint) MIN (size, G_MAXUINT);

  return TRUE;
}

/* Parses ack diff.  */
static gboolean
opt_parse_ack_diff (unused (const gchar *opt),
                    const gchar *arg,
                    unused (gpointer data),
                    unused (GError *err))
{
  g_return_val_if_fail (arg, FALSE);

  return opt_parse_time_range (arg, &OPTIONS.ack.min, &OPTIONS.ack.max);
}

/* Parses time diff.  */
static gboolean
opt_parse_time_diff (unused (const gchar *opt),
                     const gchar *arg,
                     unused (gpointer data),
                     unused (GError *err))
{
  g_return_val_if_fail (arg, FALSE);

  return opt_parse_time_range (arg, &OPTIONS.time.min, &OPTIONS.time.max);
}

/* Parses type diff.  */
static gboolean
opt_parse_type_diff (unused (const gchar *opt),
                     const gchar *arg,
                     unused (gpointer data),
                     unused (GError *err))
{
  guint64 min;
  guint64 max;

  g_return_val_if_fail (arg, FALSE);

  if (unlikely (!opt_parse_uint64_range (arg, &min, &max)))
    {
      return FALSE;
    }
  else
    {
      OPTIONS.type.min = (gem_EventType) MIN (min, G_MAXUINT);
      OPTIONS.type.max = (gem_EventType) MIN (max, G_MAXUINT);
      return TRUE;
    }
}

/* Parses window specification.  */
static gboolean
opt_parse_window_spec (unused (const gchar *opt),
                       const gchar *arg,
                       unused (gpointer data),
                       unused (GError *err))
{
  gem_EventTime w;
  gchar *end;

  g_return_val_if_fail (arg, FALSE);

# define win_slide_from_density(w, d)\
  ((1 - ((gdouble)(d))) * ((gdouble)(w)) + 1)

# define win_density_from_slide(w, s)\
  (1 - (((gdouble)(s)) - 1) / ((gdouble)(w)))

# define win_max_open(w, s)\
  ((guint)(((w) / (s)) + 1))

  w = opt_parse_time (arg, &end);
  if (unlikely (w == 0))
    return FALSE;

  if (*end == '/')
    {
      gem_EventTime s = opt_parse_time (&end[1], NULL);
      OPTIONS.sample.win_slide = s;
      OPTIONS.sample.win_density = win_density_from_slide (w, s);
    }
  else if (*end == '%')
    {
      gdouble d = g_ascii_strtod (&end[1], NULL);
      OPTIONS.sample.win_density = d / 100.;
      OPTIONS.sample.win_slide
        = (gem_EventTime) win_slide_from_density (w, d / 100.);
    }
  else if (unlikely (*end != '\0'))
    {
      return FALSE;
    }

  OPTIONS.sample.win_size = w;
  OPTIONS.sample.win_max_open = win_max_open (OPTIONS.sample.win_size,
                                              OPTIONS.sample.win_slide);
  return TRUE;
}


/**** Auxiliary ****/

/* Prints formatted error message.  */
static G_GNUC_PRINTF (1, 2) void
error (const gchar *fmt, ...)
{
  va_list args;

  va_start (args, fmt);
  g_fprintf (stderr, "%s: ", g_get_prgname ());
  g_vfprintf (stderr, fmt, args);
  g_fprintf (stderr, "\n");

  va_end (args);
}

/* Prints usage message and exits. */
static void
usage (gint status)
{
  if (status != EXIT_SUCCESS)
    g_printerr ("Try '%s --help' for more information.\n",
                g_get_prgname ());
  exit (status);
}

/* Prints version string and exits.  */
static gboolean
version (void)
{
  puts (PACKAGE_STRING);
  exit (EXIT_SUCCESS);
}

/* Formats array of #gem_EventID as a string.  */
static GString *
id_array_to_string (const GArray *array)
{
  GString *str;
  guint i;

  g_return_val_if_fail (array, NULL);

  str = g_string_new (NULL);
  if (array->len == 0)
    return str;

  g_string_append_printf (str, "%"G_GUINT64_FORMAT,
                          g_array_index (array, gem_EventID, 0));

  for (i = 1; i < array->len; i++)
    {
      g_string_append_printf (str, ", %"G_GUINT64_FORMAT,
                              g_array_index (array, gem_EventID, i));
    }

  return str;
}

/* Formats queue of #gem_EventID as a string.  */
static GString *
id_queue_to_string (const GQueue *queue)
{
  GString *str;
  GList *ell;

  g_return_val_if_fail (queue, NULL);

  str = g_string_new (NULL);
  if (queue->length == 0)
    return str;

  ell = queue->head;
  g_string_append_printf (str, "%d", GPOINTER_TO_INT (ell->data));

  for (ell = ell->next; ell != NULL; ell = ell->next)
    {
      g_string_append_printf (str, ", %d", GPOINTER_TO_INT (ell->data));
    }

  return str;
}

/* Writes event queue to file or stdout if @name is '-'.  */
static gboolean
write_event_queue_to_file (const GQueue *queue, const gchar *name)
{
  FILE *file;
  GList *ell;

  g_return_val_if_fail (queue, FALSE);
  g_return_val_if_fail (name, FALSE);

  if (g_str_equal (name, "-"))
    {
      file = stdout;
    }
  else
    {
      file = g_fopen (name, "w");
      if (unlikely (file == NULL))
        {
          error ("Couldn't create file '%s'\n", name);
          return FALSE;
        }
    }

  for (ell = queue->head; ell != NULL; ell = ell->next)
    {
      GString *str;

      str = gem_event_to_string (ell->data);
      fputs (str->str, file);
      fputc ('\n', file);
      g_string_free (str, TRUE);
    }

  if (file != stdout)
    fclose (file);

  return TRUE;
}

/* Compares two events by time.  */
static gint
compare_evt_time (gconstpointer a, gconstpointer b, unused (gpointer data))
{
  const gem_Event *e1 = a;
  const gem_Event *e2 = b;

  if (e1->time > e2->time)
    return 1;
  else if (e1->time == e2->time)
    return 0;
  else
    return -1;
}

/* Compares two integers.  */
static gint
compare_int (gconstpointer a, gconstpointer b, unused (gpointer data))
{
  gint ai = GPOINTER_TO_INT (a);
  gint bi = GPOINTER_TO_INT (b);

  if (ai > bi)
    return 1;
  else if (ai == bi)
    return 0;
  else
    return -1;
}

/* Counts the number of duplicate events in queue.  */
static guint
count_duplicates (const GQueue *queue)
{
  GSequence *seq;
  GList *ell;
  guint dups;

  g_return_val_if_fail (queue, 0);

  if (queue->length == 0)
    return 0;

  dups = 0;
  seq = g_sequence_new (NULL);

  for (ell = queue->head; ell != NULL; ell = ell->next)
    {
      gem_Event *out;
      guint h;

      out = ell->data;
      h = gem_event_array_hash (out->match);
      if (g_sequence_lookup (seq, GINT_TO_POINTER (h), compare_int, NULL))
        {
          dups++;
          continue;
        }
      g_sequence_insert_sorted (seq, GINT_TO_POINTER (h),
                                compare_int, NULL);
    }

  g_sequence_free (seq);

  return dups;
}


/**** The Benchmark ****/

/* Benchmark statistics.  */
typedef struct _BenchmarkStats
{
  gem_EventTime accum_time;      /* accumulated time of input events */
  GTimer *run_time;              /* total runtime */
  GTimer *work_time;             /* total runtime without restores */
  GTimer *save_time;             /* total save time */
  GTimer *restore_time;          /* total restore time */
  guint evts_out;                /* #events output */
  guint evts_in_buf_max;         /* max #events saved in input queue */
  guint evts_in_buf_trimmed;     /* #events trimmed from input queue */
  guint evts_restore_out;        /* #events output while restoring */
  guint evts_restore_replay;     /* #events replayed while restoring */
  guint failures;                /* #failures */
  GQueue fail_ids;               /* queue of ids that triggered failures */
  guint restores;                /* #restores */
  guint acks;                    /* #acks */
  guint saves;                   /* #saves */
  guint saves_succ;              /* #successful saves */
  guint saves_fail;              /* #failed saves */
  guint saves_start_trimmed;     /* num of ids trimmed */
  guint saves_output_ids;        /* #ids in all output-ids */
  guint saves_skip_ranges;       /* #ids in all skip-ranges */
} BenchmarkStats;

#define PERCENT(x,total) (((gdouble)(x) / (gdouble)(total)) * 100.)

/* Prints benchmark results.  */
static void
benchmark_report (const gem_Operator *op,
                  const GQueue *output,
                  const BenchmarkStats *st)
{
  gulong run_time;
  gdouble run_time_secs;

  gulong work_time;
  gdouble work_time_secs;

  gulong save_time;
  gdouble save_time_secs;

  gulong restore_time;
  gdouble restore_time_secs;

  g_return_if_fail (op);
  g_return_if_fail (output);

  g_assert_nonnull (st->run_time);
  run_time_secs = g_timer_elapsed (st->run_time, &run_time);

  g_assert_nonnull (st->work_time);
  work_time_secs = g_timer_elapsed (st->work_time, &work_time);

  g_assert_nonnull (st->save_time);
  save_time_secs = g_timer_elapsed (st->save_time, &save_time);

  g_assert_nonnull (st->restore_time);
  restore_time_secs = g_timer_elapsed (st->restore_time, &restore_time);

  g_print ("\n--- Gem Benchmark Results ----\n");

  g_print ("\nSample:\n\
  seed: %u\n\
  input-size: %u\n\
  win-size: %"GEM_TIME_FORMAT"\n\
  win-slide: %"GEM_TIME_FORMAT"\n\
  win-density: %g\n\
  win-max-open: %u\n",
           OPTIONS.sample.seed,
           OPTIONS.sample.input_size,
           GEM_TIME_ARGS (OPTIONS.sample.win_size),
           GEM_TIME_ARGS (OPTIONS.sample.win_slide),
           OPTIONS.sample.win_density,
           OPTIONS.sample.win_max_open);

  /* Operator.  */
  {
    guint flags;

    flags = gem_operator_get_flags (op);

    g_print ("\nOperator:\n\
  pattern: %s\n\
  start-id: %"G_GUINT64_FORMAT"\n\
  window-size: %"G_GUINT64_FORMAT"\n\
  window-slide: %"G_GUINT64_FORMAT"\n\
  flags\n\
    . save: %s\n\
    . skip: %s\n\
    . stats: %s\n\
    . trace: %s\n",
             gem_test_pattern_get_name (OPTIONS.test_pat),
             gem_operator_get_start_id (op),
             gem_operator_get_window_size (op),
             gem_operator_get_window_slide (op),
             strbool (flags & GEM_OPERATOR_FLAG_SAVE),
             strbool (flags & GEM_OPERATOR_FLAG_SKIP_RANGES),
             strbool (flags & GEM_OPERATOR_FLAG_STATISTICS),
             strbool (flags & GEM_OPERATOR_FLAG_TRACE));

    if (flags & GEM_OPERATOR_FLAG_STATISTICS)
      {
        gem_OperatorStats stats;

        stats = gem_operator_get_statistics (op);
        g_print ("\
  statistics\n\
    . evts-pushed: %u\n\
    . evts-skipped: %u (%.1f%%)\n\
    . evts-popped: %u\n\
    . wins-opened: %u\n\
    . wins-closed: %u\n\
    . wins-diposed: %u\n\
    . evts/win: %.1f in, %.1f out\n",
                 stats.evts_pushed,
                 stats.evts_skipped,
                 PERCENT (stats.evts_skipped, stats.evts_pushed),
                 stats.evts_popped,
                 stats.wins_opened,
                 stats.wins_closed,
                 stats.wins_disposed,
                 stats.evts_pushed / (gdouble) stats.wins_opened,
                 stats.evts_popped / (gdouble) stats.wins_opened);
      }

  }

  /* Run.  */
  {
    guint evts_in;
    guint evts_out;

    evts_in = OPTIONS.sample.input_size;
    evts_out = st->evts_out;

    g_print ("\nRun:\n\
  run-time: %"GEM_TIME_FORMAT"\n\
  work-time: %"GEM_TIME_FORMAT" (%.1f%%)\n\
  input: %u evts, %g evts/s, %g us/evt\n\
  input-time: %"GEM_TIME_FORMAT"\n\
  input-buf: max, %u evts, trimmed %u evts\n\
  output: %u evts (%.1f%% of input)\n",
             GEM_TIME_ARGS (run_time),
             GEM_TIME_ARGS (work_time),
             PERCENT (work_time_secs, run_time_secs),
             evts_in,
             (gdouble) evts_in / run_time_secs,
             (gdouble) run_time / evts_in,
             GEM_TIME_ARGS (st->accum_time),
             st->evts_in_buf_max,
             st->evts_in_buf_trimmed,
             evts_out,
             PERCENT (evts_out, evts_in));
  }

  /* Saves.  */
  if (st->acks > 0)
    {
      g_print ("\nAcks/Saves:\n  acks: %u\n", st->acks);
      if (st->saves > 0)
        {
          g_print ("\
  saves: %u, %"GEM_TIME_FORMAT" (%.1f%%)\n\
    . succ: %u (%.1f%%)\n\
    . fail: %u (%.1f%%)\n\
    . start-trim: %u, avg %u/save\n\
    . output-ids: %u, avg %u/save\n\
    . skip-ranges: %u, avg %u/save\n",
                   st->saves,
                   GEM_TIME_ARGS (save_time),
                   PERCENT (save_time_secs, run_time_secs),
                   st->saves_succ,
                   PERCENT (st->saves_succ, st->saves),
                   st->saves_fail,
                   PERCENT (st->saves_fail, st->saves),
                   st->saves_start_trimmed,
                   st->saves_start_trimmed / st->saves,
                   st->saves_output_ids,
                   st->saves_output_ids / st->saves,
                   st->saves_skip_ranges,
                   st->saves_skip_ranges / st->saves);
        }
    }

  /* Restores.  */
  if (st->restores > 0)
    {
      GString *str;

      str = id_queue_to_string (&st->fail_ids);

      g_print ("\nRestores:\n\
  failures/restores: %u/%u\n\
  restore-time: %"GEM_TIME_FORMAT" (%.1f%%), avg %"
               GEM_TIME_FORMAT"/restore\n\
  evts-out: %u, avg %u/restore\n\
  evts-replayed: %u, avg %u/restore\n\
  fail-ids: %s\n",
               st->failures,
               st->restores,
               GEM_TIME_ARGS (restore_time),
               PERCENT (restore_time_secs, run_time_secs),
               GEM_TIME_ARGS ((gdouble) restore_time / st->restores),
               st->evts_restore_out,
               st->evts_restore_out / st->restores,
               st->evts_restore_replay,
               st->evts_restore_replay / st->restores,
               str->str);
      g_string_free (str, TRUE);
    }

  /* Output.  */
  {
    g_print ("\nOutput:\n");

    if (OPTIONS.output_cache)
      {
        g_print ("  length: %u\n", output->length);
        if (output->length > 0)
          {
            guint dups = count_duplicates (output);;
            g_print ("  duplicates: %u (%.1f%%)\n",
                     dups, PERCENT (dups, output->length));
          }
      }
    else if (OPTIONS.output_file)
      {
        g_print ("  written to: %s\n",
                 g_str_equal (OPTIONS.output_file, "-")
                 ? "stdout" : OPTIONS.output_file);
      }
    else
      {
        g_printf ("  (discarded)\n");
      }
  }
}

static void
collect_output (gem_Event *out,
                GQueue *output,
                FILE *sink)
{
  if (OPTIONS.output_cache)
    {
      g_queue_push_tail (output, out);
    }
  else if (sink != NULL)
    {
      GString *str;
      guint hash;

      str = gem_event_to_string (out);
      hash = gem_event_array_hash (out->match);
      g_string_append_printf (str, "\t%u\n", hash);
      fputs (str->str, sink);
      fflush (sink);
      g_string_free (str, TRUE);
      gem_event_unref (out);
    }
  else                          /* discard */
    {
      gem_event_unref (out);
    }
}

static gem_EventID
next_failure (BenchmarkStats *st)
{
  if (OPTIONS.fail.points == NULL
      || st->failures >= OPTIONS.fail.points->len)
    {
      return GEM_EVENT_TIME_NONE;
    }
  else
    {
      return g_array_index (OPTIONS.fail.points,
                            gem_EventID, st->failures++);
    }
}

static void
trim_input (GQueue *input, gem_Savepoint *save, BenchmarkStats *st)
{
  gem_EventID start_id;
  GArray *skip;
  gem_Event *evt;
  GList *ell;
  guint i;

  g_return_if_fail (input);
  g_return_if_fail (save);

  start_id = gem_savepoint_get_start_id (save);
  while (input->length > 0)
    {
      evt = g_queue_peek_head (input);
      if (evt->id >= start_id)
        break;
      gem_event_unref (g_queue_pop_head (input));
      st->evts_in_buf_trimmed++;
      st->saves_start_trimmed++;
    }

  skip = gem_savepoint_get_skip_ranges (save);
  if (skip == NULL || skip->len == 0)
    return;

  i = 0;
  ell = input->head;
  while (ell != NULL && i < skip->len)
    {
      gem_EventID lo;
      gem_EventID hi;

      evt = ell->data;
      lo = g_array_index (skip, gem_EventID, i);
      hi = g_array_index (skip, gem_EventID, i + 1);

      if (evt->id >= lo && evt->id <= hi)
        {
          GList *next = ell->next;
          gem_event_unref (ell->data);
          g_queue_delete_link (input, ell);
          ell = next;
          st->evts_in_buf_trimmed++;
          st->saves_skip_ranges++;
        }
      else if (evt->id > hi)
        {
          i += 2;
        }
      else
        {
          ell = ell->next;
        }
    }
}

/* Returns the next failure id or GEM_EVENT_ID_NONE.  */
static void
benchmark (GQueue *output)
{
  gem_test_Rand *rand;
  gem_PatternConfig cfg;
  gem_Operator *op;
  guint flags;

  GQueue _input;
  GQueue *input;
  FILE *outsink;

  BenchmarkStats _st;
  BenchmarkStats *st;

  guint events_left;            /* #events left */
  GSequence *acks;              /* pending acks */
  gem_Savepoint *save;          /* current save */
  gem_EventID fail;             /* next failure */
  gboolean restoring;           /* restore in progress */
  GList *restore_ell;           /* pointer in input */
  guint n;

  rand = gem_test_rand_new_full_with_seed (OPTIONS.sample.seed, 0,
                                           OPTIONS.type.min,
                                           OPTIONS.type.max,
                                           OPTIONS.time.min,
                                           OPTIONS.time.max);

  cfg = gem_test_pattern (OPTIONS.test_pat);
  op = gem_operator_new (&cfg, 0,
                         OPTIONS.sample.win_size,
                         OPTIONS.sample.win_slide);
  gem_operator_unset_flags (op, -1);
  flags = 0;
  if (OPTIONS.flags.save)
    flags |= GEM_OPERATOR_FLAG_SAVE;
  if (OPTIONS.flags.skip)
    flags |= GEM_OPERATOR_FLAG_SKIP_RANGES;
  if (OPTIONS.flags.stats)
    flags |= GEM_OPERATOR_FLAG_STATISTICS;
  if (OPTIONS.flags.trace)
    flags |= GEM_OPERATOR_FLAG_TRACE;
  gem_operator_set_flags (op, flags);

  g_queue_init (&_input);
  input = &_input;

  outsink = NULL;
  if (!OPTIONS.output_cache && OPTIONS.output_file != NULL)
    {
      const gchar *name = OPTIONS.output_file;
      outsink = g_str_equal (name, "-") ? stdout : fopen (name, "w");
      if (unlikely (outsink == NULL))
        {
          error ("Couldn't create file '%s'\n", name);
        }
    }

  memset (&_st, 0, sizeof (_st));
  st = &_st;
  g_queue_init (&st->fail_ids);

  events_left = OPTIONS.sample.input_size;
  acks = g_sequence_new (NULL);
  save = NULL;
  fail = next_failure (st);
  restoring = FALSE;

  st->save_time = g_timer_new ();
  g_timer_stop (st->save_time);

  st->restore_time = g_timer_new ();
  g_timer_stop (st->restore_time);

  st->run_time = g_timer_new ();
  st->work_time = g_timer_new ();
  while (events_left > 0)
    {
      gem_Event *evt;
      gem_EventID evt_id;
      gem_EventTime evt_time;

      /* Restore is over.  */
      if (restoring && restore_ell == NULL)
        {
          restoring = FALSE;
          g_timer_stop (st->restore_time);
          g_timer_continue (st->work_time);
        }

      /* Generate new input event.  */
      if (!restoring)
        {
          g_timer_stop (st->work_time);
          g_timer_stop (st->run_time);

          evt = gem_test_rand_event (rand);
          st->accum_time += evt->time;
          evt->time = st->accum_time;

          events_left--;
          g_queue_push_tail (input, evt);
          st->evts_in_buf_max = MAX (st->evts_in_buf_max, input->length);

          g_timer_continue (st->work_time);
          g_timer_continue (st->run_time);
        }
      /* Use next event from input queue.  */
      else
        {
          evt = restore_ell->data;
          restore_ell = restore_ell->next;
          st->evts_restore_replay++;
        }

      evt_id = evt->id;
      evt_time = evt->time;

      /* Push event.  */
      n = gem_operator_push (op, evt);
      while (n > 0)
        {
          gem_Event *out;

          out = gem_operator_pop (op);
          g_assert_nonnull (out);

          if (!restoring)
            {
              /* g_timer_stop (st->work_time); */
              /* g_timer_stop (st->run_time); */

              if (OPTIONS.output_cache && OPTIONS.ack.counter > 0)
                {
                  out->time = evt_time
                    + gem_test_rand_event_time_full (rand,
                                                     OPTIONS.ack.min,
                                                     OPTIONS.ack.max);
                  g_sequence_insert_sorted (acks, out,
                                            compare_evt_time, NULL);
                }

              collect_output (out, output, outsink);
              st->evts_out++;

              /* g_timer_continue (st->run_time); */
              /* g_timer_continue (st->work_time); */
            }
          else
            {
              st->evts_restore_out++;
              gem_event_unref (out);
            }
          n--;
        }

      /* Check for pending acks.  */
      if (!restoring && OPTIONS.ack.counter > 0)
        {
          GSequenceIter *it;
          gem_Event *next;
          gem_Event *out;

          out = NULL;
          it = g_sequence_get_begin_iter (acks);
          while (!g_sequence_iter_is_end (it))
            {
              next = g_sequence_get (it);
              if (next->time > evt_time)
                break;

              out = next;
              g_sequence_remove (it);
              it = g_sequence_get_begin_iter (acks);
            }

          if (out != NULL)      /* ack */
            {
              g_assert (out->time <= evt_time);
              st->acks++;

              if (OPTIONS.ack.counter > 0
                  && st->acks % OPTIONS.ack.counter == 0) /* save */
                {
                  gem_Savepoint *sp;

                  g_timer_stop (st->work_time);

                  g_timer_continue (st->save_time);
                  sp = gem_operator_save (op, out, TRUE);
                  g_timer_stop (st->save_time);

                  st->saves++;
                  if (sp != NULL)
                    {
                      GArray *out_ids;
                      st->saves_succ++;
                      if (save != NULL)
                        gem_savepoint_unref (save);
                      save = sp;
                      trim_input (input, save, st);
                      out_ids = gem_savepoint_get_output_ids (save);
                      st->saves_output_ids += out_ids->len;
                    }
                  else
                    {
                      st->saves_fail++;
                    }

                  g_timer_continue (st->work_time);
                }
            }
        }

      /* Check if we should fail.   */
      if (evt_id == fail)
        {
          g_timer_stop (st->work_time);

          st->restores++;
          g_queue_push_tail (&st->fail_ids, GINT_TO_POINTER (evt_id));
          g_timer_continue (st->restore_time);
          gem_operator_restore (op, save);

          restore_ell = input->head;
          restoring = TRUE;
          fail = next_failure (st);
        }
    }

  g_timer_stop (st->work_time);
  g_timer_stop (st->run_time);

  n = gem_operator_flush (op);
  while (n > 0)
    {
      gem_Event *out;

      out = gem_operator_pop (op);
      g_assert_nonnull (out);
      collect_output (out, output, outsink);
      st->evts_out++;
      n--;
    }

  while (input->length > 0)
    gem_event_unref (g_queue_pop_head (input));
  g_queue_clear (input);

  if (outsink != NULL && outsink != stdout)
    fclose (outsink);

  benchmark_report (op, output, st);

  g_sequence_free (acks);
  g_timer_destroy (st->run_time);
  g_timer_destroy (st->work_time);
  g_timer_destroy (st->restore_time);
  g_queue_clear (&st->fail_ids);

  gem_operator_unref (op);
  gem_test_rand_free (rand);
}


/**** Main ****/

gint
main (gint argc, gchar *argv[])
{
  static GOptionEntry entries[] =
  {
   {"ack-counter", 'A', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_ack_counter),
    "Number of acks to await before saving [1]", NULL},

   {"ack-diff", 'a', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_ack_diff),
    "Time diff between output event acks [0-0]", NULL},

   {"detail", 'd', 0, G_OPTION_FLAG_NONE, &OPTIONS.detail,
    "Detailed report", NULL},

   {"fail", 'f', 0, G_OPTION_ARG_STRING_ARRAY, &OPTIONS.fail.str_points,
    "Fail at the given input event", NULL},

   {"size", 'n', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_input_size),
    "Input size in #evts [1000]", NULL},

   {"output", 'o', 0, G_OPTION_ARG_FILENAME, &OPTIONS.output_file,
    "Write output to file", NULL},

   {"pattern", 'p', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_pattern),
    "Pattern to search for", NULL},

   {"seed", 's', 0, G_OPTION_ARG_INT, &OPTIONS.sample.seed,
    "PRNG seed", NULL},

   {"time-diff", 't', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_time_diff),
    "Time diff between input events [0-0]", NULL},

   {"window-spec", 'w', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_window_spec),
    "Window specification: 'w/s' or 'w/d%' [1/1]", NULL},

   {"dump-options", 'X', 0, G_OPTION_FLAG_NONE, &OPTIONS.dump_options,
    "Dump options to stdout and exit", NULL},

   {"trace", 'x', 0, G_OPTION_FLAG_NONE, &OPTIONS.flags.trace,
    "Enable trace", NULL},

   {"type-diff", 'y', 0, G_OPTION_ARG_CALLBACK,
    pointerof (opt_parse_type_diff),
    "Type diff between input events [0-0]", NULL},

   {"no-output-cache", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
    &OPTIONS.output_cache,
    "Disable output caching", NULL},

   {"no-save", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
    &OPTIONS.flags.save,
    "Disable saves", NULL},

   {"no-skip", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
    &OPTIONS.flags.skip,
    "Disable skip ranges", NULL},

   {"no-stats", 0, G_OPTION_FLAG_REVERSE, G_OPTION_ARG_NONE,
    &OPTIONS.flags.stats,
    "Disable statistics", NULL},

   {"version", 0, G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK,
    pointerof (version),
    "Print version information and exit", NULL},

   {NULL},
  };

  GOptionContext *ctx;
  gboolean status;
  GError *err;
  GQueue output;

  g_set_prgname (PROGRAM_NAME);

  ctx = g_option_context_new (NULL);
  g_option_context_add_main_entries (ctx, entries, NULL);

  benchmark_options_init ();

  err = NULL;
  status = g_option_context_parse (ctx, &argc, &argv, &err);
  g_option_context_free (ctx);

  if (unlikely (!status))
    {
      g_assert_nonnull (err);
      error ("%s", err->message);
      g_error_free (err);
      usage (EXIT_FAILURE);
    }

  benchmark_options_process ();

  if (OPTIONS.flags.trace)
    {
      g_assert (g_setenv ("G_MESSAGES_DEBUG", "all", TRUE));
    }

  if (OPTIONS.dump_options)
    {
      benchmark_options_print ();
      exit (EXIT_SUCCESS);
    }

  g_queue_init (&output);

  benchmark (&output);
  benchmark_options_fini ();

  if (OPTIONS.output_cache && OPTIONS.output_file != NULL)
    write_event_queue_to_file (&output, OPTIONS.output_file);

  while (output.length > 0)
    gem_event_unref (g_queue_pop_head (&output));
  g_queue_clear (&output);

  exit (EXIT_SUCCESS);
}
